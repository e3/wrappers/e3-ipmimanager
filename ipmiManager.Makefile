## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


REQUIRED += asyn stream openipmi

EXCLUDE_ARCHS += linux-corei7-poky
EXCLUDE_ARCHS += linux-ppc64e6500

APPDB := Db
APPSRC := src

DBDS    += $(APPSRC)/asyn/ipmiAsynPortDriverInclude.dbd

# IPMI Part
SOURCES += $(APPSRC)/ipmi/ipmiMonitor.cpp
SOURCES += $(APPSRC)/ipmi/ipmiMonitorGlobals.cpp
SOURCES += $(APPSRC)/ipmi/ipmiMonitorCallbacks.cpp
SOURCES += $(APPSRC)/ipmi/ipmiMonitorDebug.cpp
SOURCES += $(APPSRC)/ipmi/ipmiMonitorSetup.cpp
SOURCES += $(APPSRC)/ipmi/ipmiMonitorLogger.cpp
SOURCES += $(APPSRC)/ipmi/ipmiMonitorSetters.cpp


HEADERS += $(APPSRC)/ipmi/ipmiMonitor.h

# Asyn Part
SOURCES += $(APPSRC)/asyn/ipmiAsynPortDriver.cpp
SOURCES += $(APPSRC)/asyn/ipmiAsynPortDriverRegister.cpp
HEADERS += $(APPSRC)/asyn/ipmiAsynPortDriver.h

# subroutine part
SOURCES += $(APPSRC)/asyn/convSecToStr.c
DBDS += $(APPSRC)/asyn/convSecToStr.dbd

SOURCES += $(APPSRC)/asyn/compVers.c
DBDS += $(APPSRC)/asyn/compVers.dbd

# database part
TEMPLATES += $(APPDB)/board.present.template
TEMPLATES += $(APPDB)/sensor.template

TEMPLATES += $(APPDB)/ipmiStreamSupport.db
TEMPLATES += $(APPDB)/ipmiStreamSupport.proto
TEMPLATES += $(APPDB)/ipmiStreamSupportPassword.db
TEMPLATES += $(APPDB)/ipmiStreamSupportPassword.proto
TEMPLATES += $(APPDB)/ipmiGlobalSettings.db

TEMPLATES += $(APPDB)/ipmiExpert.proto
TEMPLATES += $(APPDB)/ipmiExpertPassword.proto
TEMPLATES += $(APPDB)/ipmiExpert.db

TEMPLATES += $(APPDB)/ipmiPanelVer.db
TEMPLATES += $(wildcard ../template/*.db)

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

.PHONY: prebuild
prebuild: $(TEMPLATES)

$(APPDB)/%.template: $(APPDB)/%.c $(APPDB)/%.h $(APPDB)/gen.sh
	$(APPDB)/gen.sh $@ $<

.PHONY: vlibs
vlibs:
