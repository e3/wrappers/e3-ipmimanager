# This script is asigned to display widget in rtm window
# Its role is to configure all text fields, LEDs etc. depending on available RTM modules
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model.properties import WidgetColor
from org.csstudio.display.builder.model.properties import ActionInfos
from org.csstudio.display.builder.model.properties import OpenDisplayActionInfo
from org.csstudio.display.builder.model.properties.OpenDisplayActionInfo import Target
from org.csstudio.display.builder.model.widgets import ActionButtonWidget
from java.util import Arrays

# To write a portable script, check for the display builder's widget type:
display_builder = 'getVersion' in dir(widget)

if display_builder:
    phoebus = 'PHOEBUS' in dir(ScriptUtil)
    if phoebus:
		from org.phoebus.framework.macros import Macros
    else:
		from org.csstudio.display.builder.model.macros import Macros
else:
    from org.csstudio.opibuilder.scriptUtil import PVUtil, ConsoleUtil


# function to get reference to display screen
def getDisplay():
	display = widget.getDisplayModel()
	return display


# function to get module's FruId
def getFruId(pv_name):
	try:
		# create listener on PV
		pv = PVUtil.createPV(pv_name, 2000)
		# read PV
		id = pv.read().getValue()
		# release listener
		PVUtil.releasePV(pv)
	except:
		ScriptUtil.getLogger().severe("Can't read FruId from " + pv_name)
		id = 0
	return id

# find expert button and add action with proper macros
def addExpertAction(pmacro, module_macro, idx_macro, crate_macro):
	pv = pmacro + '-' + module_macro + '-' + crate_macro + idx_macro + 'FruId'
	fruId = getFruId(pv)
	but = ScriptUtil.findWidgetByName(display, "Expert_button")
	if not fruId:
		but.setPropertyValue('enabled', False)
	# generate macros for button
	expert_macros = Macros()
	expert_macros.add('FRUID', str(fruId))
	# create action
	open_info = OpenDisplayActionInfo("Expert", "../expert/ExpertFRU.bob", expert_macros, Target.WINDOW)
	info_list = Arrays.asList(open_info)
	actions_info = ActionInfos(info_list)
	but.propActions().setValue(actions_info)

# set update fields with rtm infos
def setStateFields(pmacro, module_macro, idx_macro, crate_macro, name_mode):
	if name_mode == '0':
		prefix = pmacro + '-' + module_macro + '-' + crate_macro + idx_macro
	else:
		prefix = pmacro + '-' + module_macro + '-' + crate_macro + idx_macro + ':'
	presence = prefix + "P"
	slot = ScriptUtil.findWidgetByName(display, "RTM1_update_slot")
	name = ScriptUtil.findWidgetByName(display, "RTM1_update_name")
	fru = ScriptUtil.findWidgetByName(display, "RTM1_update_fru")
	led = ScriptUtil.findWidgetByName(display, "RTM1_MultiLED")
	sensor = ScriptUtil.findWidgetByName(display, "Sensor_button")
	widgets = [slot, name, fru]
	names = ["Slot", "FruName", "FruId"]
	ScriptUtil.getLogger().severe(presence)
	try:
		# create listener on PV
		pv = PVUtil.createPV(presence, 2000)
		# read PV
		state = pv.read().getValue()
		# release listener
		PVUtil.releasePV(pv)
		# if module is available
		if state:
			for idx, w in enumerate(widgets):
				w.setPropertyValue("pv_name", prefix + names[idx])
			led.setPropertyValue("pv_name", prefix + "State")
			sensor.setPropertyValue('enabled', True)
			l = sensor.propActions().getValue()
			actions = l.getActions()
			sensor_macro = actions[0].getMacros()
			if name_mode == '0':
				sensor_macro.add('IDX', idx_macro)
				sensor_macro.add('CRATE_NUM', '-' + crate_macro)
				sensor_macro.add('MODULE', '-' + module_macro)
			else:
				sensor_macro.add('IDX', idx_macro + ':')
				sensor_macro.add('P', pmacro + '-')
				sensor_macro.add('CRATE_NUM', '-')
		else:
			for w in widgets:
				w.setPropertyValue("pv_name", "")
				w.setPropertyValue("background_color", disable)
				w.setPropertyValue("border_alarm_sensitive", False)
			led.setPropertyValue("pv_name", "")
			led.setPropertyValue("states[0].color", disable)
			led.setPropertyValue("border_alarm_sensitive", False)
	except:
		for w in widgets:
			w.setPropertyValue("pv_name", "")
			w.setPropertyValue("background_color", disable)
			w.setPropertyValue("border_alarm_sensitive", False)
		led.setPropertyValue("pv_name", "")
		led.setPropertyValue("states[0].color", disable)
		led.setPropertyValue("border_alarm_sensitive", False)

# set FRU details fields
def setDetailsFields(pmacro, module_macro, idx_macro, crate_macro, name_mode):

	p1 = ["B", "P"]
	p2 = ["Manuf", "Name", "SN", "PN"]
	if name_mode == '0':
		prefix = pmacro + '-' + module_macro + '-' + crate_macro + idx_macro
	else:
		prefix = pmacro + '-' + module_macro + '-' + crate_macro + idx_macro + ':'
	presence = prefix + "P"
	try:
		# create listener on PV
		pv = PVUtil.createPV(presence, 2000)
		# read PV
		state = pv.read().getValue()
		# release listener
		PVUtil.releasePV(pv)
		for p in p1:
			for s in p2:
				name = p + s
				if name == "PPN":
					name = "PN"
				w = ScriptUtil.findWidgetByName(display, name + "_update")
				if state:
					w.setPropertyValue("pv_name", prefix + name)
				else:
					w.setPropertyValue("pv_name", "")
					w.setPropertyValue("background_color", disable)
					w.setPropertyValue("border_alarm_sensitive", False)
	except:
		for p in p1:
			for s in p2:
				name = p + s
				if name == "PPN":
					name = "PN"
				w = ScriptUtil.findWidgetByName(display, name + "_update")
				w.setPropertyValue("pv_name", "")
				w.setPropertyValue("background_color", disable)
				w.setPropertyValue("border_alarm_sensitive", False)



if __name__ == "__main__":
	# get display reference
	display = getDisplay()
	# get macros to generate PVs names
	macro = display.getEffectiveMacros()
	pmacro = macro.getValue('P')
	name_mode = macro.getValue('NAME_MODE')

	module_macro = macro.getValue('MODULE')
	idx_macro = macro.getValue('IDX')
	crate_macro = macro.getValue('CRATE_NUM')
	# gray color definition
	disable = WidgetColor(169, 169, 169)

	setStateFields(pmacro, module_macro, idx_macro, crate_macro, name_mode)
	setDetailsFields(pmacro, module_macro, idx_macro, crate_macro, name_mode)
