e3-ipmimanager
======
ESS Site-specific EPICS module : ipmimanager

# IOC Architecture
The IOC has 2 functional blocks:
 * ASYN port driver - provides EPICS layer communication
 * ipmiMonitor - it is C++ wrapper for OpenIpmi interfaces used to communicate with MCH devices
Both modules are running in separate threads and use standard EPICS routines for inter-thread communication such as epicsMutex


# Software Structure

The software is divided into 3 layers:

 * openIPMI library - responsible for establishing a connection with MCH device and for reading and updating chassis status with the use of ipmi and rmcp+ protocols, on this layer, a dedicated extension module (plugin) was implemented to add missing functionality required by systems compliant with MTCA.4,
 * dedicated class ipmiManager using interfaces available in the openIPMI library, in order to create and update a copy of the system status, providing interfaces for status reporting,
 * EPICS IOC based on the ASYN controller, aimed at providing data read from the ipmiManager class in the form of dedicated parameters (PV) made available via EPICS protocols.


# OpenIPMI

OpenIPMI library is OpenSource implementation, which provides high level abstraction for IPMI interfaces. The main purpose is to provide functionality according to IPMI specification with additional efforts to implement different extensions defined by vendors and sub comitees (including PICMG ATCA extensions).

During evaluation of different solutions, this library has been selected as base of further implementation. The library is implemented in C language and provides a single threaded, callback based interface. It also provides additional abstractions to use it in multi-threaded envioroment, but they are not used in presented implementation.

## OpenIPMI Installation
In order to use the IOC, the OpenIPMI library must be available in the system. The library has been integrated with e3 environment, so manual installation is not needed. Due to some problems and incompatibilities of different uTCA boards with IPMI standard, some modifications to the lib are needed. e3-openIpmi/patch subfolder contains a patch that is applied to the original OpenIPMI sources before building.

It contains 3 modifications:
 * Change of FRU block size for reading FRUs from the boards - some boards are not handling large block readouts of FRU in a right way (NAT PCIe, IFC1410)
 * Disable ATCA plugin - since uTCA and ATCA are strongly related, default plugin of OpenIPMI lib dedicated for ATCA systems recognizes uTCA chassis and intercepts calls - it must be disabled on source code level
 * Disable incremental readout of SEL - default behaviour of NAT MCH is to delete SEL entry after readout - this is conflicting with default readout policy of OpenIPMI / ATCA and must be changed on software level (either by a library patch or by changing default configuration on MCH side)

You can find e3-openIpmi module here [link](https://gitlab.esss.lu.se/e3/common/e3-openIpmi)

# ipmiManager

The module provides C++ wrapper for the library functions. It is equipped with set of multi-threaded mechnisms such as mutex, locks and so on to be ready for operation in epics environment. It contains two functional blocks - global functions to be registered as callbacks for the OpenIpmi library interface and C++ Class, which provides dedicated methods to execute specific IPMI functions. Global functions receive callback and dispatch them to specific methods of the class (such methods cannot be registered ac C callback functions). The class also provides main IPMI Loop (run method), which can be used in Epics thread.

# Asyn
Asyn is responsible for reading data from ipmiManager class and passing them to PVs. This layer is also used to generate templates and substitutions files.  The constructor of Asyn class uses macro functions to create and set all db parameters while iterating through the list of available boards and sensors. It generates general-purpose, concerning to the IOC functioning as well.

The macros definitions can be found in the header file. For each type of parameters (Integer, Double, String) there is a group of three macros functions CREATE_AND_SET_<type> for board parameters, SENS_SET_<type> for sensor parameters. Moreover, there are specific macros functions for general-purpose PVs parameters. To keep coherence between macros used in board.h and sensor.h some macros are empty. Thanks to that, PVs created with macros can be easily connected with corresponding Asyn parameters.

The constructor creates two threads. The thread for "ipmiMonitor" task and the separate thread for general refresh processes where new values from the device are transferred to PVs.

To communicate with ipmiManager dedicated class, Asyn driver uses its interface called on an object of this class which is a member of Asyn class.

Asyn driver register file contains function generating templates and substitutions files. This function is called after asyn constructor so it is possible to access the current MTCA configuration and based on a list of boards and sensors produce needed files.


# StreamDevice
Since not all MCH parameters are accessible via openIPMI library, some properties are read using StreamDevice device support (MCH FW version, active links states). Then, a module to reboot and shutdown the chassis, and to reboot FRU was implemented using this communication interface.

Db files containing PVs with StreamDevice:

 * ipmiExpert.db
 * ipmiStreamSupport.db
 * ipmiStreamSupportPassword.db
All files have corresponding protocol files (.proto), describing the communication with the device.

In order not to keep open telnet session for IOC, each protocol function contains connect/disconnect commands which opens a connection and closes it when the MCH command is sent to the device.

IOC also supports telnet connections when the MCH is password protected. The password can be defined in cmd files as $(PASS) macro. It is stored in the "$(P)Ctrl-MTCA-$(CRATE_NUM)00:Pass" PV and then used in the protocol functions.  It is also necessary to set the macro $(USE_STREAM_PASS) to enable stream password mode.

IOC can be launched without stream PVs as well.


# MTCA system structure description - dynamic vs. static
During the development phase, two approaches in building the IOC database have been investigated. To eliminate the creation of multiple Db files (one file for each possible module in MTCA crate) and to decrease the wrong configuration possibility the dynamic approach was applied. Thanks to that, there is no need to predefine numerous Db files with PVs for all boards. Additionally, when a new module is installed in the chassis the IOC does not require any code changes or a new Db file. IOC creates template files themselves for PVs assigned to board parameters and then for sensors. Depending on detected modules in the chassis, IOC generates substitutions files for mentioned templates. Those substitutions files contain macros that need to be defined in cmd file. This is the only static configuration provided for an operator. The rest available Db files are composed of general PVs, with parameters concerning IOC control or MTCA crate.

The same approach was applied to operator panels. There are universal .bob files that are modified with Jython scripts and implement rules depending on detected boards and their sensors.

If needed, automatic generation of templates and substitutions files can be disabled in cmd file.


# Naming convention
All PVs have the following format - $(P)Ctrl-<board_name>-$(CRATE_NUM)<idx>:<property> where:

 * $(P) macro can be defined in cmd file
 * $(CRATE_NUM) macro can be defined in cmd file
 * <board_name> for general MTCA properties <board_name> is replaced with MTCA, then for each module <board_name> is replaced with its name e.g. PM, CU, RTM, EVR, CPU, AMC etc. In case of additional modules (PM, CU, Clock, PCI) board_name is set automatically, for other modules you can define this part in cmd file with corresponding macros.
 * <idx> settable index, two digits, (they have nothing to do with FRU or slot ID) for PM, CU, Clock, PCI it is set automatically, for other modules it can be modified with macros values in cmd files
 * <property> for board property it is just its name like: State, FruId etc., for sensor properties <property> contains prefix "Sen<number_of_sensor>" and then the real property e.g. Sen21ThrUpNonRec
 * setpoints PVs are finished with "-SP" suffix

PCI and Clock modules are special cases where the <board_name> is replaced with MTCA and it is <property> part that has a reference to the actual module e.g. "LabS:Ctrl-MTCA-100:Clk-Sen0NormMin".
