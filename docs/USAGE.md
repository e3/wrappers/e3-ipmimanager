e3-ipmimanager
======
ESS Site-specific EPICS module : ipmimanager


# Usage
This instruction will tell you how to use the ioc console to check the basic MTCA parameters.


## Get FRU information and device ID
To check FRU information the following PVs should be checked (one set for each board):
 - $(PREFIX)-$(INDEX)$(OPT)FruId
 - $(PREFIX)-$(INDEX)$(OPT)Slot
 - $(PREFIX)-$(INDEX)$(OPT)FruName
e.g.:

 - LabS:Ctrl-EVR-101:FruName
 - LabS:Ctrl-RTM-110:FruId
 - LabS:Ctrl-PM-101:Slot

## Get hot-swap states
To check hot-swap state the following PV should be verified (one PV for each board):

 - $(PREFIX)-$(INDEX)$(OPT)State
e.g.:

 - LabS:Ctrl-AMC-130:State

## Get sensor values
To check sensor information (type, name, value, unit) the following PVs should be verified (one set for each sensor):

 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)Name
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)Type
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)Value
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)Units
e.g.:

 - LabS:Ctrl-AMC-130:Sen15Value
 - LabS:Ctrl-MTCA-100:Clk-Sen3Type
 - LabS:Ctrl-RTM-120:Sen40Name
 - LabS:Ctrl-EVR-101:Sen3Units

## Get sensor thresholds levels
The thresholds set on the board for individual sensors can be examined using the following PVs:

 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrUpNonCrt
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrUpCrt
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrUpNonRec
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrLoNonCrt
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrLoCrt
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrLoNonRec

## Set thresholds values
The IOC supports setting of the thresholds by user. For this purpose set of _SP PVs was created:

 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrUpNonCrt-SP
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrUpCrt-SP
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrUpNonRec-SP
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrLoNonCrt-SP
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrLoCrt-SP
 - $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrLoNonRec-SP
The typical procedure to set one or more threshold is:

Write the desired value to -SP PVs
Write 1 into $(PREFIX)-$(INDEX)$(OPT)Sen$(SINDEX)ThrCommit PV to submit changes to hardware
After thresholds are submitted, the readback PVs will be updated.

## Restart FRU
There is one PV to restart all FRUs. It takes as an argument FRU ID

Write the desired FRU ID to PV:

 - $(P)Ctrl-MTCA-$(CRATE_NUM)00:RebootFru-SP
e.g.:

 - LabS:Ctrl-MTCA-100:RebootFru-SP
After this, the FRU will be rebooted

## Restart/Shutdown chassis
There is one PV for the reboot and one PV for the shutdown of the whole chassis:

 - $(P)Ctrl-MTCA-$(CRATE_NUM)00:Reboot-SP
 - $(P)Ctrl-MTCA-$(CRATE_NUM)00:ShutDown-SP
e.g.:

 - LabS:Ctrl-MTCA-100:ShutDown-SP
 - LabS:Ctrl-MTCA-100:Reboot-SP
