

function showThr() {

  NAME=$1:SEN$2""NAME
  echo "Sensor name: $(caget $NAME | sed 's/[^ ]*[ ]*//')"

  THR="$1:SEN#N#THRUPNONRECVAL
       $1:SEN#N#THRUPCRITVAL
       $1:SEN#N#THRUPNONCRITVAL
       $1:SEN#N#THRLONONCRITVAL
       $1:SEN#N#THRLOCRITVAL
       $1:SEN#N#THRLONONRECVAL"

  for I in $THR; do
    caget ${I//#N#/$2}
  done

}


function showSensorNames() {

  COUNT=`caget $1:SENSORCNT | sed 's/[^ ]*[ ]*//'`

  echo "Total number of sensors: $COUNT"

  for ((I=0; I<$COUNT; I++)) do
    echo "Sensor $I:" $(caget $1:SEN""$I""NAME | sed 's/[^ ]*[ ]*//')
  done

}
