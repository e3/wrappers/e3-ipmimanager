#!/usr/bin/python

import sys
sys.path.append("..")

from epics import caget, caput
from utils import ipmimanager_common as e4i_common
import time
import argparse
import configparser

THR_SETTABLE_MASK_BIT_LO_NON_CRIT = 0
THR_SETTABLE_MASK_BIT_LO_CRIT = 1
THR_SETTABLE_MASK_BIT_LO_NON_REC = 2
THR_SETTABLE_MASK_BIT_UP_NON_CRIT = 3
THR_SETTABLE_MASK_BIT_UP_CRIT = 4
THR_SETTABLE_MASK_BIT_UP_NON_REC = 5

THRESHOLD_UPDATE_INTERVAL = 5 # seconds


def get_sensor_name(sensor_prefix):
    return caget(sensor_prefix + e4i_common.PVSUFFIX_SENNAME)


def get_sensor_thresholds_update_allowed(sensor_prefix):
    return caget(sensor_prefix + e4i_common.PVSUFFIX_SENTHRUPDATEALLOWED)


def get_sensor_thresholds(sensor_prefix):
    thresholds = dict()
    thresholds['UpNonRec'] = caget(sensor_prefix + e4i_common.PVSUFFIX_SENTHRUPNONRECVAL)
    thresholds['ThrUpCrt'] = caget(sensor_prefix + e4i_common.PVSUFFIX_SENTHRUPCRITVAL)
    thresholds['ThrUpNonCrt'] = caget(sensor_prefix + e4i_common.PVSUFFIX_SENTHRUPNONCRITVAL)
    thresholds['ThrLoNonRec'] = caget(sensor_prefix + e4i_common.PVSUFFIX_SENTHRLONONRECVAL)
    thresholds['ThrLoCrt'] = caget(sensor_prefix + e4i_common.PVSUFFIX_SENTHRLOCRITVAL)
    thresholds['ThrLoNonCrt'] = caget(sensor_prefix + e4i_common.PVSUFFIX_SENTHRLONONCRITVAL)
    return thresholds


def set_sensor_thresholds(sensor_prefix, thresholds):
    caput(sensor_prefix + e4i_common.PVSUFFIX_SENTHRUPNONRECVAL_SP, thresholds['UpNonRec'], wait=True)
    caput(sensor_prefix + e4i_common.PVSUFFIX_SENTHRUPCRITVAL_SP, thresholds['ThrUpCrt'], wait=True)
    caput(sensor_prefix + e4i_common.PVSUFFIX_SENTHRUPNONCRITVAL_SP, thresholds['ThrUpNonCrt'], wait=True)
    caput(sensor_prefix + e4i_common.PVSUFFIX_SENTHRLONONRECVAL_SP, thresholds['ThrLoNonRec'], wait=True)
    caput(sensor_prefix + e4i_common.PVSUFFIX_SENTHRLOCRITVAL_SP, thresholds['ThrLoCrt'], wait=True)
    caput(sensor_prefix + e4i_common.PVSUFFIX_SENTHRLONONCRITVAL_SP, thresholds['ThrLoNonCrt'], wait=True)
    caput(sensor_prefix + e4i_common.PVSUFFIX_SENTHRCOMMIT, 1, wait=True)


def find_sensors_for_tests(fru_list):
    # Use only MCH FRUs - they are in every chassis
    sensors_list = []
    for fru in fru_list:
        if ("PCI" in fru_list[fru].upper()) or ("CLK" in fru_list[fru].upper()):
            fru_pv_prefix = fru_list[fru]
            sensor_cnt = caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUSENSORCNT)
            for i in range(0, sensor_cnt):
                sensor_thr_allowed = get_sensor_thresholds_update_allowed(fru_pv_prefix + "Sen" + str(i))
                if sensor_thr_allowed != 0:
                    sensors_list.append(fru_pv_prefix + "Sen" + str(i))

    return sensors_list


def test_setting_thresholds(config_file, configparser, section):
    global_status = True

    # Read FRU list from ipmimanager
    fru_list_ipmimanager = e4i_common.create_fru_map(config_file)
    fru_list = fru_list_ipmimanager[section]

    # Find sensors useful for tests - from PCI and/or Clk modules with settable thresholds
    sensors_list = find_sensors_for_tests(fru_list)

    # Modify default refreshing interval to speed-up the tests
    ioc_prefix = configparser['IOC']['PREFIX']
    ioc_settings_prefix = configparser[section]['PV_PREFIX_GLOBAL_SETTINGS']
    caput(ioc_prefix + ioc_settings_prefix + e4i_common.PVSUFFIX_GLOBAL_THRESHINTERVAL_SP, THRESHOLD_UPDATE_INTERVAL)

    sensors_info = dict()
    for sensor in sensors_list:
        sen_info = dict()
        sen_info['PvPrefix'] = sensor
        name = get_sensor_name(sensor)
        sen_info['ThrUpdateAllowed'] = get_sensor_thresholds_update_allowed(sensor)
        sen_info['OldThresholds'] = get_sensor_thresholds(sensor)
        sensors_info[name] = sen_info

    print("==========")
    print("Sensors selected for tests:")
    print("==========")
    for sensor in sensors_info:
        print("\t" + sensors_info[sensor]['PvPrefix'] + " - " + sensor)

    print("\n\n==========")
    print("Setting new thresholds for sensors:")
    print("==========")
    for sensor in sensors_info:
        print(sensors_info[sensor]['PvPrefix'] + " - " + sensor)
        sensor_thr_update_allowed = sensors_info[sensor]['ThrUpdateAllowed']
        new_thresholds = dict(sensors_info[sensor]['OldThresholds'])
        print("Old thresholds: ")
        print(new_thresholds)
        if sensor_thr_update_allowed & (1 << THR_SETTABLE_MASK_BIT_LO_NON_CRIT):
            new_thresholds['ThrLoNonCrt'] = int(new_thresholds['ThrLoNonCrt'] * 2)
        if sensor_thr_update_allowed & (1 << THR_SETTABLE_MASK_BIT_LO_CRIT):
            new_thresholds['ThrLoCrt'] = int(new_thresholds['ThrLoCrt'] * 2)
        if sensor_thr_update_allowed & (1 << THR_SETTABLE_MASK_BIT_LO_NON_REC):
            new_thresholds['ThrLoNonRec'] = int(new_thresholds['ThrLoNonRec'] * 2)
        if sensor_thr_update_allowed & (1 << THR_SETTABLE_MASK_BIT_UP_NON_CRIT):
            new_thresholds['ThrUpNonCrt'] = int(new_thresholds['ThrUpNonCrt'] / 2)
        if sensor_thr_update_allowed & (1 << THR_SETTABLE_MASK_BIT_UP_CRIT):
            new_thresholds['ThrUpCrt'] = int(new_thresholds['ThrUpCrt'] / 2)
        if sensor_thr_update_allowed & (1 << THR_SETTABLE_MASK_BIT_UP_NON_REC):
            new_thresholds['UpNonRec'] = int(new_thresholds['UpNonRec'] / 2)

        print("New thresholds: ")
        print(new_thresholds)
        print("Applying...")
        set_sensor_thresholds(sensors_info[sensor]['PvPrefix'], new_thresholds)
        sensors_info[sensor]['NewThresholds'] = new_thresholds
        print("---")

    time.sleep(5 * THRESHOLD_UPDATE_INTERVAL)

    print("\n\n==========")
    print("Verification")
    print("==========")
    for sensor in sensors_info:
        print(sensors_info[sensor]['PvPrefix'] + " - " + sensor)
        print("Applied thresholds:")
        print(sensors_info[sensor]['NewThresholds'])
        thresholds = get_sensor_thresholds(sensors_info[sensor]['PvPrefix'])
        print("Current thresholds:")
        print(thresholds)
        status = True
        for thr in thresholds:
            if thresholds[thr] != sensors_info[sensor]['NewThresholds'][thr]:
                status = False
                break
        global_status = global_status and status
        print("Status: {:}".format(['FAILED', 'PASSED'][status]))
        print("---")

    print("\n\nPlease restart the ipmimanager IOC - this step will verify that thresholds has been modified in hardware")
    answer = None
    while not answer or answer[0].lower() != 'y':
        print("Is the IOC restarted (Y/N)?")
        answer = input()

    # Modify default refreshing interval to speed-up the tests
    ioc_prefix = configparser['IOC']['PREFIX']
    ioc_settings_prefix = configparser[section]['PV_PREFIX_GLOBAL_SETTINGS']
    caput(ioc_prefix + ioc_settings_prefix + e4i_common.PVSUFFIX_GLOBAL_THRESHINTERVAL_SP, THRESHOLD_UPDATE_INTERVAL)

    print("\n\n==========")
    print("Verification after IOC restart")
    print("==========")
    for sensor in sensors_info:
        thresholds = get_sensor_thresholds(sensors_info[sensor]['PvPrefix'])
        status = True
        for thr in thresholds:
            if thresholds[thr] != sensors_info[sensor]['NewThresholds'][thr]:
                status = False
                break
        global_status = global_status and status
        print(sensors_info[sensor]['PvPrefix'] + " - " + sensor + ": {:}".format(['FAILED', 'PASSED'][status]))

    print("\n\n==========")
    print("Restoring previous values")
    print("==========")
    for sensor in sensors_info:
        print(sensors_info[sensor]['PvPrefix'] + " - " + sensor)
        set_sensor_thresholds(sensors_info[sensor]['PvPrefix'], sensors_info[sensor]['OldThresholds'])

    time.sleep(5 * THRESHOLD_UPDATE_INTERVAL)

    print("\n\n==========")
    print("Restored values - verification")
    print("==========")
    for sensor in sensors_info:
        thresholds_restored = get_sensor_thresholds(sensors_info[sensor]['PvPrefix'])
        status = True
        for thr in thresholds_restored:
            if thresholds_restored[thr] != sensors_info[sensor]['OldThresholds'][thr]:
                status = False
                break
        global_status = global_status and status
        print(sensors_info[sensor]['PvPrefix'] + " - " + sensor + ": {:}".format(['FAILED', 'PASSED'][status]))

    print("\n\n=============")
    print("Final status: {:}".format(['FAILED', 'PASSED'][global_status]))


if __name__ == '__main__':
    config_file = e4i_common.DEFAULT_CONFIG_FILE

    parser = argparse.ArgumentParser(description='Compare e3-ipmimanager with MCH console output')
    parser.add_argument('-c', '--config_file', type=str, help='Configuration file')
    args = parser.parse_args()

    if args.config_file:
        config_file = args.config_file

    config = configparser.ConfigParser()
    config.read(config_file)
    for section in config.sections():
        if section.startswith("MTCA_CRATE_"):
            print(section)
            test_setting_thresholds(config_file, config, section)
