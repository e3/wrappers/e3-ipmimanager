e3-ipmimanager - test scripts
======
ESS Site-specific EPICS module : ipmimanager

# Introduction

Test scripts are intended to allow quick and easy verification if the e3-ipmimanager IOC has been properly installed and configured.

The scripts have been developed using Python 3.6.8 and require following Python modules:
- pyepics (https://pypi.org/project/pyepics/)
- configparser (https://pypi.org/project/configparser)
- mdutils (https://pypi.org/project/mdutils/)
- argparse
- telnetlib

The test scripts are dedicated for detailed verification of IOC operation (e.g. after deployment)
- [`verify_pvs_availability.py`](verify_pvs_availability.py) - _verifies if all expected PVs has been created and are published by the IOC, list of PVs is generated from *.ini file_
- [`compare_to_mch.py`](compare_to_mch.py) - _reads information about modules, sensors and links both from MCH (via telnet) and ipmimanager IOC, compares them and generates report file_
- [`verify_setting_thresholds.py`](verify_setting_thresholds.py) - _modifies thresholds of PCI and/or Clk modules of MCH (because they are available in every MTCA chassis), verifies if they are properly changed and restores original values_

# Configuration

All test scripts require common configuration file. By default, the scripts look for [`../utils/mtca_crate.ini`](../utils/mtca_crate.ini) and load it if no other file specified as a command-line argument. Every script supports argument `--config_file (-c)` which allows changing the configuration file that should be used for the script execution.

The configuration file specifies following settings related to IOC and MTCA crates:
- global prefix used for all IOC variables (`PREFIX in section [IOC]`)
- MCH IP addresses (`MCH_IP in sections [MTCA_CRATE_*]`)
- prefixes for PVs related to all MTCA modules managed by the IOC (`PV_PREFIX_MODULE_* in sections [MTCA_CRATE_*]`)

Tests support more than one MTCA chassis per IOC. To specify settings for more than one MTCA chassis please add more `[MTCA_CRATE_*]` sections. Section name should start with `MTCA_CRATE_`.

Example:
```ini
[IOC]
PREFIX = LabS:Ctrl-

[MTCA_CRATE_1]
MCH_IP = 192.168.1.0
PV_PREFIX_MTCA_CRATE = MTCA-100:

...

[MTCA_CRATE_2]
MCH_IP = 192.168.1.1
PV_PREFIX_MTCA_CRATE = MTCA-200:

...

```

# Usage

## 1. verify_pvs_availability

Example command
```commandline
$ python3 verify_pvs_availability.py
```

Supported arguments
```commandline
-c, --config_file - Specify configuration file
```

Example output
```
ipmimanager - full tests:
==========================================
MTCA_CRATE_1
==========================================
Global settings testing: 8/8 PVs
Chassis testing: 9/9 PVs
FRU 5 testing: 19/19 PVs
	Sensors - 11: 429/429 PVs
FRU 6 testing: 19/19 PVs
	Sensors - 13: 507/507 PVs
FRU 7 testing: 19/19 PVs
	Sensors - 19: 741/741 PVs
FRU 9 testing: 19/19 PVs
	Sensors - 23: 897/897 PVs
FRU 11 testing: 19/19 PVs
	Sensors - 23: 897/897 PVs
FRU 40 testing: 19/19 PVs
	Sensors - 15: 585/585 PVs
FRU 41 testing: 19/19 PVs
	Sensors - 15: 585/585 PVs
FRU 51 testing: 19/19 PVs
	Sensors - 28: 1092/1092 PVs
FRU 60 testing: 19/19 PVs
	Sensors - 6: 234/234 PVs
FRU 61 testing: 19/19 PVs
	Sensors - 11: 429/429 PVs
FRU 92 testing: 19/19 PVs
	Sensors - 4: 156/156 PVs
FRU 94 testing: 19/19 PVs
	Sensors - 52: 2028/2028 PVs
FRU 96 testing: 19/19 PVs
	Sensors - 7: 273/273 PVs
==========================================
Results - 9117/9117
```

## 2. compare_to_mch

Example command
```commandline
$ python3 compare_to_mch.py
```

Supported arguments
```commandline
-c, --config_file - Specify configuration file
```

Example output
```
===================================================================
FRU List
-------------------------------------------------------------------
M   0) MCH             - NAT-MCH-CM                     [M4]

M   3) mcmc1           - NAT-MCH-MCMC                   [M4]

M   5) AMC1            - CCT AM 900/412                 [M4]
E   5)                 - CCT AM 900/412                 [M4]

M   6) AMC2            - mTCA-EVR-300                   [M4]
E   6)                 - mTCA-EVR-300                   [M4]

M   7) AMC3            - SIS8300KU AMC                  [M4]
E   7)                 - SIS8300KU AMC                  [M4]

M   9) AMC5            - RTM Carrier                    [M4]
E   9)                 - RTM Carrier                    [M4]

M  11) AMC7            - RTM Carrier                    [M4]
E  11)                 - RTM Carrier                    [M4]

M  40) CU1             - Schroff uTCA CU                [M4]
E  40)                 - Schroff uTCA CU                [M4]

M  41) CU2             - Schroff uTCA CU                [M4]
E  41)                 - Schroff uTCA CU                [M4]

M  51) PM2             - PM-AC1000                      [M4]
E  51)                 - PM-AC1000                      [M4]

M  60) Clock1          - MCH-Clock                      [M4]
E  60)                 - MCH-Clock                      [M4]

M  61) HubMod1         - MCH-PCIe                       [M4]
E  61)                 - MCH-PCIe                       [M4]

M  92) AMC3-RTM        - SIS8300KU RTM                  [M4]
E  92)                 - SIS8300KU RTM                  [M4]

M  94) AMC5-RTM        - RTM Carrier RTM                [M4]
E  94)                 - RTM Carrier RTM                [M4]

M  96) AMC7-RTM        - RTM Carrier RTM                [M4]
E  96)                 - RTM Carrier RTM                [M4]

===================================================================

...

===================================================================
Sensors List for FRU 5
-------------------------------------------------------------------
M   5) +12V PSU                       -          12.296 V               [Voltage]
E   7) +12V PSU                       -            12.3 volts           [voltage]

M   7) +3.3V PSU                      -           3.230 V               [Voltage]
E   5) +3.3V PSU                      -            3.23 volts           [voltage]

M   6) +5V PSU                        -           5.115 V               [Voltage]
E   6) +5V PSU                        -            5.12 volts           [voltage]

M   9) +PS 1V0                        -           1.010 V               [Voltage]
E   3) +PS 1V0                        -            1.02 volts           [voltage]

M   8) +PS 1V05                       -           1.080 V               [Voltage]
E   4) +PS 1V05                       -            1.08 volts           [voltage]

M   4) Ambient Temp                   -              33 C               [Temp]
E   8) Ambient Temp                   -            33.0 C               [temperature]

M   3) CPU AMB Temp                   -              35 C               [Temp]
E   9) CPU AMB Temp                   -            35.0 C               [temperature]

M  11) Ejector Handle                 -            0x01                 [0xf2]
E   1) Ejector Handle                 -             1.0 unspecified     [invalid]

M  12) HS 005 AMC1                    -            0x10                 [0xf0]
E   0) HS 005 AMC1                    -             0.0 unspecified     [invalid]

M  10) VCC CORE                       -           0.940 V               [Voltage]
E   2) VCC CORE                       -            0.95 volts           [voltage]

M
E  10) CPU INT Temp                   -            36.0 C               [temperature]

...

===================================================================
Link States
-------------------------------------------------------------------
MCH
-------------------------------------------------------------------

   5   AMC_1      0   Eth       x1      1.000  Gbps
       AMC_1      4  PCIE       x4      8.000  GT/s
                  5  PCIE       x4      8.000  GT/s
                  6  PCIE       x4      8.000  GT/s
                  7  PCIE       x4      8.000  GT/s




-------------------------------------------------------------------
ipmimanager
-------------------------------------------------------------------
5     AMC_1      0     Eth        x1         1.000 Gbps
      AMC_1      4     PCIE       x4         8.000 GT/s
                 5     PCIE       x4         8.000 GT/s
                 6     PCIE       x4         8.000 GT/s
                 7     PCIE       x4         8.000 GT/s

===================================================================

```
**Legend:**

**M - information/values taken from MCH**

**E - information/values taken from e3-ipmimanager EPICS IOC**

_NOTE: Presented example is only short part of the full output generated by the script. Full output contains test results for every module installed in the MTCA chassis._

The script generates report file in Markdown format which includes all information presented in terminal during execution of the tests. Summary of the tests can be found at the end of the generated report file.
Example summary
```commandline
===================================================================
SUMMARY
===================================================================
1. Detected FRUs - MCH: 15, ipmimanager: 1 [PASSED]
2. Detected sensors
	5) MCH: 10, ipmimanager: 11 [FAILED]
	6) MCH: 13, ipmimanager: 13 [PASSED]
	7) MCH: 19, ipmimanager: 19 [PASSED]
	9) MCH: 23, ipmimanager: 23 [PASSED]
	11) MCH: 23, ipmimanager: 23 [PASSED]
	40) MCH: 15, ipmimanager: 15 [PASSED]
	41) MCH: 15, ipmimanager: 15 [PASSED]
	51) MCH: 30, ipmimanager: 28 [FAILED]
	60) MCH: 6, ipmimanager: 6 [PASSED]
	61) MCH: 11, ipmimanager: 11 [PASSED]
	92) MCH: 4, ipmimanager: 4 [PASSED]
	94) MCH: 52, ipmimanager: 52 [FAILED]
	96) MCH: 7, ipmimanager: 7 [PASSED]
3. AMC link states [PASSED]
===================================================================

```

## 3. verify_setting_thresholds

Example command
```commandline
$ python3 verify__setting_thresholds.py
```

Supported arguments
```commandline
-c, --config_file - Specify configuration file
```

Example output
```
MTCA_CRATE_1
==========
Sensors selected for tests:
==========
	LabS:Ctrl-MTCA-100:Clk-Sen2 - Temp Mux max
	LabS:Ctrl-MTCA-100:Clk-Sen3 - Temp Mux
	LabS:Ctrl-MTCA-100:PCI-Sen9 - Temp DC/DC max
	LabS:Ctrl-MTCA-100:PCI-Sen10 - Temp DC/DC
	LabS:Ctrl-MTCA-100:PCI-Sen5 - Temp PCB max
	LabS:Ctrl-MTCA-100:PCI-Sen6 - Temp PCB
	LabS:Ctrl-MTCA-100:PCI-Sen7 - Temp Switch max
	LabS:Ctrl-MTCA-100:PCI-Sen8 - Temp Switch


==========
Setting new thresholds for sensors:
==========
LabS:Ctrl-MTCA-100:Clk-Sen2 - Temp Mux max
Old thresholds:
{'UpNonRec': 5.0, 'ThrUpCrt': 4.0, 'ThrUpNonCrt': 4.0, 'ThrLoNonRec': 2.0, 'ThrLoCrt': 4.0, 'ThrLoNonCrt': 8.0}
New thresholds:
{'UpNonRec': 2, 'ThrUpCrt': 2, 'ThrUpNonCrt': 2, 'ThrLoNonRec': 2.0, 'ThrLoCrt': 4.0, 'ThrLoNonCrt': 8.0}
Applying...

...

==========
Verification
==========
LabS:Ctrl-MTCA-100:Clk-Sen2 - Temp Mux max
Applied thresholds:
{'UpNonRec': 2, 'ThrUpCrt': 2, 'ThrUpNonCrt': 2, 'ThrLoNonRec': 2.0, 'ThrLoCrt': 4.0, 'ThrLoNonCrt': 8.0}
Current thresholds:
{'UpNonRec': 2.0, 'ThrUpCrt': 2.0, 'ThrUpNonCrt': 2.0, 'ThrLoNonRec': 2.0, 'ThrLoCrt': 4.0, 'ThrLoNonCrt': 8.0}
Status: PASSED

...

Please restart the ipmimanager IOC - this step will verify that thresholds has been modified in hardware
Is the IOC restarted (Y/N)?

==========
Verification after IOC restart
==========
LabS:Ctrl-MTCA-100:Clk-Sen2 - Temp Mux max: PASSED
LabS:Ctrl-MTCA-100:Clk-Sen3 - Temp Mux: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen9 - Temp DC/DC max: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen10 - Temp DC/DC: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen5 - Temp PCB max: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen6 - Temp PCB: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen7 - Temp Switch max: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen8 - Temp Switch: PASSED

==========
Restoring previous values
==========
LabS:Ctrl-MTCA-100:Clk-Sen2 - Temp Mux max
LabS:Ctrl-MTCA-100:Clk-Sen3 - Temp Mux
LabS:Ctrl-MTCA-100:PCI-Sen9 - Temp DC/DC max
LabS:Ctrl-MTCA-100:PCI-Sen10 - Temp DC/DC
LabS:Ctrl-MTCA-100:PCI-Sen5 - Temp PCB max
LabS:Ctrl-MTCA-100:PCI-Sen6 - Temp PCB
LabS:Ctrl-MTCA-100:PCI-Sen7 - Temp Switch max
LabS:Ctrl-MTCA-100:PCI-Sen8 - Temp Switch


==========
Restored values - verification
==========
LabS:Ctrl-MTCA-100:Clk-Sen2 - Temp Mux max: PASSED
LabS:Ctrl-MTCA-100:Clk-Sen3 - Temp Mux: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen9 - Temp DC/DC max: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen10 - Temp DC/DC: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen5 - Temp PCB max: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen6 - Temp PCB: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen7 - Temp Switch max: PASSED
LabS:Ctrl-MTCA-100:PCI-Sen8 - Temp Switch: PASSED


=============
Final status: PASSED
```
