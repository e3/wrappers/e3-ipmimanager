#!/usr/bin/python

import sys
sys.path.append("..")

from epics import caget
from utils import ipmimanager_common as e4i_common, mch_telnet as mch_conn
from mdutils.mdutils import MdUtils
import time
import argparse
import configparser


class IpmiManagerSensor:
    __slots__ = ["number", "sensor_type", "value", "units", "name"]

    def __init__(self, number, name, sensor_type, value, units):
        self.number = number
        self.sensor_type = sensor_type
        self.value = value
        self.units = units
        self.name = name

    def __lt__(self, other):
        return self.name < other.name


def get_str_fru_from_ipmimanager(fru_id, fru_pv_prefix):
    fru_name = caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUNAME)
    fru_state = caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUSTATE)
    return "{:3}) {:15} - {:30} [M{:}]".format(fru_id, "", fru_name, fru_state)


def get_str_sensor_from_mch(sensor):
    return "{:>3}) {:30} - {:>15} {:15.15} [{:}]".format(sensor.number, sensor.name, sensor.value, sensor.unit, sensor.sensor_type)


def get_str_sensor_from_ipmimanager(sensor):
    return "{:>3}) {:30} - {:>15.3} {:15.15} [{:}]".format(sensor.number, sensor.name, sensor.value, sensor.units, sensor.sensor_type)


def get_sensors_from_ipmimanager(fru_pv_prefix):
    sensor_cnt = caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUSENSORCNT)
    sensors_list = []
    for i in range(0, sensor_cnt):
        sensor_type = caget(fru_pv_prefix + "Sen" + str(i) + e4i_common.PVSUFFIX_SENTYPE)
        sensor_name = caget(fru_pv_prefix + "Sen" + str(i) + e4i_common.PVSUFFIX_SENNAME)
        sensor_value = caget(fru_pv_prefix + "Sen" + str(i) + e4i_common.PVSUFFIX_SENVALUE)
        sensor_units = caget(fru_pv_prefix + "Sen" + str(i) + e4i_common.PVSUFFIX_SENUNITS)
        sensor = IpmiManagerSensor(i, sensor_name, sensor_type, sensor_value, sensor_units)
        sensors_list.append(sensor)

    return sensors_list


def compare_sensor_type(sensor1_type, sensor2_type):
    s1_type_cut = sensor1_type[0:4].lower()
    s2_type_cut = sensor2_type[0:4].lower()

    if (s1_type_cut == s2_type_cut) or \
            (s1_type_cut == "0x0b" and s2_type_cut == "othe") or \
            (s1_type_cut == "0xc0" and s2_type_cut == "inva") or \
            (s1_type_cut == "0xf0" and s2_type_cut == "inva") or \
            (s1_type_cut == "0xf1" and s2_type_cut == "inva") or \
            (s1_type_cut == "0xf2" and s2_type_cut == "inva") :
        return True

    return False


def find_sensor_in_ipmimanager_list(sensor_name, sensor_type, sensor_list):
    for idx, s in enumerate(sensor_list):
        if sensor_name.strip() == s.name.strip():
            if compare_sensor_type(sensor_type, s.sensor_type.strip()):
                return idx
    return None


def get_str_links_from_ipmimanager(links_str):
    links_str = list(filter(None, links_str))

    output_str = ""
    amc_indices = [i for i, s in enumerate(links_str) if 'AMC' in s]
    for i, index in enumerate(amc_indices):
        if links_str[index-1].isnumeric():
            output_str += "{:5} {:10} ".format(links_str[index - 1], links_str[index])
        else:
            output_str += "{:5} {:10} ".format("", links_str[index])

        start = amc_indices[i]+1
        if i + 1 < len(amc_indices):
            end = amc_indices[i + 1]
        else:
            end = None
        amc_ports = links_str[start:end]
        amc_ports_count = int(len(amc_ports)/5)
        for p in range(0, amc_ports_count):
            if p == 0:
                output_str += "{:5} {:10} {:10} {:} {:}\n".format(amc_ports[p*5+0], amc_ports[p*5+1], amc_ports[p*5+2], amc_ports[p*5+3],  amc_ports[p*5+4])
            else:
                output_str += "{:16} {:5} {:10} {:10} {:} {:}\n".format("", amc_ports[p * 5 + 0], amc_ports[p * 5 + 1], amc_ports[p * 5 + 2], amc_ports[p * 5 + 3], amc_ports[p * 5 + 4])

    return output_str


def compare_links(mch_links, ipmimanager_links):
    mch_links_comp = ""
    for s in mch_links:
        mch_links_comp += ''.join(s.split())

    ipmimanager_links_comp = ''.join(ipmimanager_links.split())

    return mch_links_comp == ipmimanager_links_comp


def test_mtca_crate(config_file, configparser, section):
    mch_address = configparser[section]['MCH_IP']
    MCHConn = mch_conn.MCHTelnetConn(mch_address)

    mdReportFile = MdUtils(file_name=section+'_test_report_'+time.strftime("%Y%m%d-%H%M%S"), title='Test report for EPICS 4 IPMI')
    mdReportFile.new_header(level=1, title='Configuration')  # style is set 'atx' format by default.

    cf = open(config_file)
    config_file_content = cf.read()
    cf.close()

    mdReportFile.insert_code(config_file_content)

    mdReportFile.new_header(level=1, title='Tests')
    try:
        # Read fru list from MCH
        fru_list_mch = MCHConn.get_fru_list()

        # Read fru list from ipmimanager
        fru_list_ipmimanager = e4i_common.create_fru_map(config_file)

        print("===================================================================")
        print("FRU List")
        print("-------------------------------------------------------------------")
        mdReportFile.new_header(level=2, title='FRU List')
        report_out = ""
        summary_fru_list = True
        for fru in fru_list_mch:
            print("M " + str(fru))
            report_out += "M " + str(fru) + "\n"

            if fru.fru_id in sorted(fru_list_ipmimanager[section]):
                fru_pv_prefix = fru_list_ipmimanager[section][fru.fru_id]
                fru_str = get_str_fru_from_ipmimanager(fru.fru_id, fru_pv_prefix)
                print("E " + fru_str)
                report_out += "E " + fru_str + "\n"
            else:
                if fru.fru_id != 0 and fru.fru_id != 3:
                    summary_fru_list = False
            print("")
            report_out += "\n"
        mdReportFile.insert_code(report_out)
        print("===================================================================")

        # SENSORS
        summary_sensors_status_all = []
        summary_sensors_count_all = []
        for fru in fru_list_mch:
            summary_sensors_status = True
            print("\n===================================================================")
            print("Sensors List for FRU {:}".format(fru.fru_id))
            print("-------------------------------------------------------------------")
            mdReportFile.new_header(level=2, title="Sensors List for FRU {:}".format(fru.fru_id))
            report_out = ""

            # Read sensors list from MCH
            sensor_info_mch = sorted(MCHConn.get_sensor_info(fru.fru_id))

            # Read sensors list from ipmimanager
            sensor_info_ipmimanager = []
            if fru.fru_id in fru_list_ipmimanager[section]:
                fru_pv_prefix = fru_list_ipmimanager[section][fru.fru_id]
                sensor_info_ipmimanager = sorted(get_sensors_from_ipmimanager(fru_pv_prefix))

            summary_sensors_count_all.append([len(sensor_info_mch), len(sensor_info_ipmimanager)])

            ipmimanager_list_used_indexes = []
            for idx, sensor_mch in enumerate(sensor_info_mch):
                sensor_str = get_str_sensor_from_mch(sensor_mch)
                print("M " + sensor_str)
                report_out += "M " + sensor_str + "\n"

                if sensor_info_ipmimanager:
                    idx = find_sensor_in_ipmimanager_list(sensor_mch.name, sensor_mch.sensor_type, sensor_info_ipmimanager)
                    if idx is not None:
                        ipmimanager_list_used_indexes.append(idx)
                        sensor_str = get_str_sensor_from_ipmimanager(sensor_info_ipmimanager[idx])
                        print("E " + sensor_str)
                        report_out += "E " + sensor_str + "\n"
                    else:
                        print("E")
                        report_out += "E\n"
                        summary_sensors_status = False

                print("")
                report_out += "\n"

            if sensor_info_ipmimanager:
                if len(ipmimanager_list_used_indexes) != len(sensor_info_ipmimanager):
                    summary_sensors_status = False
                    ipmimanager_not_printed = [s for i, s in enumerate(sensor_info_ipmimanager) if i not in ipmimanager_list_used_indexes]
                    for idx, sensor_ipmimanager in enumerate(ipmimanager_not_printed):
                        sensor_str = get_str_sensor_from_ipmimanager(sensor_ipmimanager)
                        print("M\nE " + sensor_str + "\n")
                        report_out += "M\nE " + sensor_str + "\n"

            summary_sensors_status_all.append(summary_sensors_status)
            mdReportFile.insert_code(report_out)


        # LINK STATE
        print("===================================================================")
        print("Link States")
        print("-------------------------------------------------------------------")
        print("MCH")
        print("-------------------------------------------------------------------")
        mdReportFile.new_header(level=2, title="Link States")
        mdReportFile.new_header(level=3, title="MCH")

        mch_links = MCHConn.get_link_state()
        report_out = ""
        for s in mch_links:
            print(s)
            report_out += s + "\n"
        mdReportFile.insert_code(report_out)

        print("-------------------------------------------------------------------")
        print("ipmimanager")
        print("-------------------------------------------------------------------")
        mdReportFile.new_header(level=3, title="ipmimanager")

        links = e4i_common.get_links_state(config_file)
        links_str = get_str_links_from_ipmimanager(links[section])
        print(links_str)
        mdReportFile.insert_code(links_str)
        summary_links = compare_links(mch_links, links_str)
        print("===================================================================")

        # SUMMARY
        print("\n\n\n===================================================================")
        print("SUMMARY")
        print("===================================================================")
        report_out = ""
        report_out += "1. Detected FRUs - MCH: {:}, ipmimanager: {:} [{:}]".format(len(fru_list_mch), len(fru_list_ipmimanager), "PASSED" if summary_fru_list else "FAILED") + "\n"
        report_out += "2. Detected sensors" + "\n"
        for i, fru in enumerate(fru_list_mch):
            if fru.fru_id == 0 or fru.fru_id == 3:
                continue
            report_out += "\t{:}) MCH: {:}, ipmimanager: {:} [{:}]".format(fru.fru_id, summary_sensors_count_all[i][0], summary_sensors_count_all[i][1], "PASSED" if summary_sensors_status_all[i] else "FAILED") + "\n"
        report_out += "3. AMC link states [{}]".format("PASSED" if summary_fru_list else "FAILED") + "\n"
        print(report_out)
        print("===================================================================")

        mdReportFile.new_header(level=1, title='Summary')
        mdReportFile.insert_code(report_out)
        mdReportFile.create_md_file()

    except Exception as err:
       print("Exception: {:}".format(err))
       mdReportFile.insert_code(report_out)
       mdReportFile.create_md_file()
       exit(-1)


if __name__ == '__main__':
    config_file = e4i_common.DEFAULT_CONFIG_FILE

    parser = argparse.ArgumentParser(description='Compare e3-ipmimanager with MCH console output')
    parser.add_argument('-c', '--config_file', type=str, help='Configuration file')
    args = parser.parse_args()

    if args.config_file:
        config_file = args.config_file

    config = configparser.ConfigParser()
    config.read(config_file)
    for section in config.sections():
        if section.startswith("MTCA_CRATE_"):
            test_mtca_crate(config_file, config, section)
