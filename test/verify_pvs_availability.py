#!/usr/bin/env python

import sys
sys.path.append("..")

import epics
from utils import ipmimanager_common as e4i_common
import configparser
import argparse

tested_pvs_count = 0
passed_pvs_count = 0


def test_global_pvs(config_file=None):
	config = configparser.ConfigParser()
	config.read(config_file)

	ioc_prefix = config['IOC']['PREFIX']
	for section in config.sections():
		if section.startswith("MTCA_CRATE_"):
			ioc_settings_prefix = config[section]['PV_PREFIX_GLOBAL_SETTINGS']

	epics_pvs = []
	pvsToRead = 0
	for suffix in e4i_common.ALL_PVSUFFIXES_FOR_TESTS_GLOBAL:
		epics_pvs.append(epics.PV(ioc_prefix + ioc_settings_prefix + suffix))
		pvsToRead += 1

	pv_values = [p.get(timeout=1) for p in epics_pvs]
	# print(pv_values)
	pvsReadOK = sum(1 for val in pv_values if val != None)
	return [pvsReadOK, pvsToRead]


def test_chassis_pvs(config_file=None):
	config = configparser.ConfigParser()
	config.read(config_file)

	ioc_prefix = config['IOC']['PREFIX']
	for section in config.sections():
		if section.startswith("MTCA_CRATE_"):
			crate_prefix = config[section]['PV_PREFIX_MTCA_CRATE']

	epics_pvs = []
	pvsToRead = 0
	for suffix in e4i_common.ALL_PVSUFFIXES_FOR_TESTS_CHASSIS:
		epics_pvs.append(epics.PV(ioc_prefix + crate_prefix + suffix))
		pvsToRead += 1

	pv_values = [p.get(timeout=1) for p in epics_pvs]
	# print(pv_values)
	pvsReadOK = sum(1 for val in pv_values if val != None)
	return [pvsReadOK, pvsToRead]


def test_fru_pvs(present_fru_dict, fru_id):
	fru_pv_prefix = present_fru_dict[int(fru_id)]
	epics_pvs = []
	pvsToRead = 0
	for suffix in e4i_common.ALL_PVSUFFIXES_FOR_TESTS_FRU:
		epics_pvs.append(epics.PV(fru_pv_prefix + suffix))
		pvsToRead += 1

	pv_values = [p.get(timeout=1) for p in epics_pvs]
	# print(pv_values)
	pvsReadOK = sum(1 for val in pv_values if val != None)
	return [pvsReadOK, pvsToRead]


def test_sensors_pvs(present_fru_dict, fru_id):
	fru_pv_prefix = present_fru_dict[int(fru_id)]
	epics_pvs = []
	pvsToRead = 0
	sensor_cnt = epics.caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUSENSORCNT)
	for s in range(0, sensor_cnt):
		for suffix in e4i_common.ALL_PVSUFFIXES_FOR_TESTS_SENSOR:
			epics_pvs.append(epics.PV(fru_pv_prefix + "Sen" + str(s) + suffix))
			pvsToRead += 1

		for suffix in e4i_common.HIDDEN_PVSUFFIXES_FOR_TESTS_SENSOR:
			pv_name = fru_pv_prefix + "Sen" + str(s) + suffix
			k = pv_name.rfind(":")
			new_pv_name = pv_name[:k] + ":#" + pv_name[k + 1:]
			epics_pvs.append(epics.PV(new_pv_name))
			pvsToRead += 1

	pv_values = [p.get(timeout=1) for p in epics_pvs]
	# print(pv_values)
	pvsReadOK = sum(1 for val in pv_values if val != None)
	return [pvsReadOK, pvsToRead]


if __name__ == "__main__":
	config_file = e4i_common.DEFAULT_CONFIG_FILE

	parser = argparse.ArgumentParser(description='Test availability of PVs')
	parser.add_argument('-c', '--config_file', type=str, help='Configuration file')
	args = parser.parse_args()

	if args.config_file:
		config_file = args.config_file

	tested_pvs_count = 0		# all presence PVs in create_fru_map
	passed_pvs_count = 0

	print("")
	print("ipmimanager - full tests:")
	print("==========================================")

	fru_map = e4i_common.create_fru_map(config_file)
	for crate in fru_map:
		print(crate)
		print("==========================================")

		print("Global settings testing: ", end='')
		[pvs_ok, pvs_all] = test_global_pvs(config_file)
		tested_pvs_count += pvs_all
		passed_pvs_count += pvs_ok
		print("{:}/{:} PVs".format(pvs_ok, pvs_all))

		print("Chassis testing: ", end='')
		[pvs_ok, pvs_all] = test_chassis_pvs(config_file)
		tested_pvs_count += pvs_all
		passed_pvs_count += pvs_ok
		print("{:}/{:} PVs".format(pvs_ok, pvs_all))

		for fru_id in sorted(fru_map[crate]):
			print("FRU {:} testing: ".format(fru_id), end='')
			[pvs_ok, pvs_all] = test_fru_pvs(fru_map[crate], fru_id)
			tested_pvs_count += pvs_all
			passed_pvs_count += pvs_ok
			print("{:}/{:} PVs".format(pvs_ok, pvs_all))

			sensor_cnt = epics.caget(fru_map[crate][fru_id] + e4i_common.PVSUFFIX_FRUSENSORCNT)
			print("\tSensors - {:}: ".format(sensor_cnt), end='')
			[pvs_ok, pvs_all] = test_sensors_pvs(fru_map[crate], fru_id)
			tested_pvs_count += pvs_all
			passed_pvs_count += pvs_ok
			print("{:}/{:} PVs".format(pvs_ok, pvs_all))

	print("==========================================")
	print("Results - {:}/{:}".format(passed_pvs_count, tested_pvs_count))
