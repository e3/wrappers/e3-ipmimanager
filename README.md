# e3-ipmimanager

## Introduction

This module was developed in order to integrate uTCA chassis management infrastructure into the standard control system (EPICS). It allows easy access to sensor readouts, board information and status. Moreover it allows control over the basic parameters settable via IPMI layer.

More detailed description can be found here [link](docs/DESIGN.md)

## Package dependencies

```sh
sudo yum install -y libtool python-devel
```

## Module dependencies

e3-ipmimanager relies on:
 * asyn
 * streamdevice
 * openipmi

## Install

The ipmiManager module was developed under e3 environment and can be installed using standard [e3](https://gitlab.esss.lu.se/e3/e3) mechanisms;

Verifying installation:
1. Go to `configure` and modify configuration:
    1.1. Check `RELEASE` and confirm that `EPICS_BASE` path and `E3_REQUIRE_VERSION` are correct, if not, create `RELEASE.local` file where these variables have proper values (one can find them, in the information displayed after logging in.
    1.2. Check `CONFIG_MODULE` file, if needed, modify `E3_MODULE_VERSION` (for example when one wants to keep old, already installed versions of ipmimanager), the rest of dependencies (asyn, stream and openipmi should be fine)
2. Go to top directory and execute:
```sh
make init build install
```
3. To confirm that the proper e3-ipmimanager version has been installed one can verify it can be found in <epics_base_path>/require/5.1.0/siteMods/ipmimanager -> there should be directory named the same as `E3_MODULE_VERSION`

## Generating JSON, DB and OPI files

Once we've built the ipmiManager we can use the cli built to generate first json and afterwards db and opi files.
We can use them to connect to a chassis and monitor it via Phoebus.

Detailed description can be found [here](docs/CONFIG_GENERATION.md)

## Notes

MCH firmware verification is provided by the IOC. In case of an unsupported FW version, the IOC issues a warning message.

**N.B.!** The only supported MCH version is V2.21.8, and *ipmi compatibility mode* has to be activated (which can be done using the integrated web application).
