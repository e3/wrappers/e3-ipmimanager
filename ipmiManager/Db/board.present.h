
#define DEVICE_STRING(_SLOT,_NAME) @asyn($(PORT),$(ADDR),$(TIMEOUT))s=$(_SLOT)._NAME

#define CREATE_AND_SET_INT(SLOT, NAME, VALUE) %               \
record(longin, &$(PREFIX)$(INDEX)$(OPT)NAME&)%    \
{%                                                            \
   field(DTYP, "asynInt32")%                                  \
   field(INP,  &DEVICE_STRING(SLOT,NAME)&)%                   \
   field(SCAN, "I/O Intr")%                                   \
}

#define CREATE_AND_GET_INT(SLOT, NAME, VALUE) %               \
record(longout, &$(PREFIX)$(INDEX)$(OPT)NAME&)%    \
{%                                                            \
   field(DTYP, "asynInt32")%                                  \
   field(OUT,  &DEVICE_STRING(SLOT,NAME)&)%                   \
}

#define CREATE_AND_SET_DBL(SLOT, NAME, VALUE) %               \
record(ai, &$(PREFIX)$(INDEX)$(OPT)NAME&)%       \
{%                                                            \
   field(DTYP, "asynFloat64")%                                \
   field(INP,  &DEVICE_STRING(SLOT,NAME)&)%                   \
   field(SCAN, "I/O Intr")%                                   \
}

#define CREATE_AND_SET_OCT(SLOT, NAME, VALUE) %               \
record(stringin, &$(PREFIX)$(INDEX)$(OPT)NAME&)%  \
{%                                                            \
   field(DTYP, "asynOctetRead")%                              \
   field(INP,  &DEVICE_STRING(SLOT,NAME)&)%                   \
   field(SCAN, "I/O Intr")%                                   \
}
