#include "sensor.h"

SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, EvtrdType , sensors[i].evtrdtype ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, SdrType   , sensors[i].sdrtype   ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, P         , sensors[i].p         ) ;
SENS_CREATE_AND_SET_OCT( sensors[i].slot, sensors[i].index, Name      , sensors[i].name      ) ;
SENS_CREATE_AND_SET_OCT( sensors[i].slot, sensors[i].index, Type      , sensors[i].type      ) ;
SENS_CREATE_AND_SET_OCT( sensors[i].slot, sensors[i].index, Units     , sensors[i].units     ) ;
SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, NormMax   , sensors[i].normmax   ) ;
SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, NormMin   , sensors[i].normmin   ) ;

// thresholds
SENS_CREATE_AND_GET_INT( sensors[i].slot, sensors[i].index, ThrCommit        , 0 ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, ThrUpdtAllow , sensors[i].thrUpdateAllowed ) ;

SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, ThrUpNonRec , sensors[i].thrupnonrecval  ) ;
SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, ThrUpCrt   , sensors[i].thrupcritval    ) ;
SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, ThrUpNonCrt, sensors[i].thrupnoncritval ) ;
SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, ThrLoNonRec , sensors[i].thrlononrecval  ) ;
SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, ThrLoCrt   , sensors[i].thrlocritval    ) ;
SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, ThrLoNonCrt, sensors[i].thrlononcritval ) ;

// setpoints for thresholds
SENS_CREATE_AND_GET_DBL( sensors[i].slot, sensors[i].index, ThrUpNonRec-SP , sensors[i].thrupnonrecval  ) ;
SENS_CREATE_AND_GET_DBL( sensors[i].slot, sensors[i].index, ThrUpCrt-SP   , sensors[i].thrupcritval    ) ;
SENS_CREATE_AND_GET_DBL( sensors[i].slot, sensors[i].index, ThrUpNonCrt-SP, sensors[i].thrupnoncritval ) ;
SENS_CREATE_AND_GET_DBL( sensors[i].slot, sensors[i].index, ThrLoNonRec-SP , sensors[i].thrlononrecval  ) ;
SENS_CREATE_AND_GET_DBL( sensors[i].slot, sensors[i].index, ThrLoCrt-SP   , sensors[i].thrlocritval    ) ;
SENS_CREATE_AND_GET_DBL( sensors[i].slot, sensors[i].index, ThrLoNonCrt-SP, sensors[i].thrlononcritval ) ;

SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, ThrUpNonRecStat , sensors[i].thrupnonrecstate  ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, ThrUpCrtStat   , sensors[i].thrupcritstate    ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, ThrUpNonCrtStat, sensors[i].thrupnoncritstate ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, ThrLoNonRecStat , sensors[i].thrlononrecstate  ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, ThrLoCrtStat   , sensors[i].thrlocritstate    ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, ThrLoNonCrtStat, sensors[i].thrlononcritstate ) ;
SENS_CREATE_AND_SET_INT( sensors[i].slot, sensors[i].index, ThrNotActual, 0 ) ;

SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, HystPos   , sensors[i].hystpos  ) ;
SENS_CREATE_AND_SET_DBL( sensors[i].slot, sensors[i].index, HystNeg   , sensors[i].hystneg  ) ;

// Pass thresholds to alarm fields records
PASSTHRLOCRITVAL_CREATE() ;
PASSTHRLONONCRITVAL_CREATE() ;
PASSTHRUPNONCRITVAL_CREATE() ;
PASSTHRUPCRITVAL_CREATE() ;

// value params and other runtime params
SENS_CREATE_AND_SET_DBL_VALUE( sensors[i].slot, sensors[i].index, Value, sensors[i].value ) ;

// pass unit to egu field
SENS_EGU_CREATE() ;
SENS_WAVE_EGU_CREATE() ;

// waveform for archiving sensor values
SENS_CREATE_WAVE( sensors[i].slot, sensors[i].index, Wave ) ;
