#include "board.present.h"

CREATE_AND_SET_INT( boards[i].slot, SensorCnt  , boards[i].sensorcnt ) ;
CREATE_AND_SET_INT( boards[i].slot, P          , boards[i].p         ) ;
CREATE_AND_SET_INT( boards[i].slot, State      , boards[i].state     ) ;
CREATE_AND_SET_OCT( boards[i].slot, FruName    , boards[i].fruname   ) ;
CREATE_AND_SET_INT( boards[i].slot, FruId      , boards[i].fruid     ) ;
CREATE_AND_SET_INT( boards[i].slot, Slot       , boards[i].slot      ) ;
CREATE_AND_SET_OCT( boards[i].slot, BManuf     , boards[i].bmanuf    ) ;
CREATE_AND_SET_OCT( boards[i].slot, BName      , boards[i].bname     ) ;
CREATE_AND_SET_OCT( boards[i].slot, BSN        , boards[i].bsn       ) ;
CREATE_AND_SET_OCT( boards[i].slot, BPN        , boards[i].bpn       ) ;
CREATE_AND_SET_OCT( boards[i].slot, BFruFileId , boards[i].bfrufileid) ;
CREATE_AND_SET_OCT( boards[i].slot, PManuf     , boards[i].pmanuf    ) ;
CREATE_AND_SET_OCT( boards[i].slot, PName      , boards[i].pname     ) ;
CREATE_AND_SET_OCT( boards[i].slot, PN         , boards[i].pn        ) ;
CREATE_AND_SET_OCT( boards[i].slot, PPV        , boards[i].ppv       ) ;
CREATE_AND_SET_OCT( boards[i].slot, PSN        , boards[i].psn       ) ;
CREATE_AND_SET_OCT( boards[i].slot, PAT        , boards[i].pat       ) ;
CREATE_AND_SET_OCT( boards[i].slot, PFruFileId , boards[i].pfrufileid) ;
CREATE_AND_GET_INT( boards[i].slot, FState     , boards[i].fstate ) ;
