
#define DEVICE_STRING(_NAME) @asyn($(PORT),$(ADDR),$(TIMEOUT))s=$(SLOT).s=$(SINDEX).=_NAME


#define SENS_CREATE_AND_SET_INT(SLOT, SENS, NAME, VALUE) %               \
record(longin, &$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)NAME&)%    \
{%                                                            \
   field(DTYP, "asynInt32")%                                  \
   field(INP,  &DEVICE_STRING(NAME)&)%                   \
   field(SCAN, "I/O Intr")%                                   \
   field(DESC, "$(DESC)")%                                      \
}

#define SENS_CREATE_AND_SET_DBL(SLOT, SENS, NAME, VALUE) %               \
record(ai, &$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)NAME&)%       \
{%                                                            \
   field(DTYP, "asynFloat64")%                                \
   field(INP,  &DEVICE_STRING(NAME)&)%                   \
   field(SCAN, "I/O Intr")%                                   \
   field(DESC, "$(DESC)")%                                      \
}

#define SENS_CREATE_AND_SET_OCT(SLOT, SENS, NAME, VALUE) %               \
record(stringin, &$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)NAME&)%  \
{%                                                            \
   field(DTYP, "asynOctetRead")%                              \
   field(INP,  &DEVICE_STRING(NAME)&)%                   \
   field(SCAN, "I/O Intr")%                                   \
   field(DESC, "$(DESC)")%                                      \
}


#define SENS_CREATE_AND_GET_INT(SLOT, SENS, NAME, VALUE) %               \
record(longout, &$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)NAME&)%    \
{%                                                            \
   field(DTYP, "asynInt32")%                                  \
   field(OUT,  &DEVICE_STRING(NAME)&)%                   \
   field(DESC, "$(DESC)")%                                      \
}

#define SENS_CREATE_AND_GET_DBL(SLOT, SENS, NAME, VALUE) %               \
record(ao, &$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)NAME&)%       \
{%                                                            \
   field(DTYP, "asynFloat64")%                                \
   field(OUT,  &DEVICE_STRING(NAME)&)%                   \
   field(DESC, "$(DESC)")%                                      \
}

#define PASSTHRUPCRITVAL_CREATE() %               \
record(ao, &$(PREFIX)$(INDEX)?$(OPT)Sen$(SINDEX)PassThrUpCrt&)%       \
{%                                                            \
   field(DOL, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)ThrUpCrt CP")%                                \
   field(OMSL, "closed_loop")%                   \
   field(OUT, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)Value.HIHI PP")%                                 \
   field(DESC, "$(DESC)")%                                      \
}

#define PASSTHRUPNONCRITVAL_CREATE() %               \
record(ao, &$(PREFIX)$(INDEX)?$(OPT)Sen$(SINDEX)PassThrUpNonCrt&)%       \
{%                                                            \
   field(DOL, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)ThrUpNonCrt CP")%                                \
   field(OMSL, "closed_loop")%                   \
   field(OUT, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)Value.HIGH PP")%                                 \
   field(DESC, "$(DESC)")%                                      \
}

#define PASSTHRLONONCRITVAL_CREATE() %               \
record(ao, &$(PREFIX)$(INDEX)?$(OPT)Sen$(SINDEX)PassThrLoNonCrt&)%       \
{%                                                            \
   field(DOL, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)ThrLoNonCrt CP")%                                \
   field(OMSL, "closed_loop")%                   \
   field(OUT, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)Value.LOW PP")%                                 \
   field(DESC, "$(DESC)")%                                      \
}

#define PASSTHRLOCRITVAL_CREATE() %               \
record(ao, &$(PREFIX)$(INDEX)?$(OPT)Sen$(SINDEX)PassThrLoCrt&)%       \
{%                                                            \
   field(DOL, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)ThrLoCrt CP")%                                \
   field(OMSL, "closed_loop")%                   \
   field(OUT, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)Value.LOLO PP")%                                 \
   field(DESC, "$(DESC)")%                                      \
}

#define SENS_CREATE_AND_SET_DBL_VALUE(SLOT, SENS, NAME, VALUE) %               \
record(ai, &$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)NAME&)%       \
{%                                                            \
   field(DTYP, "asynFloat64")%                                \
   field(INP,  &DEVICE_STRING(NAME)&)%                   \
   field(SCAN, "I/O Intr")%                                   \
   field(HHSV, "MAJOR")%            \
   field(HSV, "MINOR")%           \
   field(LLSV, "MAJOR")%            \
   field(LSV, "MINOR")%           \
   field(DESC, "$(DESC)")%                                      \
}

#define SENS_EGU_CREATE()  %               \
record(stringout, &$(PREFIX)$(INDEX)?$(OPT)Sen$(SINDEX)PassEgu&)%    \
{%                                                           \
   field(DOL, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)Units CP")%         \
   field(OMSL, "closed_loop")%              \
   field(OUT, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)Value.EGU PP")%              \
   field(DESC, "$(DESC)")%                                      \
}

#define SENS_WAVE_EGU_CREATE()  %               \
record(stringout, &$(PREFIX)$(INDEX)?$(OPT)Sen$(SINDEX)PassWaveEgu&)%    \
{%                                                           \
   field(DOL, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)Units CP")%         \
   field(OMSL, "closed_loop")%              \
   field(OUT, "$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)Wave.EGU PP")%              \
   field(DESC, "$(DESC)")%                                      \
}

#define SENS_CREATE_WAVE(SLOT, SENS, NAME) %	\
record(waveform, &$(PREFIX)$(INDEX)$(OPT)Sen$(SINDEX)NAME&)%	\
{%					\
   field(DTYP, "asynFloat64ArrayIn")%	\
   field(INP, &DEVICE_STRING(NAME)&)%	\
   field(NELM, "$(ARCHIVER_SIZE)")%			\
   field(FTVL, "DOUBLE")%			\
   field(SCAN, "I/O Intr")%		\
   field(DESC, "$(DESC)")%                                      \
}
