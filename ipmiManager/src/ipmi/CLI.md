e3-ipmimanager
======
ESS Site-specific EPICS module : ipmimanager

# Introduction

Command line tool is using ipmiManager (the same code base which is used on Epics level) to provide simple user interface, which can be used for quick testing or debugging of IPMI functionality.

# Installation

This utility is installed upon installing e3-ipmimanager locally.
When the module is built and installed, you can start using this tool.

# Command Line Parameters

Several command line parameters can be specified:

   * -h <MCH_ADDR> - specify MCH address, to which tool will connect
   * -d - show device tree (presents boards visible in the system together with sensors)
   * -s <INTERVAL> - specify sensor refresh interval ( default 10s)
   * -t <INTERVAL> - specify threshold refresh interval ( default 10s)
   * -e <INTERVAL> - specify event refresh interval ( default 10s)
   * -f <FRU> - specify FRU ID number for further operations
   * -a <ACTION> - specify 0 to shutdown selected FRU, 1 to start-up selected FRU
   * -R - keep connection alvie without refreshing of the sensors
   * -r - keep connection alive and refresh sesnors with interval
   * -j <JSON_FILENAME>  - generate a configuration file that will be saved to the specified file


# Typical usage

* ipmiMonitor -h 10.1.3.144 -f 5 -a 0

Connect to MCH at 10.1.3.144 and shutdown FRU 5

* ipmiMonitor -h 10.1.3.144 -d -R

Connect to MCH at 10.1.3.144, dump information about detected boards and sensors and run main loop without sensor refresh


# Extensions

If some application specific testing is needed, ipmiMonitor can be extended by custom functions, which are run by ipmiMonitor while executing run function (executed by -R switch)


checkSync is example function, which is passed to runNoPoll method by pointer. The code can be modified to use user provided function.

    if ( runLoop == 2 )
    {
        cout << "Running main loop..." << endl ;
        monitor.runNoPoll( 0, 0, (int*)&terminateLoop, 0, checkSync ) ;
        cout << "Exitting main loop..." << endl ;
    }
