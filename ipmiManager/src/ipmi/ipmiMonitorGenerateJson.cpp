#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "ipmiMonitor.h"

int ipmiMonitor::generateJson(const char* name, int fru) {
  FILE* fd = stdout;

  if (name[0] != 0) {
    fd = fopen(name, "w+");

    if (fd == NULL) return 0;
  }

  fprintf(fd, "{\n\"utca\": {\n");
  fprintf(fd, "  \"ip\": \"%s\",\n", address);
  fprintf(fd, "  \"boards\": [\n");

  for (int i = 0; i < boardCount; i++) {
    if ((fru == -1) || (boards[i].fruid == fru)) {
      int sens = 0;

      fprintf(fd, "  {\n");
      fprintf(fd, "    \"sensorcnt\":     \"%d\",\n", boards[i].sensorcnt);
      fprintf(fd, "    \"fruname\":       \"%s\",\n", boards[i].fruname);
      fprintf(fd, "    \"fruid\":         \"%d\",\n", boards[i].fruid);
      fprintf(fd, "    \"slot\":          \"%d\",\n", boards[i].slot);
      fprintf(fd, "    \"bmanuf\":        \"%s\",\n", boards[i].bmanuf);
      fprintf(fd, "    \"bname\":         \"%s\",\n", boards[i].bname);
      fprintf(fd, "    \"bsn\":           \"%s\",\n", boards[i].bsn);
      fprintf(fd, "    \"bpn\":           \"%s\",\n", boards[i].bpn);
      fprintf(fd, "    \"bfrufileid\":    \"%s\",\n", boards[i].bfrufileid);
      fprintf(fd, "    \"pmanuf\":        \"%s\",\n", boards[i].pmanuf);
      fprintf(fd, "    \"pname\":         \"%s\",\n", boards[i].pname);
      fprintf(fd, "    \"pn\":            \"%s\",\n", boards[i].pn);
      fprintf(fd, "    \"ppv\":           \"%s\",\n", boards[i].ppv);
      fprintf(fd, "    \"psn\":           \"%s\",\n", boards[i].psn);
      fprintf(fd, "    \"pat\":           \"%s\",\n", boards[i].pat);
      fprintf(fd, "    \"pfrufileid\":    \"%s\",\n", boards[i].pfrufileid);

      fprintf(fd, "    \"sensors\": [\n");
      for (int j = 0; j < sensorCount; j++) {
        if (sensors[j].parentBoard == i) {
          sens++;
          fprintf(fd, "     {\n");
          fprintf(fd, "       \"index\":              \"%d\",\n",
                  sensors[j].index);
          fprintf(fd, "       \"evtrdtype\":          \"0x%X\",\n",
                  sensors[j].evtrdtype);
          fprintf(fd, "       \"sdrtype\":            \"0x%X\",\n",
                  sensors[j].sdrtype);
          fprintf(fd, "       \"p\":                  \"%d\",\n", sensors[j].p);
          fprintf(fd, "       \"name\":               \"%s\",\n",
                  sensors[j].name);
          fprintf(fd, "       \"type\":               \"%s\",\n",
                  sensors[j].type);
          fprintf(fd, "       \"units\":              \"%s\",\n",
                  sensors[j].units);
          fprintf(fd, "       \"normmax\":            \"%f\",\n",
                  sensors[j].normmax);
          fprintf(fd, "       \"normmin\":            \"%f\",\n",
                  sensors[j].normmin);
          fprintf(fd, "       \"thrupnonrecval\":     \"%f\",\n",
                  sensors[j].thrupnonrecval);
          fprintf(fd, "       \"thrupcritval\":       \"%f\",\n",
                  sensors[j].thrupcritval);
          fprintf(fd, "       \"thrupnoncritval\":    \"%f\",\n",
                  sensors[j].thrupnoncritval);
          fprintf(fd, "       \"thrlononrecval\":     \"%f\",\n",
                  sensors[j].thrlononrecval);
          fprintf(fd, "       \"thrlocritval\":       \"%f\",\n",
                  sensors[j].thrlocritval);
          fprintf(fd, "       \"thrlononcritval\":    \"%f\",\n",
                  sensors[j].thrlononcritval);
          fprintf(fd, "       \"thrAccess\":          \"0x%X\",\n",
                  sensors[j].thrAccess);
          fprintf(fd, "       \"thrUpdateAllowed\":   \"0x%X\",\n",
                  sensors[j].thrUpdateAllowed);
          fprintf(fd, "       \"hystpos\":            \"%f\",\n",
                  sensors[j].hystpos);
          fprintf(fd, "       \"hystneg\":            \"%f\" \n",
                  sensors[j].hystneg);
          fprintf(fd, "     }%c\n\n", boards[i].sensorcnt == sens ? ' ' : ',');
        }
      }
      fprintf(fd, "    ]\n");
      fprintf(fd, "  }%c\n\n\n",
              (fru != -1)               ? ' '
              : (i == (boardCount - 1)) ? ' '
                                        : ',');
    }
  }
  fprintf(fd, "]}\n}\n");

  if (name[0] != 0) {
    fclose(fd);
  }

  return 0;
}
