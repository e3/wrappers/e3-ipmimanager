#include <ctype.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <iostream>

#include "ipmiMonitor.h"

extern "C" {
int ipmi_oem_utca_init(void);
};

//
// Actions
//
void ipmiMonitor::actionError(int rv) {
  ipmiMonitorLogger log("ipmiMonitor::actionError");
  currentError = rv;
  os_handler_waiter_release(waiter);
}

void ipmiMonitor::domain_close() {
  ipmiMonitorLogger log("ipmiMonitor::domain_close");
  char ebuf[128];
  int rv;

  currentError = 0;
  os_handler_waiter_use(waiter);
  rv = ipmi_domain_pointer_cb(domain, global_domain_close, this);

  if (rv) {
    fprintf(stderr, "close ptr cb: %s\n",
            ipmi_get_error_string(rv, ebuf, sizeof(ebuf)));
    exit(1);
  }

  os_handler_waiter_wait(waiter, NULL);
}

void ipmiMonitor::indexEntity(ipmi_entity_t *entity) {
  ipmiMonitorLogger log("ipmiMonitor::indexEntity");
  board_info newBoard = {0};
  board_info *currentBoard = 0;

  int entity_id = ipmi_entity_get_entity_id(entity);
  int entity_channel = ipmi_entity_get_device_channel(entity);
  int entity_addr = ipmi_entity_get_device_address(entity);

  int entity_instance = ipmi_entity_get_entity_instance(entity);

  char entity_name[256] = {0};
  ipmi_entity_get_name(entity, entity_name, 256);

  ipmi_fru_t *fru = ipmi_entity_get_fru(entity);
  char fru_name[256] = {0};
  unsigned int fru_name_len = 16;
  unsigned int fru_id = 0;

  current_index = 0;

  // only care for the following device classes 10,30,192,193
  if ((entity_id == 30) && (entity_channel == 0) && (entity_addr != 0)) {
    if (fru != NULL) {
      ipmi_fru_get_board_info_board_product_name(fru, fru_name, &fru_name_len);
    }

    current_slot = 0x30 + current_cu++;
    fru_id = ipmi_entity_get_fru_device_id(entity);
    ipmi_entity_iterate_sensors(entity, global_iterate_sensors, this);
  } else if ((entity_id == 10) && (entity_channel == 0) && (entity_addr != 0)) {
    if (fru != NULL) {
      ipmi_fru_get_board_info_board_product_name(fru, fru_name, &fru_name_len);
    }

    current_slot = 0x20 + current_pm++;
    fru_id = ipmi_entity_get_fru_device_id(entity);
    ipmi_entity_iterate_sensors(entity, global_iterate_sensors, this);
  } else if (((entity_id == 192) || (entity_id == 193)) &&
             (entity_channel == 0) && (entity_addr != 0)) {
    if (fru != NULL) {
      ipmi_fru_get_board_info_board_product_name(fru, fru_name, &fru_name_len);
    }

    current_slot = entity_instance - 0x60 + ((entity_id == 193) ? 0x00 : 0x10);
    fru_id = (entity_id == 193) ? ipmi_entity_get_fru_device_id(entity)
                                : (89 + entity_instance - 0x60);
    ipmi_entity_iterate_sensors(entity, global_iterate_sensors, this);
  } else if ((entity_id == 194) && (entity_channel == 0) &&
             (entity_addr != 0) &&
             ((entity_instance == 0x63) || (entity_instance == 0x64))) {
    if (fru != NULL) {
      ipmi_entity_get_id(entity, fru_name, 64);
    }

    current_slot = entity_instance - 0x63 + 0x40;
    fru_id = ipmi_entity_get_fru_device_id(entity);
    ipmi_entity_iterate_sensors(entity, global_iterate_sensors, this);
  } else {
    return;
  }

  // register hot-swap callback on entity ( triggered by events )
  ipmi_entity_add_hot_swap_handler(entity, global_entity_hot_swap_event, this);

  // fill in board information structure
  if (fru == NULL) {
    return;
  }

  {
    // id of entity - used for lookups
    newBoard._id = ipmi_entity_convert_to_id(entity);
    newBoard.entityId = ipmi_entity_convert_to_id(entity);

    // boards[wd.num_boards].state = wd.currState ; // state is now filled in
    // callback
    newBoard.sensorcnt = current_index;
    newBoard.p = 1;

    ipmi_entity_get_id(entity, newBoard.fruname, 64);

    newBoard.fruid = fru_id;
    newBoard.slot = current_slot;

    {
      unsigned int len = 64;
      ipmi_fru_get_board_info_board_manufacturer(fru, newBoard.bmanuf, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_board_info_board_product_name(fru, newBoard.bname, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_board_info_board_serial_number(fru, newBoard.bsn, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_board_info_board_part_number(fru, newBoard.bpn, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_board_info_fru_file_id(fru, newBoard.bfrufileid, &len);
    };

    {
      unsigned int len = 64;
      ipmi_fru_get_product_info_manufacturer_name(fru, newBoard.pmanuf, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_product_info_product_name(fru, newBoard.pname, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_product_info_product_part_model_number(fru, newBoard.pn,
                                                          &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_product_info_product_version(fru, newBoard.ppv, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_product_info_product_serial_number(fru, newBoard.psn, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_product_info_asset_tag(fru, newBoard.pat, &len);
    };
    {
      unsigned int len = 64;
      ipmi_fru_get_product_info_fru_file_id(fru, newBoard.pfrufileid, &len);
    };

    newBoard.fstate = -1;

    currentBoard = addBoard(&newBoard);

    // get hot swap state
    {
      int rv;

      os_handler_waiter_use(waiter);
      rv = ipmi_entity_get_hot_swap_state(
          entity, global_entity_hot_swap_state_done, this);

      if (rv) {
        // for all boards which are not hot-pluggable or report other errors
        // assume state M4
        currentBoard->state = (ipmi_hot_swap_states)4;
        os_handler_waiter_release(waiter);
      }
      os_handler_waiter_wait(waiter, NULL);

      printLog(
          "Indexed board: %16s %16s   (fruId: %2d, state: %1d, sensors: %3d, "
          "slot: %3d)\n",
          entity_name, fru_name, currentBoard->fruid, currentBoard->state,
          currentBoard->sensorcnt, currentBoard->slot);
    }
  }
}

void ipmiMonitor::indexSensor(ipmi_sensor_t *sensor) {
  ipmiMonitorLogger log("ipmiMonitor::indexSensor");
  sensor_info newSensor = {0};

  if (sensorCount >= MAX_SENSORS) return;

  newSensor._id = ipmi_sensor_convert_to_id(sensor);
  newSensor._type = ipmi_sensor_get_event_reading_type(sensor);

  newSensor.slot = current_slot;
  newSensor.index = current_index;
  newSensor.parentBoard = boardCount;
  newSensor.evtrdtype = ipmi_sensor_get_event_reading_type(sensor);
  newSensor.sdrtype = 0;
  newSensor.p = 1;

  newSensor.arch = new std::vector<epicsFloat64>;
  newSensor.arch->reserve(newSensor.arch->size() + archiverSize);
  for (int i = 0; i < archiverSize; i++) {
    newSensor.arch->push_back(0);
  }

  ipmi_sensor_get_id(sensor, newSensor.name, 64);

  memcpy(newSensor.type, ipmi_sensor_get_sensor_type_string(sensor),
         strlen(ipmi_sensor_get_sensor_type_string(sensor)));

  {
    const char *base;
    const char *percent = "";

    const char *mod_use = "";
    const char *modifier = "";
    const char *rate;

    base = ipmi_sensor_get_base_unit_string(sensor);

    if (ipmi_sensor_get_percentage(sensor)) {
      percent = "%";
    }

    switch (ipmi_sensor_get_modifier_unit_use(sensor)) {
      case IPMI_MODIFIER_UNIT_NONE:
        break;

      case IPMI_MODIFIER_UNIT_BASE_DIV_MOD:
        mod_use = "/";
        modifier = ipmi_sensor_get_modifier_unit_string(sensor);
        break;

      case IPMI_MODIFIER_UNIT_BASE_MULT_MOD:
        mod_use = "*";
        modifier = ipmi_sensor_get_modifier_unit_string(sensor);
        break;
    }
    rate = ipmi_sensor_get_rate_unit_string(sensor);

    sprintf(newSensor.units, "%s%s%s%s%s", percent, base, mod_use, modifier,
            rate);
  }

  ipmi_sensor_get_normal_max(sensor, &newSensor.normmax);
  ipmi_sensor_get_normal_min(sensor, &newSensor.normmin);

  addSensor(&newSensor);

  // thresholds and hysteresis require interaction with hardware
  {
    // fetch thresholds for sensor
    int rv;

    os_handler_waiter_use(waiter);

    rv =
        ipmi_sensor_get_thresholds(sensor, global_sensor_thresholds_done, this);

    if (rv) {
      // probably thrsholds are not supported
      // fprintf(stderr, "fetch sensor: %s\n", ipmi_get_error_string(rv, ebuf,
      // sizeof(ebuf)));
      os_handler_waiter_release(waiter);
    }
    os_handler_waiter_wait(waiter, NULL);
  }

  {
    // fetch hysteresis for sensor
    int rv;

    os_handler_waiter_use(waiter);

    rv =
        ipmi_sensor_get_hysteresis(sensor, global_sensor_hysteresis_done, this);

    if (rv) {
      // probably hysteresis are not supported
      os_handler_waiter_release(waiter);
    }
    os_handler_waiter_wait(waiter, NULL);
  }

  // this params are not used in current concept
  // int ipmi_sensor_get_tolerance(ipmi_sensor_t *sensor,
  // int ipmi_sensor_get_accuracy(ipmi_sensor_t *sensor, int val, double
  // *accuracy);

  current_index++;
}

//
// Constructor
//
ipmiMonitor::ipmiMonitor(char *address, printLog_t pLog, os_vlog_t pLib,
                         int size) {
  char ebuf[128];
  int rv;
  ipmi_args_t *args;
  os_handler_waiter_factory_t *waiterf;
  ipmi_con_t *con;

  archiverSize = size;
  connected = 2;

  printLog = pLog;

  printLog("I'm monitor. I will connect to %s\n", address);

  strncpy(this->address, address, 256);

  this->eventSensorUpdateDone = 0;
  this->mutexSensorUpdateInProgress = 0;

  this->last_sens_update = 0;
  this->last_thresh_update = 0;
  this->last_event_update = 0;

  // clean sensor table
  memset(&sensors, 0, sizeof(sensors));
  this->sensorCount = 0;

  // clean board table
  memset(&boards, 0, sizeof(boards));
  this->boardCount = 0;

  current_slot = 0;
  current_index = 0;

  current_cu = 0;
  current_pm = 0;

  /* OS handler allocated first. */
  os_hnd = ipmi_posix_setup_os_handler();
  if (!os_hnd) {
    printLog("ipmi_smi_setup_con: Unable to allocate os handler\n");
    exit(1);
  }

  os_hnd->set_log_handler(os_hnd, pLib);

  /* Initialize the OpenIPMI library. */
  ipmi_init(os_hnd);
  ipmi_oem_utca_init();
  {
    int argc = 5;
    int curr_arg = 1;

    char dummy[] = "dummy";
    char lan[] = "lan";
    char opt0[] = "-A";
    char opt1[] = "none";

    char *argv[] = {dummy, lan, opt0, opt1, address};

    rv = ipmi_parse_args2(&curr_arg, argc, argv, &args);
    if (rv) {
      fprintf(stderr, "Error parsing command arguments, argument %d: %s\n",
              curr_arg, ipmi_get_error_string(rv, ebuf, sizeof(ebuf)));
      exit(1);
    }
  }

  rv = ipmi_args_setup_con(args, os_hnd, NULL, &con);
  if (rv) {
    fprintf(stderr, "ipmi_ip_setup_con: %s",
            ipmi_get_error_string(rv, ebuf, sizeof(ebuf)));
    exit(1);
  }

  rv = os_handler_alloc_waiter_factory(os_hnd, 0, 0, &waiterf);
  if (rv) {
    fprintf(stderr, "os_handler_alloc_waiter_factory: %s",
            ipmi_get_error_string(rv, ebuf, sizeof(ebuf)));
    exit(1);
  }

  waiter = os_handler_alloc_waiter(waiterf);

  if (!waiter) {
    fprintf(stderr, "os_handler_alloc_waiter: Out of memory");
    exit(1);
  }

  {
    ipmi_open_option_t opt[] = {
        {IPMI_OPEN_OPTION_ALL, {0}},
        {IPMI_OPEN_OPTION_SDRS, {1}},
        {IPMI_OPEN_OPTION_FRUS, {1}},
        {IPMI_OPEN_OPTION_SEL, {1}},
        {IPMI_OPEN_OPTION_OEM_INIT, {1}},
        {IPMI_OPEN_OPTION_SET_EVENT_RCVR, {0}},
        {IPMI_OPEN_OPTION_SET_SEL_TIME, {0}},
        {IPMI_OPEN_OPTION_ACTIVATE_IF_POSSIBLE, {0}},
    };

    rv = ipmi_open_domain("", &con, 1, global_domain_setup_done, this,
                          global_domain_fully_up, this, opt, 8, &domain);

    if (rv) {
      fprintf(stderr, "ipmi_init_domain: %s\n",
              ipmi_get_error_string(rv, ebuf, sizeof(ebuf)));
      exit(1);
    }
  }

  os_handler_waiter_wait(waiter, NULL);

  if (currentError) {
    fprintf(stderr, "Error starting connection: %s\n",
            ipmi_get_error_string(currentError, ebuf, sizeof(ebuf)));
    exit(1);
  }

  // we count all sensors and create list of ids
  ipmi_domain_pointer_cb(domain, global_iterate_entities, this);

  printLog("Total number of sensors to monitor: %d\n", sensorCount);

  syncStatus = 1;

  // initialize time stamps vector
  iniTimeStamps(archiverSize);
  printLog("Time stamps initialised\n");

  // initial setup for events
  ipmiEventHandlers = ipmi_event_handlers_alloc();

  ipmi_event_handlers_set_threshold(ipmiEventHandlers,
                                    global_analyze_threshold_events);
  ipmi_event_handlers_set_discrete(ipmiEventHandlers,
                                   global_analyze_discrete_events);
}

ipmiMonitor::~ipmiMonitor() {
  char ebuf[128];

  domain_close();

  if (currentError) {
    fprintf(stderr, "ipmi_domain_close: %s\n",
            ipmi_get_error_string(currentError, ebuf, sizeof(ebuf)));
  }

  os_hnd->free_os_handler(os_hnd);
}
