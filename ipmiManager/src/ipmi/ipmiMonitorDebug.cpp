#include <ctype.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <iostream>

#include "ipmiMonitor.h"

void ipmiMonitor::dumpSensor(int index, int mode) {
  ipmiMonitorLogger log("ipmiMonitor::dumpSensor");
}

void ipmiMonitor::dumpBoard(int index, int mode) {
  ipmiMonitorLogger log("ipmiMonitor::dumpBoard");
}

void ipmiMonitor::debug() {
  ipmiMonitorLogger log("ipmiMonitor::debug");
#ifdef STANDALONE
  int boardNum = -1;
  for (int i = 0; i < sensorCount; i++) {
    if (sensors[i].parentBoard != boardNum) {
      boardNum = sensors[i].parentBoard;
      printLog("board: %32s FRU ID: %4d slot: %4d sensors: %4d\n",
               boards[boardNum].fruname, boards[boardNum].fruid,
               boards[boardNum].slot, boards[boardNum].sensorcnt);
    }
    printLog(
        "\tsensor: %16s slot: %3d index: %3d(%3d) upd: %X value: %f(%d) %s\n",
        sensors[i].name, sensors[i].slot, sensors[i].index, i,
        sensors[i].thrUpdateAllowed, sensors[i].value, sensors[i].value_raw,
        sensors[i].units);
  }
#endif
}
