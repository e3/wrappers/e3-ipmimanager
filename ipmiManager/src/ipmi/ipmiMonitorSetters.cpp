#include <ctype.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <iostream>

#include "ipmiMonitor.h"

int ipmiMonitor::getSensor(ipmi_sensor_id_t id) {
  ipmiMonitorLogger log("ipmiMonitor::getSensor");
  int sensorIndex = 0;

  for (sensorIndex = 0; sensorIndex < sensorCount; sensorIndex++) {
    if (ipmi_cmp_sensor_id(sensors[sensorIndex]._id, id) == 0) {
      return sensorIndex;
    }
  }
  return -1;
}

int ipmiMonitor::getBoard(ipmi_entity_id_t id) {
  ipmiMonitorLogger log("ipmiMonitor::getBoard");
  int boardIndex = 0;

  for (boardIndex = 0; boardIndex < boardCount; boardIndex++) {
    if (ipmi_cmp_entity_id(boards[boardIndex]._id, id) == 0) {
      return boardIndex;
    }
  }
  return -1;
}

ipmi_entity_id_t ipmiMonitor::getBoardByFru(int fru) {
  ipmiMonitorLogger log("ipmiMonitor::getBoardByFru");
  int boardIndex = 0;
  ipmi_entity_id_t inv;

  ipmi_entity_id_set_invalid(&inv);

  for (boardIndex = 0; boardIndex < boardCount; boardIndex++) {
    if (boards[boardIndex].fruid == fru) {
      return boards[boardIndex]._id;
    }
  }
  return inv;
}

void ipmiMonitor::addSensor(sensor_info *sensor) {
  ipmiMonitorLogger log("ipmiMonitor::addSensor");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  memcpy(&(this->sensors[this->sensorCount]), sensor, sizeof(sensor_info));
  this->sensorCount++;

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

board_info *ipmiMonitor::addBoard(board_info *board) {
  ipmiMonitorLogger log("ipmiMonitor::addBoard");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  memcpy(&(this->boards[this->boardCount]), board, sizeof(board_info));
  this->boardCount++;

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);

  return &(this->boards[this->boardCount - 1]);
}

void ipmiMonitor::setSensorThresholdsRd(ipmi_sensor_id_t id, double *thrs,
                                        int *flags, int *states) {
  ipmiMonitorLogger log("ipmiMonitor::setSensorThresholdsRd");

  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  int sensorIndex = getSensor(id);

  if (sensorIndex > 0) {
    if (thrs != NULL) {
      sensors[sensorIndex].thrupnonrecval = thrs[0];
      sensors[sensorIndex].thrupcritval = thrs[1];
      sensors[sensorIndex].thrupnoncritval = thrs[2];
      sensors[sensorIndex].thrlononcritval = thrs[3];
      sensors[sensorIndex].thrlocritval = thrs[4];
      sensors[sensorIndex].thrlononrecval = thrs[5];
    }

    if (flags != NULL) {
      sensors[sensorIndex].thrAccess = flags[0];
      sensors[sensorIndex].thrUpdateAllowed = flags[1];
    }

    if (states != NULL) {
      sensors[sensorIndex].thrupnonrecstate = states[0];
      sensors[sensorIndex].thrupcritstate = states[1];
      sensors[sensorIndex].thrupnoncritstate = states[2];
      sensors[sensorIndex].thrlononcritstate = states[3];
      sensors[sensorIndex].thrlocritstate = states[4];
      sensors[sensorIndex].thrlononrecstate = states[5];
    }
  }

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::setSensorThresholdsSp(ipmi_sensor_id_t id, double *thrs) {
  ipmiMonitorLogger log("ipmiMonitor::setSensorThresholdsSp");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  int sensorIndex = getSensor(id);

  if (sensorIndex > 0) {
    if (thrs != NULL) {
      sensors[sensorIndex].thrupnonrecval_sp = thrs[0];
      sensors[sensorIndex].thrupcritval_sp = thrs[1];
      sensors[sensorIndex].thrupnoncritval_sp = thrs[2];
      sensors[sensorIndex].thrlononcritval_sp = thrs[3];
      sensors[sensorIndex].thrlocritval_sp = thrs[4];
      sensors[sensorIndex].thrlononrecval_sp = thrs[5];

      sensors[sensorIndex].thrNeedsUpdate = 0x3F;
    } else {
      sensors[sensorIndex].thrNeedsUpdate = 0x00;
    }
  }

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::setSensorHysteresisRd(ipmi_sensor_id_t id, double *hyst) {
  ipmiMonitorLogger log("ipmiMonitor::setSensorHysteresisRd");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  int sensorIndex = getSensor(id);

  if (sensorIndex > 0) {
    sensors[sensorIndex].hystpos = hyst[0];
    sensors[sensorIndex].hystneg = hyst[1];
  }

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::setSensorValueRd(ipmi_sensor_id_t id, double value,
                                   unsigned int value_raw) {
  ipmiMonitorLogger log("ipmiMonitor::setSensorValueRd");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  int sensorIndex = getSensor(id);

  if (sensorIndex > 0) {
    sensors[sensorIndex].value = value;
    sensors[sensorIndex].value_raw = value_raw;
    if (enableArch) {
      sensors[sensorIndex].arch->erase(sensors[sensorIndex].arch->begin());
      sensors[sensorIndex].arch->push_back(value);
    }
  }
  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::setBoardStateRd(ipmi_entity_id_t id,
                                  enum ipmi_hot_swap_states state) {
  ipmiMonitorLogger log("ipmiMonitor::setBoardStateRd");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  int boardIndex = getBoard(id);

  if (boardIndex >= 0) {
    boards[boardIndex].state = state;
  }

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::setIntervals(IntervalsIndexes index, int val) {
  ipmiMonitorLogger log("ipmiMonitor::setIntervals");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);
  switch (index) {
    case IntervalsIndexes::SENSOR:
      sens_interval = val * 1.0;
      break;
    case IntervalsIndexes::THRESH:
      thresh_interval = val * 1.0;
      break;
    case IntervalsIndexes::EVENT:
      event_interval = val * 1.0;
      break;
  }
  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::setBoardFStateSp(ipmi_entity_id_t id, int val) {
  ipmiMonitorLogger log("ipmiMonitor::setBoardFStateSp");

  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  int boardIndex = getBoard(id);

  if (boardIndex >= 0) {
    boards[boardIndex].fstate = val;
  }

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

int ipmiMonitor::getTick() {
  ipmiMonitorLogger log("ipmiMonitor::getTick");
  return tick;
}

void ipmiMonitor::setEnableArch(int val) {
  if (val == 0 || val == 1) {
    enableArch = val;
  }
}

epicsFloat64 *ipmiMonitor::getTimeStamps() {
  ipmiMonitorLogger log("ipmiMonitor::getTimeStamps");
  return &stamps[0];
}

int ipmiMonitor::getTimeStampsLength() {
  ipmiMonitorLogger log("ipmiMonitor::getTimeStampsLength");
  return stamps.size();
}

ipmi_event_handlers_t *ipmiMonitor::getIpmiEventHandlers() {
  return ipmiEventHandlers;
}

char ipmiMonitor::getSync() { return syncStatus; }

void ipmiMonitor::setSync(char sync) { syncStatus = sync; }

char ipmiMonitor::getConnected() { return connected; }
