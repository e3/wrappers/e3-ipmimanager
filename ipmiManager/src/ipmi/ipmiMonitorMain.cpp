#include <getopt.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iomanip>
#include <iostream>

#include "ipmiMonitor.h"

using namespace std;

// dummy epics api
void epicsEventMustTrigger(int arg) {}
void epicsMutexMustLock(int arg) {}
void epicsMutexUnlock(int arg) {}

volatile int terminateLoop = 0;

void handler(int) {
  terminateLoop = 1;

  signal(SIGINT, SIG_DFL);
}

void printHelp() {
  cout << endl;
  cout << "Several command line parameters can be specified:" << endl;
  cout << endl;
  cout << "    -h <MCH_ADDR>         - specify MCH address, to which tool will "
          "connect"
       << endl;
  cout << "    -d                    - show device tree (presents boards "
          "visible in the system together with sensors)"
       << endl;
  cout << "    -s <INTERVAL>         - specify sensor refresh interval ( "
          "default 10s)"
       << endl;
  cout << "    -t <INTERVAL>         - specify threshold refresh interval ( "
          "default 10s)"
       << endl;
  cout << "    -e <INTERVAL>         - specify event refresh interval ( "
          "default 10s)"
       << endl;
  cout << "    -f <FRU>              - specify FRU ID number for further "
          "operations"
       << endl;
  cout << "    -a <ACTION>           - specify 0 to shutdown selected FRU, 1 "
          "to start-up selected FRU"
       << endl;
  cout << "    -R                    - keep connection alvie without "
          "refreshing of the sensors"
       << endl;
  cout << "    -r                    - keep connection alive and refresh "
          "sesnors with interval"
       << endl;
  cout << "    -g \"<F>:<N>\"          - generate database files ( <F> "
          "subfolder, <N> name )"
       << endl;
  cout << "    -j [FRU_ID][:FILENAME] - generate JSON description of the FRU "
          "or the system and optionally send results to a file"
       << endl;
  cout << endl;
  cout << endl;
}

void printLog(const char *format, ...) {
  char formatted[256] = {0};

  va_list args;
  va_start(args, format);
  { vsprintf(formatted, format, args); }
  va_end(args);

  printf("app %s", formatted);
}

void printLib(os_handler_t *handler, const char *format,
              enum ipmi_log_type_e log_type, va_list args) {
  char formatted[256] = {0};
  char type[16] = {0};
  int do_nl = 1;

  switch (log_type) {
    case IPMI_LOG_INFO:
      sprintf(type, "INFO: ");
      break;

    case IPMI_LOG_WARNING:
      sprintf(type, "WARN: ");
      break;

    case IPMI_LOG_SEVERE:
      sprintf(type, "SEVR: ");
      break;

    case IPMI_LOG_FATAL:
      sprintf(type, "FATL: ");
      break;

    case IPMI_LOG_ERR_INFO:
      sprintf(type, "EINF: ");
      break;

    case IPMI_LOG_DEBUG_START:
      do_nl = 0;
    /* FALLTHROUGH */
    case IPMI_LOG_DEBUG:
      sprintf(type, "DEBG: ");
      break;

    case IPMI_LOG_DEBUG_CONT:
      do_nl = 0;
    /* FALLTHROUGH */
    case IPMI_LOG_DEBUG_END:
      break;
  }

  { vsprintf(formatted, format, args); }
  printf("lib %s %s", type, formatted);
  if (do_nl) printf("\n");
}

void checkSync(ipmiMonitor *mon) {
  if (mon->getSync() == 0) {
    printf("We are out of sync ! \n");
  }
}

int main(int argc, char *argv[]) {
  int opt;

  // command line arguments
  char mchAddr[128] = {0};
  int showTree = 0;

  int intervalSens = 10;
  int intervalThr = 10;
  int intervalEvent = 10;

  int fruId = -1;
  int fruAction = -1;

  int runLoop = 0;
  int generateFiles = 0;

  char folder[256] = {0};
  uint8_t mode = 0;

  uint8_t generateJson = 0;
  int generateJsonFru = -1;
  char generateJsonName[256] = {0};

  opterr = 0;

  while ((opt = getopt(argc, argv, "h:ds:t:e:rRf:a:g:j:")) != -1) {
    switch (opt) {
      case 'h': {
        memcpy(mchAddr, optarg, strlen(optarg));
        break;
      }
      case 'd': {
        showTree = 1;
        break;
      }
      case 's': {
        char *err;
        intervalSens = strtol(optarg, &err, 0);

        if (*err != 0) {
          cout << "Wrong sensor interval - switching to default (10s)" << endl;
          intervalSens = 10;
        }
        break;
      }
      case 't': {
        char *err;
        intervalThr = strtol(optarg, &err, 0);

        if (*err != 0) {
          cout << "Wrong threshold interval - switching to default (10s)"
               << endl;
          intervalThr = 10;
        }
        break;
      }
      case 'e': {
        char *err;
        intervalEvent = strtol(optarg, &err, 0);

        if (*err != 0) {
          cout << "Wrong event interval - switching to default (10s)" << endl;
          intervalEvent = 10;
        }
        break;
      }
      case 'f': {
        char *err;
        fruId = strtol(optarg, &err, 0);

        if (*err != 0) {
          cout << "Wrong fru id - exiting" << endl;
          exit(1);
        }
        break;
      }
      case 'a': {
        char *err;
        fruAction = strtol(optarg, &err, 0);

        if (*err != 0) {
          cout << "Wrong fru action - exiting" << endl;
          exit(1);
        }
        break;
      }
      case 'r': {
        runLoop = 1;
        break;
      }
      case 'R': {
        runLoop = 2;
        break;
      }
      case 'g': {
        size_t i = 0;

        generateFiles = 1;

        for (i = 0; i < strlen(optarg); i++) {
          if (optarg[i] == ':') {
            optarg[i] = 0;
            break;
          }
        }
        strcpy(folder, &optarg[0]);

        mode = optarg[i + 1] == 48 ? 0 : 1;

        break;
      }
      case 'j': {
        char *err;
        int _hasColon = 0;
        char *_name;

        generateJson = 1;

        if (optarg[0] == '-')  // no arguments to j specified
        {
          optind -= 1;
          break;
        }

        // do we have ":" in our sting ?
        for (size_t i = 0; i < strlen(optarg); i++) {
          if (optarg[i] == ':') {
            _hasColon = 1;
            optarg[i] = 0;
            _name = &optarg[i + 1];
            break;
          }
        }

        if (_hasColon == 1) {
          generateJsonFru = strtol(optarg, &err, 0);

          if (*err != 0)  // wrong fru id given
          {
            printf("Fru ID specified is not an integer !\n");
            return 0;
          }
          strncpy(generateJsonName, _name, 256);
        } else {
          generateJsonFru = strtol(optarg, &err, 0);

          if (*err !=
              0)  // not intgere given as a param - we assume it is filename
          {
            generateJsonFru = -1;
            strncpy(generateJsonName, optarg, 256);
          }
        }
        break;
      }
      case '?': {
        if (optopt == 'j') {
          generateJson = 1;
        } else {
          printf("Error: Option %c requires argument !\n", optopt);
          return 0;
        }
      }
    }
  }

  if (argc == 1) {
    printHelp();
    return 0;
  }

  if (mchAddr[0] == 0) {
    cout << "You need to specify IP addr of MCH !" << endl;
    return -1;
  }

  signal(SIGINT, handler);

  ipmiMonitor monitor(mchAddr, printLog, printLib, 1024);

  monitor.setEnableArch(1);
  monitor.setIntervals(IntervalsIndexes::SENSOR, intervalSens);
  monitor.setIntervals(IntervalsIndexes::THRESH, intervalThr);
  monitor.setIntervals(IntervalsIndexes::EVENT, intervalEvent);

  monitor.refreshSensors();

  if (showTree == 1) {
    monitor.debug();
  }

  if (generateFiles == 1) {
    if (monitor.generateDb(folder, "boards", mode) == 0) {
      cout << "Database file generation successful." << endl;
    } else {
      cout << "Database file generation failed." << endl;
    }
  }

  if ((fruAction != -1) && (fruId != -1)) {
    switch (fruAction) {
      case 0: {
        cout << fruId << " "
             << "shut" << endl;

        monitor.shutdownFru(fruId);

        break;
      }
      case 1: {
        cout << fruId << " "
             << "start" << endl;

        monitor.startFru(fruId);

        break;
      }
      default: {
        cout << "Unknown action - ignoring" << endl;
        break;
      }
    }
  }

  if (generateJson == 1) {
    monitor.generateJson(generateJsonName, generateJsonFru);
  }

  if (runLoop == 1) {
    cout << "Running main loop..." << endl;
    monitor.run(0, 0, (int *)&terminateLoop, 0);
    cout << "Exitting main loop..." << endl;
  }

  if (runLoop == 2) {
    cout << "Running main loop..." << endl;
    monitor.runNoPoll(0, 0, (int *)&terminateLoop, 0, checkSync);
    cout << "Exitting main loop..." << endl;
  }
}
