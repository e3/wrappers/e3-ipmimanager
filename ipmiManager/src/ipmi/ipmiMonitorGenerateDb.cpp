#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "ipmiMonitor.h"

#define MAX_MODULES 32

typedef struct {
  char name[64];
  int slot;
  char opts[20];

} module_t;

static void initModules(module_t* modules) {
  for (unsigned int i = 0; i < MAX_MODULES; i++) {
    modules[i].slot = 0;
    memset(modules[i].name, 0, sizeof(modules[i].name));
    memset(modules[i].opts, 0, sizeof(modules[i].opts));
  }
}

static void addModule(module_t* modules, char* name, int size, char* opts,
                      int opt_size, int slot) {
  for (unsigned int i = 0; i < MAX_MODULES; i++) {
    if (modules[i].slot == 0) {
      modules[i].slot = slot;
      strncpy(modules[i].name, name, size);
      strncpy(modules[i].opts, opts, opt_size);
      break;
    }
  }
}

// need to find faster way of checking if on list
static int isSlotEmpty(board_info* boards, int count, int slot) {
  for (int i = 0; i < count; i++) {
    if (boards[i].slot == slot) {
      return 0;
    }
  }
  return 1;
}

int ipmiMonitor::generateDb(const char* folder, const char* name, int mode) {
  int fd_existing;
  int fd_sensors;

  module_t modules[MAX_MODULES];

  initModules(modules);

  char filename[256];

  mkdir(folder, 0777);
  sprintf(filename, "%s/%s.board.present.substitutions", folder, name);
  fd_existing = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0660);

  if (fd_existing == -1) {
    return -1;
  }

  {
    char buf[256];
    int len = 0;
    int boardCount = 0;

    board_info* boards = getBoardInfo(&boardCount);

    len = sprintf(buf,
                  "file \"$(ipmimanager_DIR)db/board.present.template\"\n{\n");
    len = write(fd_existing, buf, len);
    len = sprintf(buf, "  pattern { %8s,  %48s, %10s, %6s }\n", "PREFIX",
                  "INDEX", "OPT", "SLOT");
    len = write(fd_existing, buf, len);

    int pm_cnt = 0;
    int cu_cnt = 0;

    for (int j = 0; j < 80; j++) {
      // board in chassis
      if (isSlotEmpty(boards, boardCount, j) == 0) {
        char idx[20];
        char device[20];
        char opt[10] = {};

        if (mode) {
          if (j == 64) {
            strcpy(opt, "Clk-");
          } else if (j == 65) {
            strcpy(opt, "PCI-");
          } else {
            sprintf(device, "$(SLOT%d_MODULE)", j);
            sprintf(idx, "$(SLOT%d_IDX)", j);
            strcpy(opt, "\"\"");
          }
        } else {
          // power modules possible slots
          if ((30 < j) && (j < 35)) {
            pm_cnt++;
            sprintf(idx, "%02d", pm_cnt);
            strcpy(device, "PM");
            strcpy(opt, "\"\"");
          }
          // cu possible slots
          else if ((47 < j) && (j < 50)) {
            cu_cnt++;
            sprintf(idx, "%02d", cu_cnt);
            strcpy(device, "CU");
            strcpy(opt, "\"\"");
          }
          // fixed slot clock
          else if (j == 64) {
            strcpy(idx, "00");
            strcpy(device, "MTCA");
            strcpy(opt, "Clk-");
          }
          // fixed PCI slot
          else if (j == 65) {
            strcpy(idx, "00");
            strcpy(device, "MTCA");
            strcpy(opt, "PCI-");
          }
          // AMCs possible slots
          else if ((16 > j) || (j > 29)) {
            sprintf(device, "$(SLOT%d_MODULE)", j);
            sprintf(idx, "$(SLOT%d_IDX)", j);
            strcpy(opt, "\"\"");
          }
          // RTMs possible slots
          else {
            sprintf(device, "$(SLOT%d_MODULE)", j);
            sprintf(idx, "$(SLOT%d_IDX)", j - 16);
            strcpy(opt, "\"\"");
          }
        }

        {
          // generate substitution string
          const char* prefix = "$(P)-";
          char name_for_sens[64] = ":";

          len = 1;

          if (!((mode) && ((j == 64) || (j == 65)))) {
            len = sprintf(name_for_sens, "%s-%s%s:", device,
                          (mode == 0) ? "$(CRATE_NUM)" : "", idx);
          }

          // Add module names and slots to array of structures
          addModule(modules, name_for_sens, len, opt, strlen(opt), j);

          if (mode) {
            prefix = "$(P)Ctrl-";
            if ((j == 64) || (j == 65)) prefix = "$(P)";
          }
          len = sprintf(buf, "          { %8s,  %48s, %10s, %6d }\n", prefix,
                        name_for_sens, opt, j);
          len = write(fd_existing, buf, len);
        }
      }
    }
    len = sprintf(buf, "}\n");
    len = write(fd_existing, buf, len);

    close(fd_existing);
  }

  sprintf(filename, "%s/%s.sensor.substitutions", folder, name);
  fd_sensors = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0660);

  if (fd_sensors != -1) {
    char buf[1024];
    int len = 0;

    int sensorCount = 0;
    sensor_info* sensors = getSensorInfo(&sensorCount);

    int archLen = 0;
    archLen = getTimeStampsLength();

    len = sprintf(buf, "file \"$(ipmimanager_DIR)db/sensor.template\"\n{\n");
    len = write(fd_sensors, buf, len);
    len = sprintf(buf, "  pattern { %8s, %48s, %8s, %8s, %8s, %16s, %32s }\n",
                  "PREFIX", "INDEX", "OPT", "SLOT", "SINDEX", "ARCHIVER_SIZE",
                  "DESC");
    len = write(fd_sensors, buf, len);

    for (int j = 0; j < sensorCount; j++) {
      for (unsigned int i = 0; i < MAX_MODULES; i++) {
        char sensorName[128] = {0};

        if (sensors[j].slot == modules[i].slot) {
          const char* prefix = "$(P)-";

          if (mode) {
            prefix = "$(P)Ctrl-";
            if ((j == 64) || (j == 65)) prefix = "$(P)";
          }

          sprintf(sensorName, "\"%s\"", sensors[j].name);
          len = sprintf(buf,
                        "          { %8s, %48s, %8s, %8d, %8d, %16d, %32s }\n",
                        prefix, modules[i].name, modules[i].opts,
                        sensors[j].slot, sensors[j].index, archLen, sensorName);
          len = write(fd_sensors, buf, len);
        }
      }
    }
    len = sprintf(buf, "}\n");
    len = write(fd_sensors, buf, len);

    close(fd_sensors);
  } else {
    return -1;
  }
  return 0;
}
