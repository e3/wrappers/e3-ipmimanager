#include <iostream>
#include <string>

#include "ipmiMonitor.h"

ipmiMonitorLogger::ipmiMonitorLogger(std::string fun, int _enabled) {
  this->functionName = fun;
  enabled = _enabled;

  if (enabled) {
    for (int i = 0; i < level * 3; i++) std::cout << " ";
    std::cout << "|- Entering " << this->functionName << "..." << std::endl;
  }
  level++;
}

ipmiMonitorLogger::~ipmiMonitorLogger() {
  if (enabled) {
    for (int i = 0; i < (level - 1) * 3; i++) std::cout << " ";
    std::cout << "|- Exiting  " << this->functionName << "..." << std::endl;
  }
  level--;
}

ipmiMonitorLogger& ipmiMonitorLogger::operator<<(std::string& str) {
  if (enabled) {
    for (int i = 0; i < (level - 1) * 3; i++) std::cout << " ";
    std::cout << "|--- " << str << std::endl;
  }

  return *this;
}

ipmiMonitorLogger& ipmiMonitorLogger::operator<<(char* str) {
  if (enabled) {
    for (int i = 0; i < (level - 1) * 3; i++) std::cout << " ";
    std::cout << "|--- " << str << std::endl;
  }

  return *this;
}

ipmiMonitorLogger& ipmiMonitorLogger::operator<<(const char* str) {
  if (enabled) {
    for (int i = 0; i < (level - 1) * 3; i++) std::cout << " ";
    std::cout << "|--- " << str << std::endl;
  }

  return *this;
}
