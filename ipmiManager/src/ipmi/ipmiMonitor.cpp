#include "ipmiMonitor.h"

#include <ctype.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <iostream>

int ipmiMonitorLogger::level = 0;

extern "C" {
int ipmi_oem_utca_init(void);
};

//
// Actions
//
void ipmiMonitor::updateSensorValue(ipmi_sensor_t *sensor, int err,
                                    enum ipmi_value_present_e value_present,
                                    unsigned int raw_value, double val,
                                    ipmi_states_t *states) {
  ipmiMonitorLogger log("ipmiMonitor::updateSensorValue");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  if (err) {
    printf("sensor update error\n");
    goto out;
  }

  /* printf("Got threshold reading for sensor %s\n", name);
  if (ipmi_is_event_messages_enabled(states))
  printf(" event messages enabled\n");
  if (ipmi_is_sensor_scanning_enabled(states))
  printf(" sensor scanning enabled\n");
  if (ipmi_is_initial_update_in_progress(states))
  printf(" initial update in progress\n");
  */

  setSensorValueRd(ipmi_sensor_convert_to_id(sensor), val, raw_value);

  {
    int flags[6] = {0};

    if (ipmi_sensor_get_threshold_access(sensor) ==
        IPMI_THRESHOLD_ACCESS_SUPPORT_NONE) {
      goto out;
    }

    for (int i = IPMI_LOWER_NON_CRITICAL; i <= IPMI_UPPER_NON_RECOVERABLE;
         i++) {
      int val = 0;
      int rv = ipmi_sensor_threshold_reading_supported(
          sensor, (enum ipmi_thresh_e)i, &val);

      if (!(rv || !val)) {
        if (ipmi_is_threshold_out_of_range(states, (enum ipmi_thresh_e)i))
          flags[i - IPMI_LOWER_NON_CRITICAL] = 1;
      }
    }

    int toSet[6] = {flags[5], flags[4], flags[3], flags[0], flags[1], flags[2]};

    setSensorThresholdsRd(ipmi_sensor_convert_to_id(sensor), NULL, NULL, toSet);
  }

out:
  os_handler_waiter_release(waiter);
  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::refreshSensors() {
  ipmiMonitorLogger log("ipmiMonitor::refreshSensors");
  int rv;

  for (int i = 0; i < sensorCount; i++) {
    if (connected == 0) break;

    if (sensors[i].p == 0) {
      //          printLog("Skipping sesnor%d\n",i) ;
      continue;
    }
    os_handler_waiter_use(waiter);

    if (sensors[i]._type == IPMI_EVENT_READING_TYPE_THRESHOLD) {
      rv = ipmi_sensor_id_get_reading(sensors[i]._id, global_sensor_reading,
                                      this);
    } else {
      rv =
          ipmi_sensor_id_get_states(sensors[i]._id, global_sensor_states, this);
    }

    if (rv) {
      os_handler_waiter_release(waiter);
      continue;
    }
    os_handler_waiter_wait(waiter, NULL);
  }
}

void ipmiMonitor::updateSensorState(ipmi_sensor_t *sensor, int err,
                                    ipmi_states_t *states) {
  ipmiMonitorLogger log("ipmiMonitor::updateSensorState");
  int i;
  unsigned int value_raw = 0;

  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  if (err) {
    goto out;
  }

  for (i = 0; i < 15; i++) {
    int val, rv;

    rv = ipmi_sensor_discrete_event_readable(sensor, i, &val);

    if (rv || !val) {
      continue;
    }

    value_raw |= ipmi_is_state_set(states, i) << i;
  }

  setSensorValueRd(ipmi_sensor_convert_to_id(sensor), value_raw, value_raw);

out:
  os_handler_waiter_release(waiter);
  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::run(epicsEventId eventSensorUpdateDone,
                      epicsMutexId mutexSensorUpdateInProgress, int *terminate,
                      epicsMutexId mutexTerminate) {
  ipmiMonitorLogger log("ipmiMonitor::run");
  int rv;

  this->eventSensorUpdateDone = eventSensorUpdateDone;
  this->mutexSensorUpdateInProgress = mutexSensorUpdateInProgress;

  // at this point domain and tree is built, we can run main thread loop
  epicsMutexMustLock(mutexTerminate);

  while (*terminate == 0) {
    time_t actual_time;
    struct timeval tim;
    gettimeofday(&tim, NULL);
    actual_time = tim.tv_sec;

    epicsMutexUnlock(mutexTerminate);

    // refresh values of all sensors
    if ((connected == 1) &&
        (difftime(actual_time, last_sens_update) >= sens_interval)) {
      tick++;
      printLog(
          "Sensor refreshing, last update was %.1f ago, interval is %.1f\n",
          difftime(actual_time, last_sens_update), sens_interval);

      refreshSensors();
      if (enableArch) {
        stamps.erase(stamps.begin());
        stamps.push_back(actual_time);
      }

      // refresh state of the boards
      for (int i = 0; i < boardCount; i++) {
        if (connected == 0) break;

        if (boards[i].state == IPMI_HOT_SWAP_NOT_PRESENT) {
          //                   printLog("Skipping board %d\n",i) ;
          continue;
        }
        os_handler_waiter_use(waiter);

        rv = ipmi_entity_id_get_hot_swap_state(
            boards[i].entityId, global_entity_hot_swap_state_done, this);

        if (rv) {
          os_handler_waiter_release(waiter);
          continue;
        }
        os_handler_waiter_wait(waiter, NULL);
      }
      if (getSync() == 0) {
        printLog("We are no longer in sync. Monitor must be restarted\n");
      }

      last_sens_update = actual_time;
    }

    // set thresholds
    if ((connected == 1) &&
        (difftime(actual_time, last_thresh_update) >= thresh_interval)) {
      for (int i = 0; i < sensorCount; i++) {
        ipmi_thresholds_t *th;

        if (connected == 0) break;

        if (sensors[i].p == 0) {
          //                   printLog("Skipping sesnor thr %d\n",i) ;
          continue;
        }

        if (sensors[i].thrNeedsUpdate == 0) continue;

        //#ifdef STANDALONE
        printLog("%d %d %d %s needs thr update. Allowed: %X\n", i,
                 sensors[i].slot, sensors[i].index, sensors[i].name,
                 sensors[i].thrUpdateAllowed);
        //#endif

        th = (ipmi_thresholds_t *)malloc(ipmi_thresholds_size());

        ipmi_thresholds_init(th);

        //// TODO: Correct bitmask for specific thresholds

        // we update only thrs which are allowed for update
        if ((sensors[i].thrUpdateAllowed & 0x01) != 0) {
          ipmi_threshold_set(th, NULL, IPMI_LOWER_NON_CRITICAL,
                             sensors[i].thrlononcritval_sp);
        }
        if ((sensors[i].thrUpdateAllowed & 0x02) != 0) {
          ipmi_threshold_set(th, NULL, IPMI_LOWER_CRITICAL,
                             sensors[i].thrlocritval_sp);
        }
        if ((sensors[i].thrUpdateAllowed & 0x04) != 0) {
          ipmi_threshold_set(th, NULL, IPMI_LOWER_NON_RECOVERABLE,
                             sensors[i].thrlononrecval_sp);
        }
        if ((sensors[i].thrUpdateAllowed & 0x08) != 0) {
          ipmi_threshold_set(th, NULL, IPMI_UPPER_NON_CRITICAL,
                             sensors[i].thrupnoncritval_sp);
        }
        if ((sensors[i].thrUpdateAllowed & 0x10) != 0) {
          ipmi_threshold_set(th, NULL, IPMI_UPPER_CRITICAL,
                             sensors[i].thrupcritval_sp);
        }
        if ((sensors[i].thrUpdateAllowed & 0x20) != 0) {
          ipmi_threshold_set(th, NULL, IPMI_UPPER_NON_RECOVERABLE,
                             sensors[i].thrupnonrecval_sp);
        }
        os_handler_waiter_use(waiter);

        rv = ipmi_sensor_id_set_thresholds(sensors[i]._id, th,
                                           global_set_sensor_thr_done, this);

        if (rv) {
          printLog("up error\n");
          os_handler_waiter_release(waiter);
          free(th);
          sensors[i].thrNeedsUpdate =
              0;  // something went wrong - we cancel update
          continue;
        }
        os_handler_waiter_wait(waiter, NULL);
        free(th);
      }

      // we just read it from HW again to reflect any internal/external changes
      for (int i = 0; i < sensorCount; i++) {
        // fetch thresholds for sensor
        int rv;

        if (connected == 0) break;

        if (sensors[i].p == 0) {
          //                   printLog("Skipping sesnor thr %d\n",i) ;
          continue;
        }

        os_handler_waiter_use(waiter);

        rv = ipmi_sensor_id_get_thresholds(sensors[i]._id,
                                           global_sensor_thresholds_done, this);

        if (rv) {
          os_handler_waiter_release(waiter);
        }
        os_handler_waiter_wait(waiter, NULL);
      }
      last_thresh_update = actual_time;
    }

    // events
    if (difftime(actual_time, last_event_update) >= event_interval) {
      // do we need to shutdown any boards ??
      for (int i = 0; i < boardCount; i++) {
        if (boards[i].fstate == 0) {
          shutdownFru(boards[i].fruid);
          boards[i].fstate = -1;
        }
        if (boards[i].fstate == 1) {
          startFru(boards[i].fruid);
          boards[i].fstate = -1;
        }
      }

      last_event_update = actual_time;
    }

    os_hnd->perform_one_op(os_hnd, NULL);

    epicsEventMustTrigger(eventSensorUpdateDone);

    // here we need to pause based on dynamically set interval
    usleep(10000);

    epicsMutexMustLock(mutexTerminate);
  }
  epicsMutexUnlock(mutexTerminate);
}

void ipmiMonitor::runNoPoll(epicsEventId eventSensorUpdateDone,
                            epicsMutexId mutexSensorUpdateInProgress,
                            int *terminate, epicsMutexId mutexTerminate,
                            void (*fun)(ipmiMonitor *)) {
  ipmiMonitorLogger log("ipmiMonitor::run_no_poll");

  while (*terminate == 0) {
    os_hnd->perform_one_op(os_hnd, NULL);
    if (fun != NULL) fun(this);
  }
}

board_info *ipmiMonitor::getBoardInfo(int *count) {
  ipmiMonitorLogger log("ipmiMonitor::getBoardInfo");
  *count = boardCount;

  return boards;
}

sensor_info *ipmiMonitor::getSensorInfo(int *count) {
  ipmiMonitorLogger log("ipmiMonitor::getSensorInfo");
  *count = sensorCount;

  return sensors;
}

void ipmiMonitor::iniTimeStamps(unsigned int size) {
  ipmiMonitorLogger log("ipmiMonitor::iniTimeStmaps");
  struct timeval tv;
  time_t t;
  gettimeofday(&tv, NULL);
  t = tv.tv_sec;
  stamps.reserve(stamps.size() + size);
  for (unsigned int i = 0; i < size; i++) {
    stamps.push_back((t - (size * 60) + i * 60) * 1.0);
  }
}

void ipmiMonitor::registerLogFunction(printLog_t func) {
  ipmiMonitorLogger log("ipmiMonitor::registerLogFunction");
  printLog = func;
}

void ipmiMonitor::shutdownFru(int fruId) {
  ipmiMonitorLogger log("ipmiMonitor::shutdownFru");
  int rv;

  ipmi_entity_id_t entId = getBoardByFru(fruId);

  if (!ipmi_entity_id_is_invalid(&entId)) {
    os_handler_waiter_use(waiter);
    rv = ipmi_entity_id_deactivate(entId, global_entity_deactivate_done, this);
    if (rv) {
      os_handler_waiter_release(waiter);
    }
    os_handler_waiter_wait(waiter, NULL);
  }
}

void ipmiMonitor::startFru(int fruId) {
  ipmiMonitorLogger log("ipmiMonitor::startFru");
  int rv;

  ipmi_entity_id_t entId = getBoardByFru(fruId);

  if (!ipmi_entity_id_is_invalid(&entId)) {
    os_handler_waiter_use(waiter);
    rv = ipmi_entity_id_activate(entId, global_entity_deactivate_done, this);
    if (rv) {
      os_handler_waiter_release(waiter);
    }
    os_handler_waiter_wait(waiter, NULL);
  }
}
