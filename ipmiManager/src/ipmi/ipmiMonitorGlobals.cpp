#include "ipmiMonitor.h"

//
// Global Callbacks - dispatchers to from library to class
//
void global_entity_deactivate_done(ipmi_entity_t *ent, int err, void *cb_data) {
  ipmiMonitorLogger log("global_entity_deactivate_done");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->entity_deactivate_done(ent, err);
}

void global_domain_setup_done(ipmi_domain_t *domain, int err,
                              unsigned int conn_num, unsigned int port_num,
                              int still_connected, void *cb_data) {
  ipmiMonitorLogger log("global_domain_setup_done");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->domain_setup_done(err, conn_num, port_num, still_connected);
}

void global_domain_fully_up(ipmi_domain_t *domain, void *cb_data) {
  ipmiMonitorLogger log("global_domain_fully_up");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->domain_fully_up();
}

void global_domain_close_done(void *cb_data) {
  ipmiMonitorLogger log("global_domain_close_done");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->domain_close_done();
}

void global_entity_iterate_done(ipmi_entity_t *entity, void *cb_data) {
  ipmiMonitorLogger log("global_entity_iterate_done");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  // setup removal hook for entity
  ipmi_entity_add_presence_handler(
      entity, global_entity_presence_change_handler, cb_data);

  mon->indexEntity(entity);
}

void global_sensor_thresholds_done(ipmi_sensor_t *sensor, int err,
                                   ipmi_thresholds_t *th, void *cb_data) {
  ipmiMonitorLogger log("global_sensor_thresholds_done");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->sensor_thresholds_done(sensor, err, th);
}

void global_sensor_hysteresis_done(ipmi_sensor_t *sensor, int err,
                                   unsigned int positive_hysteresis,
                                   unsigned int negative_hysteresis,
                                   void *cb_data) {
  ipmiMonitorLogger log("global_sensor_hysteresis_done");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->sensor_hysteresis_done(sensor, err, positive_hysteresis,
                              negative_hysteresis);
}

void global_entity_hot_swap_state_done(ipmi_entity_t *ent, int err,
                                       enum ipmi_hot_swap_states state,
                                       void *cb_data) {
  ipmiMonitorLogger log("global_entity_hot_swap_state_done");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->entity_hot_swap_state_done(ent, err, state);
}

int global_entity_hot_swap_event(ipmi_entity_t *ent,
                                 enum ipmi_hot_swap_states last_state,
                                 enum ipmi_hot_swap_states curr_state,
                                 void *cb_data, ipmi_event_t *event) {
  ipmiMonitorLogger log("global_entity_hot_swap_event");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->entity_hot_swap_event(ent, last_state, curr_state, event);

  return 0;
}

/* Set the thresholds for the given sensor. */
void global_set_sensor_thr_done(ipmi_sensor_t *sensor, int err, void *cb_data) {
  ipmiMonitorLogger log("global_set_sensor_thr_done");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->set_sensor_thr_done(sensor, err);
}

// hooks for actions
void global_domain_close(ipmi_domain_t *domain, void *cb_data) {
  ipmiMonitorLogger log("global_domain_close");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  int rv;

  rv = ipmi_domain_close(domain, global_domain_close_done, cb_data);

  if (rv) {
    mon->actionError(rv);
  }
}

void global_iterate_entities(ipmi_domain_t *domain, void *cb_data) {
  ipmiMonitorLogger log("global_iterate_entities");
  ipmi_domain_iterate_entities(domain, global_entity_iterate_done, cb_data);

  // setup event hooks
  ipmi_domain_add_event_handler(domain, global_event_handler, cb_data);

  // setup removal hooks
  ipmi_domain_add_entity_update_handler(domain, global_entity_update_handler,
                                        cb_data);

  // decrease audit time
  // ipmi_domain_set_ipmb_rescan_time( domain, 90 ) ;

  ipmi_domain_enable_events(domain);
}

void global_iterate_sensors(ipmi_entity_t *entity, ipmi_sensor_t *sensor,
                            void *cb_data) {
  ipmiMonitorLogger log("global_iterate_sensors");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;
  mon->indexSensor(sensor);
}

void global_sensor_reading(ipmi_sensor_t *sensor, int err,
                           enum ipmi_value_present_e value_present,
                           unsigned int raw_value, double val,
                           ipmi_states_t *states, void *cb_data) {
  ipmiMonitorLogger log("global_sensor_reading");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;
  mon->updateSensorValue(sensor, err, value_present, raw_value, val, states);
}

void global_sensor_states(ipmi_sensor_t *sensor, int err, ipmi_states_t *states,
                          void *cb_data) {
  ipmiMonitorLogger log("global_sensor_states");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;
  mon->updateSensorState(sensor, err, states);
}

void global_event_handler(ipmi_domain_t *domain, ipmi_event_t *event,
                          void *event_data) {
  ipmiMonitorLogger log("globa_event_handler");
  ipmiMonitor *mon = (ipmiMonitor *)event_data;

  int result = ipmi_event_call_handler(domain, mon->getIpmiEventHandlers(),
                                       event, event_data);

  if (result == IPMI_EVENT_HANDLED) {
  } else {
    mon->analyzeOtherEvents(domain, event);
  }
}

int global_analyze_threshold_events(ipmi_sensor_t *sensor,
                                    enum ipmi_event_dir_e dir,
                                    enum ipmi_thresh_e threshold,
                                    enum ipmi_event_value_dir_e high_low,
                                    enum ipmi_value_present_e value_present,
                                    unsigned int raw_value, double value,
                                    void *cb_data, ipmi_event_t *event) {
  ipmiMonitorLogger log("global_analyze_threshold_events");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  return mon->analyzeThresholdEvents(sensor, dir, threshold, high_low,
                                     value_present, raw_value, value, event);
}

int global_analyze_discrete_events(ipmi_sensor_t *sensor,
                                   enum ipmi_event_dir_e dir, int offset,
                                   int severity, int prev_severity,
                                   void *cb_data, ipmi_event_t *event) {
  ipmiMonitorLogger log("global_analyze_discrete_events");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  return mon->analyzeDiscreteEvents(sensor, dir, offset, severity,
                                    prev_severity, event);
}

void global_entity_update_handler(enum ipmi_update_e op, ipmi_domain_t *domain,
                                  ipmi_entity_t *entity, void *cb_data) {
  ipmiMonitorLogger log("global_analyze_threshold_events");
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->setSync(0);
}

int global_entity_presence_change_handler(ipmi_entity_t *entity, int present,
                                          void *cb_data, ipmi_event_t *event) {
  ipmiMonitor *mon = (ipmiMonitor *)cb_data;

  mon->entityPresenceChange(entity, present);

  return IPMI_EVENT_HANDLED;
}
