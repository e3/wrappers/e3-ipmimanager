#ifndef _IPMI_MON_H_
#define _IPMI_MON_H_

#include <OpenIPMI/ipmi_auth.h>
#include <OpenIPMI/ipmi_err.h>
#include <OpenIPMI/ipmi_fru.h>
#include <OpenIPMI/ipmi_lan.h>
#include <OpenIPMI/ipmi_posix.h>
#include <OpenIPMI/ipmi_smi.h>
#include <OpenIPMI/ipmiif.h>

#include <string>
#include <vector>

#define MAX_SENSORS 512
#define MAX_BOARDS 128

// if we compile standalone, we override epics API
#ifdef STANDALONE
typedef int epicsEventId;
typedef int epicsMutexId;

void epicsEventMustTrigger(int arg);
void epicsMutexMustLock(int arg);
void epicsMutexUnlock(int arg);

typedef double epicsFloat64;
#else
#include <epicsEvent.h>
#include <epicsMutex.h>
#include <epicsTypes.h>
#include <errlog.h>
#endif

enum class IntervalsIndexes { SENSOR, THRESH, EVENT };

void global_entity_deactivate_done(ipmi_entity_t *ent, int err, void *cb_data);
void global_domain_setup_done(ipmi_domain_t *domain, int err,
                              unsigned int conn_num, unsigned int port_num,
                              int still_connected, void *cb_data);
void global_domain_fully_up(ipmi_domain_t *domain, void *cb_data);
void global_domain_close_done(void *cb_data);
void global_entity_iterate_done(ipmi_entity_t *entity, void *cb_data);
void global_sensor_thresholds_done(ipmi_sensor_t *sensor, int err,
                                   ipmi_thresholds_t *th, void *cb_data);
void global_sensor_hysteresis_done(ipmi_sensor_t *sensor, int err,
                                   unsigned int positive_hysteresis,
                                   unsigned int negative_hysteresis,
                                   void *cb_data);
void global_entity_hot_swap_state_done(ipmi_entity_t *ent, int err,
                                       enum ipmi_hot_swap_states state,
                                       void *cb_data);
int global_entity_hot_swap_event(ipmi_entity_t *ent,
                                 enum ipmi_hot_swap_states last_state,
                                 enum ipmi_hot_swap_states curr_state,
                                 void *cb_data, ipmi_event_t *event);
void global_set_sensor_thr_done(ipmi_sensor_t *sensor, int err, void *cb_data);

void global_domain_close(ipmi_domain_t *domain, void *cb_data);
void global_iterate_entities(ipmi_domain_t *domain, void *cb_data);
void global_iterate_sensors(ipmi_entity_t *entity, ipmi_sensor_t *sensor,
                            void *cb_data);
void global_sensor_reading(ipmi_sensor_t *sensor, int err,
                           enum ipmi_value_present_e value_present,
                           unsigned int raw_value, double val,
                           ipmi_states_t *states, void *cb_data);
void global_sensor_states(ipmi_sensor_t *sensor, int err, ipmi_states_t *states,
                          void *cb_data);

void global_event_handler(ipmi_domain_t *domain, ipmi_event_t *event,
                          void *event_data);
int global_analyze_threshold_events(ipmi_sensor_t *sensor,
                                    enum ipmi_event_dir_e dir,
                                    enum ipmi_thresh_e threshold,
                                    enum ipmi_event_value_dir_e high_low,
                                    enum ipmi_value_present_e value_present,
                                    unsigned int raw_value, double value,
                                    void *cb_data, ipmi_event_t *event);
int global_analyze_discrete_events(ipmi_sensor_t *sensor,
                                   enum ipmi_event_dir_e dir, int offset,
                                   int severity, int prev_severity,
                                   void *cb_data, ipmi_event_t *event);

void global_entity_update_handler(enum ipmi_update_e op, ipmi_domain_t *domain,
                                  ipmi_entity_t *entity, void *cb_data);
int global_entity_presence_change_handler(ipmi_entity_t *entity, int present,
                                          void *cb_data, ipmi_event_t *event);

typedef struct {
  ipmi_sensor_id_t _id;
  int _type;

  double value;
  unsigned int value_raw;
  std::vector<epicsFloat64> *arch;

  int slot;
  int index;
  int parentBoard;

  int evtrdtype;  // ipmi_sensor_get_event_reading_type(sensor);

  int sdrtype;  // ?
  int p;        // = 1

  char name[64];  // int ipmi_sensor_get_name(ipmi_sensor_t *sensor, char *name,
                  // int length);
  // int ipmi_sensor_get_id(ipmi_sensor_t *sensor, char *id, int length);

  char type[64];  // int ipmi_sensor_get_sensor_type(ipmi_sensor_t *sensor);
  // const char *ipmi_sensor_get_sensor_type_string(ipmi_sensor_t *sensor);
  // int ipmi_sensor_get_sensor_type(ipmi_sensor_t *sensor);

  char units[64];  // long code

  double normmax;  // int ipmi_sensor_get_normal_max(ipmi_sensor_t *sensor,
                   // double *normal_max);
  double normmin;  // int ipmi_sensor_get_normal_min(ipmi_sensor_t *sensor,
                   // double *normal_min);

  // thresholds and hysteresis require interaction with hardware
  double thrupnonrecval_sp;
  double thrupnonrecval;
  int thrupnonrecstate;

  double thrupcritval_sp;
  double thrupcritval;
  int thrupcritstate;

  double thrupnoncritval_sp;
  double thrupnoncritval;
  int thrupnoncritstate;

  double thrlononrecval_sp;
  double thrlononrecval;
  int thrlononrecstate;

  double thrlocritval_sp;
  double thrlocritval;
  int thrlocritstate;

  double thrlononcritval_sp;
  double thrlononcritval;
  int thrlononcritstate;  // int ipmi_sensor_get_thresholds(ipmi_sensor_t
                          // *sensor,

  int thrAccess;
  int thrUpdateAllowed;
  int thrNeedsUpdate;

  double hystpos;
  double hystneg;  // int ipmi_sensor_get_hysteresis(ipmi_sensor_t *sensor,

  // this params are not used in current concept
  // int ipmi_sensor_get_tolerance(ipmi_sensor_t *sensor,
  // int ipmi_sensor_get_accuracy(ipmi_sensor_t *sensor, int val, double
  // *accuracy);
} sensor_info;

typedef struct {
  ipmi_entity_id_t _id;

  int sensorcnt;  // number of sensors for the board

  ipmi_entity_id_t
      entityId;  // internal id to be able to find entry from event handlers

  int p;  // module presence

  enum ipmi_hot_swap_states state;  // hot swap state of the board

  char fruname[64];  // fru name
  int fruid;         // fru id
  int slot;          // slot

  char bmanuf[64];      // board manufacturer
  char bname[64];       // board name
  char bsn[64];         // board serial number
  char bpn[64];         // board part number
  char bfrufileid[64];  // board fru file id

  char pmanuf[64];      // product manufacturer
  char pname[64];       // product name
  char pn[64];          // product number
  char ppv[64];         // product part version
  char psn[64];         // product serial number
  char pat[64];         // product asset tag
  char pfrufileid[64];  // product fru file id

  char mrec[4];  // multi record area

  int fstate;

} board_info;

typedef void (*ipmi_monitor_iteration_cb)(struct sensor_data *data, int count,
                                          void *cb_data);
typedef void (*ipmi_monitor_sensor_discovery_cb)(struct sensor_data *data,
                                                 int count, void *cb_data);

typedef void (*printLog_t)(const char *format, ...);

class ipmiMonitor {
 public:
  ipmiMonitor(char *address, printLog_t pLog, os_vlog_t pLib, int size);
  ~ipmiMonitor();

  // callbacks - must be public since they are called by global dispatchers
  void domain_setup_done(int err, unsigned int conn_num, unsigned int port_num,
                         int still_connected);
  void domain_fully_up();
  void domain_close_done();

  void sensor_thresholds_done(ipmi_sensor_t *sensor, int err,
                              ipmi_thresholds_t *th);
  void sensor_hysteresis_done(ipmi_sensor_t *sensor, int err,
                              unsigned int positive_hysteresis,
                              unsigned int negative_hysteresis);
  void set_sensor_thr_done(ipmi_sensor_t *sensor, int err);

  void entity_hot_swap_state_done(ipmi_entity_t *ent, int err,
                                  enum ipmi_hot_swap_states state);
  int entity_hot_swap_event(ipmi_entity_t *ent,
                            enum ipmi_hot_swap_states last_state,
                            enum ipmi_hot_swap_states curr_state,
                            ipmi_event_t *event);
  void entity_deactivate_done(ipmi_entity_t *ent, int err);

  //    void eventHandler( ipmi_domain_t *domain, ipmi_event_t  *event ) ;
  int analyzeThresholdEvents(ipmi_sensor_t *sensor, enum ipmi_event_dir_e dir,
                             enum ipmi_thresh_e threshold,
                             enum ipmi_event_value_dir_e high_low,
                             enum ipmi_value_present_e value_present,
                             unsigned int raw_value, double value,
                             ipmi_event_t *event);
  int analyzeDiscreteEvents(ipmi_sensor_t *sensor, enum ipmi_event_dir_e dir,
                            int offset, int severity, int prev_severity,
                            ipmi_event_t *event);
  int analyzeOtherEvents(ipmi_domain_t *domain, ipmi_event_t *event);

  void entityPresenceChange(ipmi_entity_t *entity, int present);

  // actions
  void actionError(int rv);
  void indexEntity(ipmi_entity_t *entity);
  void indexSensor(ipmi_sensor_t *sensor);

  void refreshSensors();

  void updateSensorState(ipmi_sensor_t *sensor, int err, ipmi_states_t *states);
  void updateSensorValue(ipmi_sensor_t *sensor, int err,
                         enum ipmi_value_present_e value_present,
                         unsigned int raw_value, double val,
                         ipmi_states_t *states);

  void domain_close();

  void shutdownFru(int fruId);
  void startFru(int fruId);

  void setIntervals(IntervalsIndexes index, int val);

  char getSync();
  void setSync(char);

  char getConnected();

  // main loop
  void run(epicsEventId eventSensorUpdateDone,
           epicsMutexId mutexSensorUpdateInProgress, int *terminate,
           epicsMutexId mutexTerminate);
  void runNoPoll(epicsEventId eventSensorUpdateDone,
                 epicsMutexId mutexSensorUpdateInProgress, int *terminate,
                 epicsMutexId mutexTerminate, void (*fun)(ipmiMonitor *));

  // get internal data (must be protected by mutex if monitor is running) - data
  // is used by asyn part
  board_info *getBoardInfo(int *count);
  sensor_info *getSensorInfo(int *count);

  ipmi_event_handlers_t *getIpmiEventHandlers();

  // helper functions
  void debug();
  void dumpBoard(int index, int mode);
  void dumpSensor(int index, int mode);

  // get and set runtime params
  int getTick();

  void setSensorThresholdsSp(ipmi_sensor_id_t id, double *thrs);

  void setBoardFStateSp(ipmi_entity_id_t id, int val);
  void registerLogFunction(printLog_t func);

  // archiver functions
  epicsFloat64 *getTimeStamps();
  int getTimeStampsLength();

  int enableArch;
  int archiverSize;
  void setEnableArch(int val);

  // generate DB
  int generateDb(const char *folder, const char *name, int mode);

  // generate JSON
  int generateJson(const char *name, int fru);

 protected:
 private:
  printLog_t printLog;

  char address[256];

  os_handler_waiter_t *waiter;

  int tick;

  // are we up to date ?
  char syncStatus;

  // are we connected ?
  char connected;

  // archiver time stamps
  std::vector<epicsFloat64> stamps;
  void iniTimeStamps(unsigned int size);

  // time intervals for refreshing sensors, thresholds and events
  double sens_interval;
  double thresh_interval;
  double event_interval;

  time_t last_sens_update;
  time_t last_thresh_update;
  time_t last_event_update;

  board_info boards[MAX_BOARDS];
  sensor_info sensors[MAX_SENSORS];

  int boardCount;
  int sensorCount;

  ipmi_domain_id_t domain;
  os_handler_t *os_hnd;

  // setters
  int getSensor(ipmi_sensor_id_t id);
  int getBoard(ipmi_entity_id_t id);

  ipmi_entity_id_t getBoardByFru(int fru);

  void addSensor(sensor_info *sensor);
  void setSensorThresholdsRd(ipmi_sensor_id_t id, double *thrs, int *flags,
                             int *states);
  void setSensorHysteresisRd(ipmi_sensor_id_t id, double *hyst);
  void setSensorValueRd(ipmi_sensor_id_t id, double value,
                        unsigned int value_raw);

  board_info *addBoard(board_info *board);
  void setBoardStateRd(ipmi_entity_id_t id, enum ipmi_hot_swap_states state);

  // params for index* callbacks
  unsigned int current_slot;
  unsigned int current_index;
  unsigned int current_cu;
  unsigned int current_pm;

  int currentError;

  // structs for event
  ipmi_event_handlers_t *ipmiEventHandlers;

  // hold shared mutex and event
  epicsEventId eventSensorUpdateDone;
  epicsMutexId mutexSensorUpdateInProgress;
};

class ipmiMonitorLogger {
 private:
  std::string functionName;
  int enabled;

  static int level;

 public:
  ipmiMonitorLogger(std::string functionName, int enabled = 0);
  ~ipmiMonitorLogger();

  ipmiMonitorLogger &operator<<(std::string &);
  ipmiMonitorLogger &operator<<(char *);
  ipmiMonitorLogger &operator<<(const char *);
};

#endif
