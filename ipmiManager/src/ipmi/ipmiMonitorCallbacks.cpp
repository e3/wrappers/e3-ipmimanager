#include <ctype.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <iostream>

#include "ipmiMonitor.h"

//
// Callbacks for domain actions
//
void ipmiMonitor::domain_setup_done(int err, unsigned int conn_num,
                                    unsigned int port_num,
                                    int still_connected) {
  ipmiMonitorLogger log("ipmiMonitor::domain_setup_done");
  currentError = err;

  // no need to release waiter - this is not 'wait' action
  //    if ( connected ==  2 ) // we call waiter_release only for initial domain
  //    setup. Next calls are not directly related to waiter_wait.
  //    {
  //       printf("waiter ipmiMonitorCallbacks.cpp:27 in\n") ;
  //       os_handler_waiter_release(waiter);
  //       printf("waiter ipmiMonitorCallbacks.cpp:29 out\n") ;
  //    }

  connected = still_connected;
}

void ipmiMonitor::domain_fully_up() {
  ipmiMonitorLogger log("ipmiMonitor::domain_fully_up");
  currentError = 0;
  os_handler_waiter_release(waiter);
}

void ipmiMonitor::domain_close_done() {
  ipmiMonitorLogger log("ipmiMonitor::domain_close_done");
  os_handler_waiter_release(waiter);
}

void ipmiMonitor::sensor_thresholds_done(ipmi_sensor_t *sensor, int err,
                                         ipmi_thresholds_t *th) {
  ipmiMonitorLogger log("ipmiMonitor::sensor_thresholds_done");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  if (err == 0) {
    double thrs[6] = {0.0};
    int flags[2] = {0};
    int val = 0;

    ipmi_threshold_get(th, IPMI_UPPER_NON_RECOVERABLE, &thrs[0]);
    ipmi_threshold_get(th, IPMI_UPPER_CRITICAL, &thrs[1]);
    ipmi_threshold_get(th, IPMI_UPPER_NON_CRITICAL, &thrs[2]);
    ipmi_threshold_get(th, IPMI_LOWER_NON_CRITICAL, &thrs[3]);
    ipmi_threshold_get(th, IPMI_LOWER_CRITICAL, &thrs[4]);
    ipmi_threshold_get(th, IPMI_LOWER_NON_RECOVERABLE, &thrs[5]);

    flags[0] = ipmi_sensor_get_threshold_access(sensor);

    for (int i = IPMI_LOWER_NON_CRITICAL; i <= IPMI_UPPER_NON_RECOVERABLE;
         i++) {
      int ev = 0;

      ipmi_sensor_threshold_settable(sensor, (enum ipmi_thresh_e)i, &ev);

      val |= ev << i;
    }

    flags[1] = val;

    setSensorThresholdsRd(ipmi_sensor_convert_to_id(sensor), thrs, flags, NULL);
  }

  os_handler_waiter_release(waiter);

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::set_sensor_thr_done(ipmi_sensor_t *sensor, int err) {
  ipmiMonitorLogger log("ipmiMonitor::set_sensor_thr_done");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  if (err == 0) {
    setSensorThresholdsSp(ipmi_sensor_convert_to_id(sensor), NULL);
  } else {
    // if we fail, we cancel update - do not try again
    printLog("ERR\n");

    setSensorThresholdsSp(ipmi_sensor_convert_to_id(sensor), NULL);
  }
  os_handler_waiter_release(waiter);

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::sensor_hysteresis_done(ipmi_sensor_t *sensor, int err,
                                         unsigned int positive_hysteresis,
                                         unsigned int negative_hysteresis) {
  ipmiMonitorLogger log("ipmiMonitor::sensor_hysteresis_done");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  if (err == 0) {
    double hyst[2] = {(double)positive_hysteresis, (double)negative_hysteresis};

    setSensorHysteresisRd(ipmi_sensor_convert_to_id(sensor), hyst);
  }
  os_handler_waiter_release(waiter);

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::entity_hot_swap_state_done(ipmi_entity_t *ent, int err,
                                             enum ipmi_hot_swap_states state) {
  ipmiMonitorLogger log("ipmiMonitor::entity_hot_swap_state_done");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  // at this point we need to update our internal tree
  if (err == 0) {
    setBoardStateRd(ipmi_entity_convert_to_id(ent), state);
  }
  os_handler_waiter_release(waiter);

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);
}

void ipmiMonitor::entity_deactivate_done(ipmi_entity_t *ent, int err) {
  ipmiMonitorLogger log("ipmiMonitor::entity_deactivate_done");
  os_handler_waiter_release(waiter);
}

int ipmiMonitor::entity_hot_swap_event(ipmi_entity_t *ent,
                                       enum ipmi_hot_swap_states last_state,
                                       enum ipmi_hot_swap_states curr_state,
                                       ipmi_event_t *event) {
  ipmiMonitorLogger log("ipmiMonitor::entity_hot_swap_event");
  if (mutexSensorUpdateInProgress)
    epicsMutexMustLock(mutexSensorUpdateInProgress);

  setBoardStateRd(ipmi_entity_convert_to_id(ent), curr_state);

  epicsEventMustTrigger(eventSensorUpdateDone);

  if (mutexSensorUpdateInProgress)
    epicsMutexUnlock(mutexSensorUpdateInProgress);

  return 0;
}

// void ipmiMonitor::eventHandler( ipmi_domain_t *domainPtr, ipmi_event_t *event
// )
//{
//   std::cout << "dziabie " << ipmi_event_call_handler( domainPtr,
//   ipmiEventHandlers, event, this ) << std::endl ;
// }

int ipmiMonitor::analyzeThresholdEvents(ipmi_sensor_t *sensor,
                                        enum ipmi_event_dir_e dir,
                                        enum ipmi_thresh_e threshold,
                                        enum ipmi_event_value_dir_e high_low,
                                        enum ipmi_value_present_e value_present,
                                        unsigned int raw_value, double value,
                                        ipmi_event_t *event) {
  // the function is only called in scope of ipmi thread, so no need to protect
  // with mutex
  int a = getSensor(ipmi_sensor_convert_to_id(sensor));

  if (a < 0) return IPMI_EVENT_NOT_HANDLED;

  switch (threshold) {
    case IPMI_LOWER_NON_CRITICAL:
      sensors[a].thrlononcritstate = (dir == IPMI_ASSERTION) ? 1 : 0;
      break;
    case IPMI_LOWER_CRITICAL:
      sensors[a].thrlocritstate = (dir == IPMI_ASSERTION) ? 1 : 0;
      break;
    case IPMI_LOWER_NON_RECOVERABLE:
      sensors[a].thrlononrecstate = (dir == IPMI_ASSERTION) ? 1 : 0;
      break;
    case IPMI_UPPER_NON_CRITICAL:
      sensors[a].thrupnoncritstate = (dir == IPMI_ASSERTION) ? 1 : 0;
      break;
    case IPMI_UPPER_CRITICAL:
      sensors[a].thrupcritstate = (dir == IPMI_ASSERTION) ? 1 : 0;
      break;
    case IPMI_UPPER_NON_RECOVERABLE:
      sensors[a].thrupnonrecstate = (dir == IPMI_ASSERTION) ? 1 : 0;
      break;
  };

  switch (value_present) {
    case IPMI_NO_VALUES_PRESENT:
      break;
    case IPMI_RAW_VALUE_PRESENT:
      sensors[a].value_raw = raw_value;
      break;
    case IPMI_BOTH_VALUES_PRESENT:
      sensors[a].value_raw = raw_value;
      sensors[a].value = value;
      break;
  }

  return IPMI_EVENT_HANDLED;
}

int ipmiMonitor::analyzeDiscreteEvents(ipmi_sensor_t *sensor,
                                       enum ipmi_event_dir_e dir, int offset,
                                       int severity, int prev_severity,
                                       ipmi_event_t *event) {
  // we need to check if the event comes from nown board/sensor
  for (int i = 0; i < sensorCount; i++) {
    if (ipmi_cmp_sensor_id(sensors[i]._id, ipmi_sensor_convert_to_id(sensor))) {
      return IPMI_EVENT_NOT_HANDLED;
    }
  }

  printLog("New board detected !\n");

  syncStatus = 0;

  return IPMI_EVENT_HANDLED;
}

int ipmiMonitor::analyzeOtherEvents(ipmi_domain_t *domain,
                                    ipmi_event_t *event) {
  // it seems that if we get to this point, event comes from unknown board !
  // moreover ipmi_event_get_generating_sensor_id is returning wrong value -
  // instead of returning invalid sensor it is assigning one of existing sensor
  // to the event. In such case this method cannot be used to detect new boards
  // for now we assume that if we got here, we have new board and we invalidate

  //   ipmi_sensor_id_t sensor = ipmi_event_get_generating_sensor_id(domain,
  //   NULL, event ) ;
  //
  //   // we need to check if the event comes from nown board/sensor
  //   for ( int i = 0 ; i < sensorCount; i ++ )
  //   {
  //     if ( ipmi_cmp_sensor_id( sensors[i]._id, sensor ))
  //     {
  //        printLog("existing board: %s !\n", sensors[i].name ) ;
  //
  //        return IPMI_EVENT_HANDLED ;
  //     }
  //   }

  printLog("New board detected !\n");

  syncStatus = 0;

  return IPMI_EVENT_HANDLED;
}

void ipmiMonitor::entityPresenceChange(ipmi_entity_t *entity, int present) {
  if (present) return;

  syncStatus = 0;

  int index = getBoard(ipmi_entity_convert_to_id(entity));

  // mark board as do not update
  if (index >= 0) {
    boards[index].state = IPMI_HOT_SWAP_NOT_PRESENT;
    boards[index].fstate = 0;

    // mark all sensors for the board not ot update
    for (int i = 0; i < sensorCount; i++) {
      if (sensors[i].slot == boards[index].slot) {
        sensors[i].p = 0;
      }
    }
  }
}
