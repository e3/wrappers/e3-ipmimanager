#include <aSubRecord.h>
#include <dbDefs.h>
#include <epicsExport.h>
#include <registryFunction.h>
#include <stdio.h>
#include <string.h>

static long compVers(aSubRecord *prec) {
  int res;
  res = strcmp((char *)prec->a, (char *)prec->b);
  return res;
}

epicsRegisterFunction(compVers);
