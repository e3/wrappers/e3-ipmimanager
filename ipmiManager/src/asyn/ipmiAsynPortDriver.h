#include "alarm.h"
#include "asynPortDriver.h"
#include "ipmiMonitor.h"

//#define DBG_PARAMS

// macros for easy handling of param db
#ifdef DBG_PARAMS
#define PARAM_DBG_INT(NAME, VALUE) printf("%s: %d\n", NAME, VALUE);
#define PARAM_DBG_OCT(NAME, VALUE) printf("%s: %s\n", NAME, VALUE);
#define PARAM_DBG_DBL(NAME, VALUE) printf("%s: %f\n", NAME, VALUE);
#else
#define PARAM_DBG_INT(NAME, VALUE)
#define PARAM_DBG_OCT(NAME, VALUE)
#define PARAM_DBG_DBL(NAME, VALUE)
#endif

#ifdef DBG_PARAMS
#define SET_PARAM_DBG_INT(NAME, VALUE) printf("%s: %d\n", NAME, VALUE);
#define SET_PARAM_DBG_OCT(NAME, VALUE) printf("%s: %s\n", NAME, VALUE);
#define SET_PARAM_DBG_DBL(NAME, VALUE) printf("%s: %f\n", NAME, VALUE);
#else
#define SET_PARAM_DBG_INT(NAME, VALUE)
#define SET_PARAM_DBG_OCT(NAME, VALUE)
#define SET_PARAM_DBG_DBL(NAME, VALUE)
#endif

#define CREATE_AND_SET_STATUS(NAME, VALUE)    \
  {                                           \
    int ind;                                  \
    PARAM_DBG_INT(#NAME, VALUE);              \
    createParam(#NAME, asynParamInt32, &ind); \
    setIntegerParam(ind, VALUE);              \
  }
#define SET_STATUS(NAME, VALUE)            \
  {                                        \
    int ind;                               \
    PARAM_DBG_INT(#NAME, VALUE);           \
    findParam(#NAME, &ind);                \
    if (ind != -1)                         \
      setIntegerParam(ind, VALUE);         \
    else                                   \
      printf("error setting %s\n", #NAME); \
  }

#define CREATE_AND_SET_INTERVALS(NAME, VALUE, INDEX) \
  {                                                  \
    int ind;                                         \
    PARAM_DBG_INT(#NAME, VALUE);                     \
    createParam(#NAME, asynParamInt32, &ind);        \
    setIntegerParam(ind, VALUE);                     \
    monitor.setIntervals(INDEX, VALUE);              \
  }
#define CREATE_TIME_STAMPS(NAME)                     \
  {                                                  \
    int ind;                                         \
    createParam(#NAME, asynParamFloat64Array, &ind); \
  }
#define CREATE_AND_SET_EN(NAME, VALUE)        \
  {                                           \
    int ind;                                  \
    createParam(#NAME, asynParamInt32, &ind); \
    setIntegerParam(ind, VALUE);              \
    monitor.setEnableArch(VALUE);             \
  }

#define CREATE_AND_SET_INT(SLOT, NAME, VALUE) \
  {                                           \
    int ind;                                  \
    char name[256];                           \
    sprintf(name, "s%d.%s", SLOT, #NAME);     \
    PARAM_DBG_INT(name, VALUE);               \
    createParam(name, asynParamInt32, &ind);  \
    setIntegerParam(ind, VALUE);              \
  }
#define CREATE_AND_SET_OCT(SLOT, NAME, VALUE) \
  {                                           \
    int ind;                                  \
    char name[256];                           \
    sprintf(name, "s%d.%s", SLOT, #NAME);     \
    PARAM_DBG_OCT(name, VALUE);               \
    createParam(name, asynParamOctet, &ind);  \
    setStringParam(ind, VALUE);               \
  }
#define CREATE_AND_SET_DBL(SLOT, NAME, VALUE)  \
  {                                            \
    int ind;                                   \
    char name[256];                            \
    sprintf(name, "s%d.%s", SLOT, #NAME);      \
    PARAM_DBG_DBL(name, VALUE);                \
    createParam(name, asynParamFloat64, &ind); \
    setDoubleParam(ind, VALUE);                \
  }

#define SET_INT(SLOT, NAME, VALUE)        \
  {                                       \
    int ind;                              \
    char name[256];                       \
    sprintf(name, "s%d.%s", SLOT, #NAME); \
    PARAM_DBG_INT(name, VALUE);           \
    findParam(name, &ind);                \
    if (ind != -1)                        \
      setIntegerParam(ind, VALUE);        \
    else                                  \
      printf("error setting %s\n", name); \
  };
#define SET_OCT(SLOT, NAME, VALUE)        \
  {                                       \
    int ind;                              \
    char name[256];                       \
    sprintf(name, "s%d.%s", SLOT, #NAME); \
    PARAM_DBG_OCT(name, VALUE);           \
    findParam(name, &ind);                \
    if (ind != -1)                        \
      setStringParam(ind, VALUE);         \
    else                                  \
      printf("error setting %s\n", name); \
  };
#define SET_DBL(SLOT, NAME, VALUE)        \
  {                                       \
    int ind;                              \
    char name[256];                       \
    sprintf(name, "s%d.%s", SLOT, #NAME); \
    PARAM_DBG_DBL(name, VALUE);           \
    findParam(name, &ind);                \
    if (ind != -1)                        \
      setDoubleParam(ind, VALUE);         \
    else                                  \
      printf("error setting %s\n", name); \
  };

#define SENS_CREATE_AND_SET_INT(SLOT, SENS, NAME, VALUE) \
  {                                                      \
    int ind;                                             \
    char name[256];                                      \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME);      \
    PARAM_DBG_INT(name, VALUE);                          \
    createParam(name, asynParamInt32, &ind);             \
    setIntegerParam(ind, VALUE);                         \
  }
#define SENS_CREATE_AND_SET_OCT(SLOT, SENS, NAME, VALUE) \
  {                                                      \
    int ind;                                             \
    char name[256];                                      \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME);      \
    PARAM_DBG_OCT(name, VALUE);                          \
    createParam(name, asynParamOctet, &ind);             \
    setStringParam(ind, VALUE);                          \
  }
#define SENS_CREATE_AND_SET_DBL(SLOT, SENS, NAME, VALUE) \
  {                                                      \
    int ind;                                             \
    char name[256];                                      \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME);      \
    PARAM_DBG_DBL(name, VALUE);                          \
    createParam(name, asynParamFloat64, &ind);           \
    setDoubleParam(ind, VALUE);                          \
  }

#define SENS_SET_INT(SLOT, SENS, NAME, VALUE)       \
  {                                                 \
    int ind;                                        \
    char name[256];                                 \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME); \
    PARAM_DBG_INT(name, VALUE);                     \
    findParam(name, &ind);                          \
    if (ind != -1)                                  \
      setIntegerParam(ind, VALUE);                  \
    else                                            \
      printf("error setting %s\n", name);           \
  };
#define SENS_SET_OCT(SLOT, SENS, NAME, VALUE)       \
  {                                                 \
    int ind;                                        \
    char name[256];                                 \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME); \
    PARAM_DBG_OCT(name, VALUE);                     \
    findParam(name, &ind);                          \
    if (ind != -1)                                  \
      setStringParam(ind, VALUE);                   \
    else                                            \
      printf("error setting %s\n", name);           \
  };
#define SENS_SET_DBL(SLOT, SENS, NAME, VALUE)       \
  {                                                 \
    int ind;                                        \
    char name[256];                                 \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME); \
    PARAM_DBG_DBL(name, VALUE);                     \
    findParam(name, &ind);                          \
    if (ind != -1)                                  \
      setDoubleParam(ind, VALUE);                   \
    else                                            \
      printf("error setting %s\n", name);           \
  };
#define SENS_SET_WAVE(SLOT, SENS, NAME, STRUCT, POINT)        \
  {                                                           \
    int ind;                                                  \
    char name[256];                                           \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME);           \
    findParam(name, &ind);                                    \
    if (ind != -1) {                                          \
      epicsFloat64 *samples = STRUCT->data();                 \
      doCallbacksFloat64Array(POINT, STRUCT->size(), ind, 0); \
    } else                                                    \
      printf("error setting %s\n", name);                     \
  };

#define SENS_SET_VAL(SLOT, SENS, VALUE)               \
  {                                                   \
    int ind;                                          \
    char name[256];                                   \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, "value"); \
    SET_PARAM_DBG_DBL(name, VALUE);                   \
    findParam(name, &ind);                            \
    setDoubleParam(ind, VALUE);                       \
  };

#define SENS_GET_DBL(SLOT, SENS, NAME, VALUE)       \
  {                                                 \
    int ind;                                        \
    char name[256];                                 \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME); \
    PARAM_DBG_DBL(name, VALUE);                     \
    findParam(name, &ind);                          \
    getDoubleParam(ind, &VALUE);                    \
  };

#define SENS_SET_ALARM(SLOT, SENS, NAME, STATE)     \
  {                                                 \
    int ind;                                        \
    char name[256];                                 \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME); \
    findParam(name, &ind);                          \
    if (ind != -1) {                                \
      int status = 0;                               \
      int severity = 0;                             \
      if (!STATE) {                                 \
        status = UDF_ALARM;                         \
        severity = INVALID_ALARM;                   \
      }                                             \
      setParamAlarmStatus(ind, status);             \
      setParamAlarmSeverity(ind, severity);         \
    } else                                          \
      printf("eror setting %s\n", name);            \
  };

#define SET_ALARM(SLOT, NAME, STATE)        \
  {                                         \
    int ind;                                \
    char name[256];                         \
    sprintf(name, "s%d.%s", SLOT, #NAME);   \
    findParam(name, &ind);                  \
    if (ind != -1) {                        \
      int status = 0;                       \
      int severity = 0;                     \
      if (!STATE) {                         \
        status = UDF_ALARM;                 \
        severity = INVALID_ALARM;           \
      }                                     \
      setParamAlarmStatus(ind, status);     \
      setParamAlarmSeverity(ind, severity); \
    } else                                  \
      printf(" error setting %s\n", name);  \
  };

#define SET_ALARM(SLOT, NAME, STATE)        \
  {                                         \
    int ind;                                \
    char name[256];                         \
    sprintf(name, "s%d.%s", SLOT, #NAME);   \
    findParam(name, &ind);                  \
    if (ind != -1) {                        \
      int status = 0;                       \
      int severity = 0;                     \
      if (!STATE) {                         \
        status = 17;                        \
        severity = 3;                       \
      }                                     \
      setParamAlarmStatus(ind, status);     \
      setParamAlarmSeverity(ind, severity); \
    } else                                  \
      printf(" error setting %s\n", name);  \
  };

#define PASSTHRLOCRITVAL_CREATE() ;
#define PASSTHRLONONCRITVAL_CREATE() ;
#define PASSTHRUPNONCRITVAL_CREATE() ;
#define PASSTHRUPCRITVAL_CREATE() ;
#define SENS_EGU_CREATE() ;
#define SENS_WAVE_EGU_CREATE() ;

#define SENS_CREATE_WAVE(SLOT, SENS, NAME)          \
  {                                                 \
    int ind;                                        \
    char name[256];                                 \
    sprintf(name, "s%d.s%d.%s", SLOT, SENS, #NAME); \
    createParam(name, asynParamFloat64Array, &ind); \
  };

class ipmiAsynPortDriver : public asynPortDriver {
 public:
  ipmiAsynPortDriver(const char *portName, const char *connectionString,
                     int archiver, int archiverSize);
  ~ipmiAsynPortDriver();

  void terminateAllThreads();

  void createMonitor(const char *mchName);
  void ipmiMonitorTask();

  void ipmiMainTask();

  asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
  asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);

  board_info *getBoardInfo(int *count);
  sensor_info *getSensorInfo(int *count);

  int getTimeStampsLength();

  void diffTime(epicsFloat64 *time, unsigned int size, epicsFloat64 *output);
  int setInterval(char *name, IntervalsIndexes idx, int func, int val);

 protected:
 private:
  ipmiMonitor monitor;

  epicsEventId eventSensorUpdateDone;
  epicsMutexId mutexSensorUpdateInProgress;
  epicsMutexId mutexTerminate;

  int terminateWorkers;
  int ackFromWorkersCount;

  pthread_mutex_t ackFromWorkersLock;
  pthread_cond_t ackFromWorkersCond;
};
