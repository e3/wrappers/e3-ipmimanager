#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>
#include <epicsMutex.h>
#include <epicsString.h>
#include <epicsThread.h>
#include <epicsTime.h>
#include <epicsTimer.h>
#include <epicsTypes.h>
#include <errno.h>
#include <fcntl.h>
#include <iocsh.h>
#include <math.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "ipmiAsynPortDriver.h"

extern "C" {
#include "ipmiMonitor.h"
}

#define MAX_PORTS 8
#define MAX_MODULES 32

extern "C" {

static ipmiAsynPortDriver *ports[MAX_PORTS] = {0};
static unsigned int ipmiAsynPortCount = 0;

void exitFunc(void *) {
  for (unsigned int i = 0; i < ipmiAsynPortCount; i++) {
    ports[i]->terminateAllThreads();

    delete ports[i];
  }
}

int ipmiCreatePort(const char *portName, const char *connectionString,
                   int archiver, int archiverSize) {
  epicsAtExit(exitFunc, 0);

  if (ipmiAsynPortCount < MAX_PORTS) {
    ports[ipmiAsynPortCount++] = new ipmiAsynPortDriver(
        portName, connectionString, archiver, archiverSize);
    return (asynSuccess);
  } else {
    return (asynError);
  }
}

/* INIT Function */
static const iocshArg createArg0 = {"portName", iocshArgString};
static const iocshArg createArg1 = {"connStr", iocshArgString};
static const iocshArg createArg2 = {"enArch", iocshArgInt};
static const iocshArg createArg3 = {"sizeArch", iocshArgInt};
static const iocshArg *const createArgs[] = {&createArg0, &createArg1,
                                             &createArg2, &createArg3};

static const iocshFuncDef createFuncDef = {"ipmiCreatePort", 4, createArgs};

static void createCallFunc(const iocshArgBuf *args) {
  ipmiCreatePort(args[0].sval, args[1].sval, args[2].ival, args[3].ival);
}

void ipmiAsynCreatePortRegister(void) {
  iocshRegister(&createFuncDef, createCallFunc);
}

epicsExportRegistrar(ipmiAsynCreatePortRegister);
}

extern "C" {

int getTimeStamp(char *time, int size) {
  if (!size) {
    return 0;
  }
  struct timeval tv;
  time_t t;
  struct tm *info;
  gettimeofday(&tv, NULL);
  int millisec;
  millisec = lrint(tv.tv_usec / 1000.0);
  if (millisec >= 1000) {
    millisec -= 1000;
    tv.tv_sec++;
  }
  t = tv.tv_sec;
  info = localtime(&t);
  char buffer[64];
  strftime(buffer, sizeof buffer, "%d-%m-%Y %H:%M:%S", info);
  strncpy(time, buffer, size);
  return millisec;
}

void iocPrint(const char *format, ...) {
  char formatted[256] = {0};

  va_list args;
  va_start(args, format);
  { vsprintf(formatted, format, args); }
  va_end(args);
  char buffer[64];
  int millisec;
  millisec = getTimeStamp(buffer, sizeof(buffer) / sizeof(buffer[0]));
  errlogPrintf("%s.%03d ioc %s", buffer, millisec, formatted);
}

void libPrint(os_handler_t *handler, const char *format,
              enum ipmi_log_type_e log_type, va_list args) {
  char formatted[256] = {0};
  char type[16] = {0};
  int do_nl = 1;

  switch (log_type) {
    case IPMI_LOG_INFO:
      sprintf(type, "INFO: ");
      break;

    case IPMI_LOG_WARNING:
      sprintf(type, "WARN: ");
      break;

    case IPMI_LOG_SEVERE:
      sprintf(type, "SEVR: ");
      break;

    case IPMI_LOG_FATAL:
      sprintf(type, "FATL: ");
      break;

    case IPMI_LOG_ERR_INFO:
      sprintf(type, "EINF: ");
      break;

    case IPMI_LOG_DEBUG_START:
      do_nl = 0;
    /* FALLTHROUGH */
    case IPMI_LOG_DEBUG:
      sprintf(type, "DEBG: ");
      break;

    case IPMI_LOG_DEBUG_CONT:
      do_nl = 0;
    /* FALLTHROUGH */
    case IPMI_LOG_DEBUG_END:
      break;
  }

  { vsprintf(formatted, format, args); }
  char buffer[64];
  int millisec;
  millisec = getTimeStamp(buffer, sizeof(buffer) / sizeof(buffer[0]));
  errlogPrintf("%s.%03d lib %s %s", buffer, millisec, type, formatted);
  if (do_nl) printf("\n");
}
}

extern "C" {
static const iocshArg generateDbArg0 = {"portName", iocshArgString};
static const iocshArg generateDbArg1 = {"fileName", iocshArgString};
static const iocshArg generateDbArg2 = {"modeName", iocshArgInt};
static const iocshArg generateDbArg3 = {"deployDir", iocshArgString};
static const iocshArg *const generateDbArgs[] = {
    &generateDbArg0, &generateDbArg1, &generateDbArg2, &generateDbArg3};

static const iocshFuncDef generateDbFuncDef = {"ipmiGenerateDb", 4,
                                               generateDbArgs};
// structure to keep names of the detected modules
// they are used in sensor substitutions file creation
struct module {
  char name[64];
  int slot;
  char opts[20];
} modules[MAX_MODULES];

void initModules() {
  for (unsigned int i = 0; i < MAX_MODULES; i++) {
    modules[i].slot = 0;
    modules[i].name[0] = '\0';
  }
}
void addModule(char *name, int size, char *opts, int opt_size, int slot) {
  for (unsigned int i = 0; i < MAX_MODULES; i++) {
    if (modules[i].slot == 0) {
      modules[i].slot = slot;
      strncpy(modules[i].name, name, size);
      strncpy(modules[i].opts, opts, opt_size);
      break;
    }
  }
}

// need to find faster way of checking if on list
int isSlotEmpty(board_info *boards, int count, int slot) {
  for (int i = 0; i < count; i++) {
    if (boards[i].slot == slot) {
      return 0;
    }
  }
  return 1;
}

int generateDb(char *param1, char *param2, int mode, char *param3) {
  int fd_existing;
  int fd_sensors;
  initModules();

  for (unsigned int i = 0; i < ipmiAsynPortCount; i++) {
    if (strcmp(ports[i]->portName, param1) == 0) {
      char filename[256];

      iocPrint("   * port %s found\n", param1);

      sprintf(filename, "%s/%s.board.present.substitutions", param3, param2);
      fd_existing = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0660);

      if (fd_existing != -1) {
        iocPrint("   * file \"%s\" created\n", filename);
      } else {
        epicsExit(0);
        return 0;
      }

      {
        char buf[256];
        int len = 0;
        int boardCount = 0;

        board_info *boards = ports[i]->getBoardInfo(&boardCount);

        len = sprintf(buf,
                      "file \"$(ipmimanager_DIR)db/board.present.template\"\n");
        len = write(fd_existing, buf, len);
        len = sprintf(buf, "{\n");
        len = write(fd_existing, buf, len);
        len = sprintf(buf, "  pattern {      PREFIX,  INDEX,  OPT,  SLOT }\n");
        len = write(fd_existing, buf, len);

        int pm_cnt = 0;
        int cu_cnt = 0;

        for (int j = 0; j < 80; j++) {
          // board in chassis
          if (isSlotEmpty(boards, boardCount, j) == 0) {
            char idx[20];
            char device[20];
            char opt[10] = {};
            if (mode) {
              if (j == 64) {
                strcpy(opt, "Clk-");
              } else if (j == 65) {
                strcpy(opt, "PCI-");
              } else {
                sprintf(device, "$(SLOT%d_MODULE)", j);
                sprintf(idx, "$(SLOT%d_IDX)", j);
                strcpy(opt, "\"\"");
              }
            } else {
              // power modules possible slots
              if ((30 < j) && (j < 35)) {
                pm_cnt++;
                sprintf(idx, "%02d", pm_cnt);
                strcpy(device, "PM");
                strcpy(opt, "\"\"");
              }
              // cu possible slots
              else if ((47 < j) && (j < 50)) {
                cu_cnt++;
                sprintf(idx, "%02d", cu_cnt);
                strcpy(device, "CU");
                strcpy(opt, "\"\"");
              }
              // fixed slot clock
              else if (j == 64) {
                strcpy(idx, "00");
                strcpy(device, "MTCA");
                strcpy(opt, "Clk-");
              }
              // fixed PCI slot
              else if (j == 65) {
                strcpy(idx, "00");
                strcpy(device, "MTCA");
                strcpy(opt, "PCI-");
              }
              // AMCs possible slots
              else if ((16 > j) || (j > 29)) {
                sprintf(device, "$(SLOT%d_MODULE)", j);
                sprintf(idx, "$(SLOT%d_IDX)", j);
                strcpy(opt, "\"\"");
              }
              // RTMs possible slots
              else {
                sprintf(device, "$(SLOT%d_MODULE)", j);
                sprintf(idx, "$(SLOT%d_IDX)", j - 16);
                strcpy(opt, "\"\"");
              }
            }
            char name_for_sens[64];
            if (mode) {
              if ((j == 64) || (j == 65)) {
                len = sprintf(name_for_sens, "%s", ":");
              } else {
                len = sprintf(name_for_sens, "%s-%s:", device, idx);
              }
            } else {
              len = sprintf(name_for_sens, "%s-$(CRATE_NUM)%s:", device, idx);
            }
            // Add module names and slots to array of structures
            addModule(name_for_sens, len, opt, strlen(opt), j);
            if (mode) {
              if ((j == 64) || (j == 65)) {
                len = sprintf(buf, "          { $(P),  %s, %s, %5d }\n",
                              name_for_sens, opt, j);
              } else {
                len = sprintf(buf, "          { $(P)Ctrl-,  %s, %s, %5d }\n",
                              name_for_sens, opt, j);
              }
            } else {
              len = sprintf(buf, "          { $(P)-,  %s, %s, %5d }\n",
                            name_for_sens, opt, j);
            }
            len = write(fd_existing, buf, len);
          }
        }

        len = sprintf(buf, "}\n");
        len = write(fd_existing, buf, len);

        close(fd_existing);
      }

      sprintf(filename, "%s/%s.sensor.substitutions", param3, param2);
      fd_sensors = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0660);

      if (fd_sensors != -1) {
        char buf[256];
        int len = 0;

        iocPrint("   * file \"%s\" created\n", filename);

        int sensorCount = 0;
        sensor_info *sensors = ports[i]->getSensorInfo(&sensorCount);

        int archLen = 0;
        archLen = ports[i]->getTimeStampsLength();

        len = sprintf(buf, "file \"$(ipmimanager_DIR)db/sensor.template\"\n");
        len = write(fd_sensors, buf, len);
        len = sprintf(buf, "{\n");
        len = write(fd_sensors, buf, len);
        len = sprintf(buf,
                      "  pattern {      PREFIX,  INDEX,  OPT,  SLOT,  SINDEX, "
                      "ARCHIVER_SIZE,  DESC    }\n");
        len = write(fd_sensors, buf, len);

        for (int j = 0; j < sensorCount; j++) {
          for (unsigned int i = 0; i < MAX_MODULES; i++) {
            if (sensors[j].slot == modules[i].slot) {
              if (mode) {
                if ((modules[i].slot == 64) || (modules[i].slot == 65)) {
                  len =
                      sprintf(buf,
                              "          { $(P),  %s,  %s,  %5d,  %5d,    %5d, "
                              "  \"%s\" }\n",
                              modules[i].name, modules[i].opts, sensors[j].slot,
                              sensors[j].index, archLen, sensors[j].name);
                } else {
                  len =
                      sprintf(buf,
                              "          { $(P)Ctrl-,  %s,  %s,  %5d,  %5d,    "
                              "%5d,   \"%s\" }\n",
                              modules[i].name, modules[i].opts, sensors[j].slot,
                              sensors[j].index, archLen, sensors[j].name);
                }
              } else {
                len = sprintf(buf,
                              "          { $(P)-,  %s,  %s,  %5d,  %5d,    "
                              "%5d,   \"%s\" }\n",
                              modules[i].name, modules[i].opts, sensors[j].slot,
                              sensors[j].index, archLen, sensors[j].name);
              }
              len = write(fd_sensors, buf, len);
            }
          }
        }
        len = sprintf(buf, "}\n");
        len = write(fd_sensors, buf, len);

        close(fd_sensors);
      } else {
        epicsExit(0);
        return 0;
      }
    }
  }
  iocPrint(
      "Generation is finished\n If you want to run ioc, remove generateDb "
      "function\n");
  epicsExit(0);
  return (0);
}

static void generateDbCallFunc(const iocshArgBuf *args) {
  generateDb(args[0].sval, args[1].sval, args[2].ival, args[3].sval);
}

void ipmiGenerateDbRegister(void) {
  iocshRegister(&generateDbFuncDef, generateDbCallFunc);
}

epicsExportRegistrar(ipmiGenerateDbRegister);
}
