#include "ipmiAsynPortDriver.h"

#include <epicsEvent.h>
#include <epicsExport.h>
#include <epicsMutex.h>
#include <epicsString.h>
#include <epicsThread.h>
#include <epicsTime.h>
#include <epicsTimer.h>
#include <epicsTypes.h>
#include <errlog.h>
#include <errno.h>
#include <iocsh.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ipmiMonitor.h"

const int SENSOR_INTERVAL = 60;
const int THRESH_INTERVAL = 15;
const int EVENT_INTERVAL = 10;

extern "C" {

void iocPrint(const char *format, ...);
void libPrint(os_handler_t *handler, const char *format,
              enum ipmi_log_type_e log_type, va_list args);
}

// static const char *driverName="ipmiAsynPortDriver";
void simTask(void *drvPvt);

void ipmiMonitorTask(void *drvPvt) {
  ipmiAsynPortDriver *pPvt = (ipmiAsynPortDriver *)drvPvt;
  pPvt->ipmiMonitorTask();
}

void ipmiMainTask(void *drvPvt) {
  ipmiAsynPortDriver *pPvt = (ipmiAsynPortDriver *)drvPvt;
  pPvt->ipmiMainTask();
}

ipmiAsynPortDriver::ipmiAsynPortDriver(const char *portName,
                                       const char *connectionString,
                                       int archiver, int archiverSize)
    : asynPortDriver(portName, 1, /* maxAddr */
                     asynInt32Mask | asynFloat64Mask | asynOctetMask |
                         asynFloat64ArrayMask | asynEnumMask |
                         asynDrvUserMask, /* Interface mask */
                     asynInt32Mask | asynFloat64Mask | asynOctetMask |
                         asynFloat64ArrayMask |
                         asynEnumMask, /* Interrupt mask */
                     0, /* asynFlags. This driver does not block and it is not
                           multi-device, so flag is 0 */
                     1, /* Autoconnect */
                     0, /* Default priority */
                     0) /* Default stack size*/
      ,
      monitor((char *)connectionString, iocPrint, libPrint, archiverSize) {
  iocPrint("I'm constructor %s '%s'\n\n", portName, connectionString);

  mutexSensorUpdateInProgress = epicsMutexMustCreate();
  mutexTerminate = epicsMutexMustCreate();
  eventSensorUpdateDone = epicsEventCreate(epicsEventEmpty);

  // we create global params for the port
  {
    int ind = 0;
    createParam("tick", asynParamInt32, &ind);
    setIntegerParam(ind, 0);

    CREATE_AND_SET_INTERVALS(SensorInterval - SP, SENSOR_INTERVAL,
                             IntervalsIndexes::SENSOR);
    CREATE_AND_SET_INTERVALS(ThreshInterval - SP, THRESH_INTERVAL,
                             IntervalsIndexes::THRESH);
    CREATE_AND_SET_INTERVALS(EventInterval - SP, EVENT_INTERVAL,
                             IntervalsIndexes::EVENT);
    CREATE_TIME_STAMPS(TimeStamps);
    CREATE_AND_SET_EN(EnableArch, archiver);
    (archiver) ? iocPrint("Archiver turned on\n")
               : iocPrint("Archiver turned off\n");

    CREATE_AND_SET_STATUS(SyncStatus - RB, 1);
    CREATE_AND_SET_STATUS(ConnStatus - RB, 1);
  }

  // we need to iterate through entities and create params in database
  {
    int num_boards = 0;
    board_info *boards = monitor.getBoardInfo(&num_boards);

    for (int i = 0; i < num_boards; i++) {
      CREATE_AND_SET_INT(boards[i].slot, SensorCnt, boards[i].sensorcnt);
      CREATE_AND_SET_INT(boards[i].slot, P, boards[i].p);
      CREATE_AND_SET_INT(boards[i].slot, State, boards[i].state);
      CREATE_AND_SET_OCT(boards[i].slot, FruName, boards[i].fruname);
      CREATE_AND_SET_INT(boards[i].slot, FruId, boards[i].fruid);
      CREATE_AND_SET_INT(boards[i].slot, Slot, boards[i].slot);
      CREATE_AND_SET_OCT(boards[i].slot, BManuf, boards[i].bmanuf);
      CREATE_AND_SET_OCT(boards[i].slot, BName, boards[i].bname);
      CREATE_AND_SET_OCT(boards[i].slot, BSN, boards[i].bsn);
      CREATE_AND_SET_OCT(boards[i].slot, BPN, boards[i].bpn);
      CREATE_AND_SET_OCT(boards[i].slot, BFruFileId, boards[i].bfrufileid);
      CREATE_AND_SET_OCT(boards[i].slot, PManuf, boards[i].pmanuf);
      CREATE_AND_SET_OCT(boards[i].slot, PName, boards[i].pname);
      CREATE_AND_SET_OCT(boards[i].slot, PN, boards[i].pn);
      CREATE_AND_SET_OCT(boards[i].slot, PPV, boards[i].ppv);
      CREATE_AND_SET_OCT(boards[i].slot, PSN, boards[i].psn);
      CREATE_AND_SET_OCT(boards[i].slot, PAT, boards[i].pat);
      CREATE_AND_SET_OCT(boards[i].slot, PFruFileId, boards[i].pfrufileid);
      CREATE_AND_SET_INT(boards[i].slot, FState, 0);

      // create empty params to get rid of asyn errors
      for (int j = 0; j < 64; j++) {
        // general info on params
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, EvtrdType, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, SdrType, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, P, 0);
        SENS_CREATE_AND_SET_OCT(boards[i].slot, j, Name, "");
        SENS_CREATE_AND_SET_OCT(boards[i].slot, j, Type, "");
        SENS_CREATE_AND_SET_OCT(boards[i].slot, j, Units, "");
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, NormMax, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, NormMin, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrCommit, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrUpdtAllow, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrUpNonRec, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrUpCrt, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrUpNonCrt, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrLoNonRec, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrLoCrt, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrLoNonCrt, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrUpNonRec - SP, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrUpCrt - SP, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrUpNonCrt - SP, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrLoNonRec - SP, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrLoCrt - SP, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, ThrLoNonCrt - SP, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrUpNonRecStat, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrUpCrtStat, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrUpNonCrtStat, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrLoNonRecStat, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrLoCrtStat, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrLoNonCrtStat, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, HystPos, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, HystNeg, 0);
        SENS_CREATE_AND_SET_DBL(boards[i].slot, j, Value, 0);
        SENS_CREATE_AND_SET_INT(boards[i].slot, j, ThrNotActual, 0);
      }
    }
    // CREATE_AND_SET_( char mrec[4] ; // multi record area
  }

  //
  {
    // we need to iterate through sensors and create params
    int num_sensors = 0;
    sensor_info *sensors = monitor.getSensorInfo(&num_sensors);

    for (int i = 0; i < num_sensors; i++) {
      // general info on params
      SENS_SET_INT(sensors[i].slot, sensors[i].index, EvtrdType,
                   sensors[i].evtrdtype);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, SdrType,
                   sensors[i].sdrtype);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, P, sensors[i].p);
      SENS_SET_OCT(sensors[i].slot, sensors[i].index, Name, sensors[i].name);
      SENS_SET_OCT(sensors[i].slot, sensors[i].index, Type, sensors[i].type);
      SENS_SET_OCT(sensors[i].slot, sensors[i].index, Units, sensors[i].units);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, NormMax,
                   sensors[i].normmax);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, NormMin,
                   sensors[i].normmin);

      // thresholds
      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrCommit, 0);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrUpdtAllow,
                   sensors[i].thrUpdateAllowed);

      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpNonRec,
                   sensors[i].thrupnonrecval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpCrt,
                   sensors[i].thrupcritval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpNonCrt,
                   sensors[i].thrupnoncritval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoNonRec,
                   sensors[i].thrlononrecval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoCrt,
                   sensors[i].thrlocritval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoNonCrt,
                   sensors[i].thrlononcritval);

      // setpoints for thresholds
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpNonRec - SP,
                   sensors[i].thrupnonrecval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpCrt - SP,
                   sensors[i].thrupcritval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpNonCrt - SP,
                   sensors[i].thrupnoncritval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoNonRec - SP,
                   sensors[i].thrlononrecval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoCrt - SP,
                   sensors[i].thrlocritval);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoNonCrt - SP,
                   sensors[i].thrlononcritval);

      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrUpNonRecStat,
                   sensors[i].thrupnonrecstate);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrUpCrtStat,
                   sensors[i].thrupcritstate);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrUpNonCrtStat,
                   sensors[i].thrupnoncritstate);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrLoNonRecStat,
                   sensors[i].thrlononrecstate);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrLoCrtStat,
                   sensors[i].thrlocritstate);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrLoNonCrtStat,
                   sensors[i].thrlononcritstate);
      SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrNotActual, 0);

      // hysteresis
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, HystPos,
                   sensors[i].hystpos);
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, HystNeg,
                   sensors[i].hystneg);

      // Pass thresholds to alarm fields records
      PASSTHRLOCRITVAL_CREATE();
      PASSTHRLONONCRITVAL_CREATE();
      PASSTHRUPNONCRITVAL_CREATE();
      PASSTHRUPCRITVAL_CREATE();

      // value params and other runtime params
      SENS_SET_DBL(sensors[i].slot, sensors[i].index, Value, sensors[i].value);

      // pass unit to egu field
      SENS_EGU_CREATE();
      SENS_WAVE_EGU_CREATE();

      // waveform for archiving sensor values
      SENS_CREATE_WAVE(sensors[i].slot, sensors[i].index, Wave);
    }
  }

  // here we are ready to start monitor theread and update thread
  terminateWorkers = 0;
  ackFromWorkersCount = 0;

  pthread_mutex_init(&ackFromWorkersLock, NULL);
  pthread_cond_init(&ackFromWorkersCond, NULL);

  (asynStatus)(
      epicsThreadCreate(connectionString, epicsThreadPriorityMedium,
                        epicsThreadGetStackSize(epicsThreadStackMedium),
                        (EPICSTHREADFUNC)::ipmiMonitorTask, this) == NULL);

  (asynStatus)(
      epicsThreadCreate("MainSensorLoop", epicsThreadPriorityMedium,
                        epicsThreadGetStackSize(epicsThreadStackMedium),
                        (EPICSTHREADFUNC)::ipmiMainTask, this) == NULL);
}

void ipmiAsynPortDriver::terminateAllThreads() {
  ipmiMonitorLogger("ipmiAsynPortDriver::terminateAllThreads()");

  epicsMutexMustLock(mutexTerminate);
  terminateWorkers = 1;
  epicsMutexUnlock(mutexTerminate);

  pthread_mutex_lock(&ackFromWorkersLock);
  do {
    if (ackFromWorkersCount == 2) {
      break;
    } else {
      pthread_cond_wait(&ackFromWorkersCond, &ackFromWorkersLock);
    }
  } while (1);

  pthread_mutex_unlock(&ackFromWorkersLock);
}

ipmiAsynPortDriver::~ipmiAsynPortDriver() {
  ipmiMonitorLogger("~ipmiAsynPortDriver()");

  iocPrint("Closing connection\n");
}

void ipmiAsynPortDriver::ipmiMonitorTask() {
  monitor.run(eventSensorUpdateDone, mutexSensorUpdateInProgress,
              &terminateWorkers, mutexTerminate);

  pthread_mutex_lock(&ackFromWorkersLock);
  ++ackFromWorkersCount;
  pthread_cond_signal(&ackFromWorkersCond);
  pthread_mutex_unlock(&ackFromWorkersLock);
}

void ipmiAsynPortDriver::ipmiMainTask() {
  // we need to iterate through sensors and create params
  epicsMutexMustLock(mutexTerminate);
  while (terminateWorkers == 0) {
    epicsMutexUnlock(mutexTerminate);

    epicsEventMustWait(eventSensorUpdateDone);

    lock();
    epicsMutexMustLock(mutexSensorUpdateInProgress);
    {
      // update tick
      {
        int ind = -1;
        findParam("tick", &ind);

        if (ind != -1) {
          setIntegerParam(ind, monitor.getTick());
        } else {
          iocPrint("error setting tick\n");
        }
      }
      // check archiver enable
      int ind = -1;
      int enable = 0;

      findParam("EnableArch", &ind);
      if (ind != -1) {
        getIntegerParam(ind, &enable);
      }

      // update waveform with timestamp
      if (enable) {
        int ind = -1;
        unsigned int size = 0;
        size = monitor.getTimeStampsLength();
        epicsFloat64 *stamps;
        stamps = monitor.getTimeStamps();
        epicsFloat64 output[size];
        diffTime(stamps, size, output);
        findParam("TimeStamps", &ind);
        if (ind != -1) {
          doCallbacksFloat64Array(output, size, ind, 0);
        }
      }

      // are we still in sync ?
      int connStatus = monitor.getConnected();
      {
        SET_STATUS(SyncStatus - RB, monitor.getSync());
        SET_STATUS(ConnStatus - RB, connStatus);
      }

      {
        // update hotswap state
        {
          int num_boards = 0;
          board_info *boards = monitor.getBoardInfo(&num_boards);

          for (int i = 0; i < num_boards; i++) {
            SET_INT(boards[i].slot, state, boards[i].state);
            // setting alarms
            SET_ALARM(boards[i].slot, SensorCnt, connStatus);
            SET_ALARM(boards[i].slot, P, connStatus);
            SET_ALARM(boards[i].slot, State, connStatus);
            SET_ALARM(boards[i].slot, FruName, connStatus);
            SET_ALARM(boards[i].slot, FruId, connStatus);
            SET_ALARM(boards[i].slot, Slot, connStatus);
            SET_ALARM(boards[i].slot, BManuf, connStatus);
            SET_ALARM(boards[i].slot, BName, connStatus);
            SET_ALARM(boards[i].slot, BSN, connStatus);
            SET_ALARM(boards[i].slot, BPN, connStatus);
            SET_ALARM(boards[i].slot, BFruFileId, connStatus);
            SET_ALARM(boards[i].slot, PManuf, connStatus);
            SET_ALARM(boards[i].slot, PName, connStatus);
            SET_ALARM(boards[i].slot, PN, connStatus);
            SET_ALARM(boards[i].slot, PPV, connStatus);
            SET_ALARM(boards[i].slot, PSN, connStatus);
            SET_ALARM(boards[i].slot, PAT, connStatus);
            SET_ALARM(boards[i].slot, PFruFileId, connStatus);
            SET_ALARM(boards[i].slot, FState, connStatus);
          }
        }

        // update alarms and thresholds
        {
          int num_sensors = 0;
          sensor_info *sensors = monitor.getSensorInfo(&num_sensors);
          for (int i = 0; i < num_sensors; i++) {
            if (connStatus) {
              SENS_SET_VAL(sensors[i].slot, sensors[i].index, sensors[i].value);
            } else {
              SENS_SET_VAL(sensors[i].slot, sensors[i].index, 0.0);
            }
            SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpNonRec,
                         sensors[i].thrupnonrecval);
            SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpCrt,
                         sensors[i].thrupcritval);
            SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrUpNonCrt,
                         sensors[i].thrupnoncritval);
            SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoNonRec,
                         sensors[i].thrlononrecval);
            SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoCrt,
                         sensors[i].thrlocritval);
            SENS_SET_DBL(sensors[i].slot, sensors[i].index, ThrLoNonCrt,
                         sensors[i].thrlononcritval);

            SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrUpNonRecStat,
                         sensors[i].thrupnonrecstate);
            SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrUpCrtStat,
                         sensors[i].thrupcritstate);
            SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrUpNonCrtStat,
                         sensors[i].thrupnoncritstate);
            SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrLoNonRecStat,
                         sensors[i].thrlononrecstate);
            SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrLoCrtStat,
                         sensors[i].thrlocritstate);
            SENS_SET_INT(sensors[i].slot, sensors[i].index, ThrLoNonCrtStat,
                         sensors[i].thrlononcritstate);
            // setting alarms
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, value,
                           connStatus);

            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, EvtrdType,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, SdrType,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, P, connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, Name, connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, Type, connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, Units,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, NormMax,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, NormMin,
                           connStatus);

            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrUpNonRec,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrUpCrt,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrUpNonCrt,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrLoNonRec,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrLoCrt,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrLoNonCrt,
                           connStatus);

            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrUpNonRecStat,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrUpCrtStat,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrUpNonCrtStat,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrLoNonRecStat,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrLoCrtStat,
                           connStatus);
            SENS_SET_ALARM(sensors[i].slot, sensors[i].index, ThrLoNonCrtStat,
                           connStatus);

            if (enable) {
              SENS_SET_WAVE(sensors[i].slot, sensors[i].index, Wave,
                            sensors[i].arch, samples);
            }
          }
        }
        callParamCallbacks();
      }
    }
    epicsMutexUnlock(mutexSensorUpdateInProgress);
    unlock();
    epicsMutexMustLock(mutexTerminate);
  }
  epicsMutexUnlock(mutexTerminate);

  pthread_mutex_lock(&ackFromWorkersLock);
  ++ackFromWorkersCount;
  pthread_cond_signal(&ackFromWorkersCond);
  pthread_mutex_unlock(&ackFromWorkersLock);
}

// Setting flag in any threshold was changed
asynStatus ipmiAsynPortDriver::writeFloat64(asynUser *pasynUser,
                                            epicsFloat64 value) {
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;

  int num_sensors = 0;

  /* Set the parameter in the parameter library. */
  status = (asynStatus)setDoubleParam(function, value);
  if (status != asynSuccess) {
    return status;
  }

  sensor_info *sensors = monitor.getSensorInfo(&num_sensors);

  for (int i = 0; i < num_sensors; i++) {
    int ind;
    char name[256];
    const char *thrs[6] = {"ThrLoNonRec-SP", "ThrLoCrt-SP", "ThrLoNonCrt-SP",
                           "ThrUpNonCrt-SP", "ThrUpCrt-SP", "ThrUpNonRec-SP"};
    for (int j = 0; j < 6; j++) {
      memset(name, 0, sizeof name);
      sprintf(name, "s%d.s%d.%s", sensors[i].slot, sensors[i].index, thrs[j]);
      findParam(name, &ind);
      if (function == ind) {
        memset(name, 0, sizeof name);
        sprintf(name, "s%d.s%d.ThrNotActual", sensors[i].slot,
                sensors[i].index);
        findParam(name, &ind);
        if (ind != -1) {
          setIntegerParam(ind, 1);
          callParamCallbacks();
          return status;
        }
      }
    }
  }
  callParamCallbacks();
  return status;
}

int ipmiAsynPortDriver::setInterval(char *name, IntervalsIndexes idx, int func,
                                    int val) {
  int ind;
  findParam(name, &ind);
  if (func == ind) {
    monitor.setIntervals(idx, val);
    iocPrint("Changed time interval for %s it will be %d sec\n", name, val);
    return 1;
  }
  return 0;
}

// monitor commits for sensor thrs
// no need to lock db or mutex - we are only reading data
asynStatus ipmiAsynPortDriver::writeInt32(asynUser *pasynUser,
                                          epicsInt32 value) {
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;

  int num_sensors = 0;
  int num_boards = 0;

  /* Set the parameter in the parameter library. */
  status = (asynStatus)setIntegerParam(function, value);
  if (status != asynSuccess) {
    return status;
  }

  /* Update intervals */
  int done = 0;
  done = setInterval((char *)"SensorInterval-SP", IntervalsIndexes::SENSOR,
                     function, value);
  if (done) {
    callParamCallbacks();
    return status;
  }
  done = setInterval((char *)"ThreshInterval-SP", IntervalsIndexes::THRESH,
                     function, value);
  if (done) {
    callParamCallbacks();
    return status;
  }
  done = setInterval((char *)"EventInterval-SP", IntervalsIndexes::EVENT,
                     function, value);
  if (done) {
    callParamCallbacks();
    return status;
  }

  // Update enable archiver
  int ind;
  findParam("enable_arch", &ind);
  if (function == ind) {
    monitor.setEnableArch(value);
    (value) ? iocPrint("Archiver turned on\n")
            : iocPrint("Archiver turned off\n");
    callParamCallbacks();
    return status;
  }

  // we need to check if we are fstate of some board
  board_info *boards = monitor.getBoardInfo(&num_boards);

  for (int i = 0; i < num_boards; i++) {
    int ind;
    char name[256];
    sprintf(name, "s%d.FState", boards[i].slot);
    findParam(name, &ind);

    if (function == ind) {
      monitor.setBoardFStateSp(boards[i]._id, value);

      iocPrint("board fstate pressed for %d\n", boards[i].slot);
      callParamCallbacks();
      return status;
    }
  }

  // we need to check if we are commit of some sensor
  sensor_info *sensors = monitor.getSensorInfo(&num_sensors);
  for (int i = 0; i < num_sensors; i++) {
    int ind;
    char name[256];
    sprintf(name, "s%d.s%d.ThrCommit", sensors[i].slot, sensors[i].index);
    findParam(name, &ind);

    if (function == ind) {
      double val[6] = {0};

      SENS_GET_DBL(sensors[i].slot, sensors[i].index, ThrUpNonRec - SP, val[0]);
      SENS_GET_DBL(sensors[i].slot, sensors[i].index, ThrUpCrt - SP, val[1]);
      SENS_GET_DBL(sensors[i].slot, sensors[i].index, ThrUpNonCrt - SP, val[2]);
      SENS_GET_DBL(sensors[i].slot, sensors[i].index, ThrLoNonCrt - SP, val[3]);
      SENS_GET_DBL(sensors[i].slot, sensors[i].index, ThrLoCrt - SP, val[4]);
      SENS_GET_DBL(sensors[i].slot, sensors[i].index, ThrLoNonRec - SP, val[5]);

      monitor.setSensorThresholdsSp(sensors[i]._id, val);

      memset(name, 0, sizeof name);
      sprintf(name, "s%d.s%d.ThrNotActual", sensors[i].slot, sensors[i].index);
      findParam(name, &ind);
      if (ind != -1) {
        setIntegerParam(ind, 0);
      }
      iocPrint("commit pressed for %d %d\n", sensors[i].slot, sensors[i].index);

      callParamCallbacks();
      return status;
    }
  }
  callParamCallbacks();

  return status;
}

board_info *ipmiAsynPortDriver::getBoardInfo(int *count) {
  return monitor.getBoardInfo(count);
}

sensor_info *ipmiAsynPortDriver::getSensorInfo(int *count) {
  return monitor.getSensorInfo(count);
}

int ipmiAsynPortDriver::getTimeStampsLength() {
  return monitor.getTimeStampsLength();
}

void ipmiAsynPortDriver::diffTime(epicsFloat64 *time, unsigned int size,
                                  epicsFloat64 *output) {
  unsigned int i;
  epicsFloat64 cur = time[size - 1];
  for (i = 0; i < size; i++) {
    if (i != size) {
      output[i] = roundf((time[i] - cur) / 60 * 100) / 100;
    }
  }
}
