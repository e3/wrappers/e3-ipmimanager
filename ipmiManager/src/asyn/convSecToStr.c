#include <aSubRecord.h>
#include <dbDefs.h>
#include <epicsExport.h>
#include <registryFunction.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define BUFFER_SIZE 20

static long convSecToStr(aSubRecord *prec) {
  char array[prec->noa][BUFFER_SIZE];
  memset(array, 0, sizeof array);
  epicsFloat64 *samples = (epicsFloat64 *)prec->a;
  int i;
  for (i = 0; i < prec->noa; i++) {
    char buffer[BUFFER_SIZE];
    struct tm *info;
    long ts = (long)samples[i];
    info = localtime(&ts);
    strftime(buffer, sizeof buffer, "%d-%m-%Y %H:%M:%S", info);
    strcpy(array[i], buffer);
  }
  prec->neva = prec->noa;
  memcpy(prec->vala, array, prec->nova * sizeof(char) * BUFFER_SIZE);

  return 0;
}

epicsRegisterFunction(convSecToStr);
