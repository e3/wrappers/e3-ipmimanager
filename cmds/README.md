# e3-ipmimanager

## Setting up your IOC

Creating an IOC that uses ipmimanager is a little bit complicated and requires a bit of setup; you have to generate substitution files based on your hardware configuration.

### Substitution files generation

**NOTE** Substitution files have to be generated on a crate basis. It cannot be used at run-time - execution of ipmiGenerateDb functions exits IOC after generation

The generation of the substitutions files is a separate utility. See the `gennerateDb.cmd` file:

```
require ipmimanager

iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=$(DB_NAME=dynamic), MCH_ADDR=$(MCH_ADDR), DEPLOYMENT_DIR=$(DEPLOYMENT_DIR=/tmp/auto-gen-db), GENERATE_DB=, NAME_MODE=$(NAME_MODE=0)")
```

To generate substitutions files an operator needs to define at least:
 - `$(MCH_ADDR)` - IP address of the MTCA chassis
Additionally, a user can modify path where the files will be saved `$(DEPLOYMENT_DIR)` and naming convention mode `$(NAME_MODE)`. Those macros have default values.

**NOTE** The path, defined in `$(DEPLOYMENT_DIR)` must be an existing directory. The snippet won't create this directory for you.

Those macros can be passed via command line:
``` sh
MCH_ADDR=10.1.3.97 iocsh.bash cmds/generateDb.cmd
```

If you want to run IOC in deployed location you are not allowed to use `GENERATE_DB` flag - it will prevent IOC from running.

### Set up snippet and database files for your IOC

After you have generated substition files, you should copy these to your IOC repository together with the file `chassis.iocsh`.

### Startup script

For sample MTCA chassis, there is set of two cmd files - one for each naming convention style. You can take the example you need based on your system naming and copy it to your IOC repository.

**NAME_MODE = 0 (standard naming convention for MTCA chassis and its modules)**

To launch IOC, you need to use `example_ess_name0.cmd` file. Several features of the e3-ipmimnager can be configured there,
 - `$(P)` prefix, the first part of PVs names,
 - `$(CRATE_NUM)` number of the MTCA chassis,
 - `$(DISP_MSG)` if set to 1, the ioc will print all messages on the IOC console, if set to 0 only the most important messages will be generated,
 - `$(MCH_ADDR)` is an IP address of the MCH,
 - `$(TELNET_PORT)` is a ip port for telnet session to connect with MCH (it used to read link state),
 - `$(ARCHIVER)` enables internal archiving mechanism, if set to 1, archiver is turned on, set to 0, archiver is disabled,
 - `$(ARCHIVER_SIZE)` number of samples to be stored with internal archiver,
 - `$(MODE_NAME)` if set to 0, the standard naming convention for MTCA chassis will be applied, if set to 1, the non-standard convention can be implemented
 - `$(MTCA_PREF)` prefix for non-dynamic PVs for MTCA chassis settings
 - `$(IOC_PREF)` prefix for non-dynamic PVs for IOC settings
 - `$(DEPLOYMENT_DIR)` path with subsitutions files

Each AMC module has two macros to be defined:
 - `SLOT_X_IDX`
 - `SLOT_X_MODULE`

where X represents a slot of the given module. It means that before running the IOC, an operator has to know the localization of all AMC boards.
AMC modules can be named differently, their names should follow ESS naming Convention. The macro SLOT_X_MODULE represents those names.
The macro SLOT_X_IDX is two digits in a three-digit index. The first digit stands for the crate number, the two others symbolize index which form is dependent on the module type. Verify Naming Convention [here](https://chess.esss.lu.se/enovia/link/ESS-0000757/21308.51166.45568.45993/valid) before introducing the indexes.
For RTMs modules, there is only SLOT_X_MODULE macro that needs to be defined since their slot is the same as the AMC module it is attached to.
For the rest of the modules like PMs, CUs, Clock, PCI there is no need to define any macros. Their names and indexes are strictly described in Naming Convention and there is no other way to use different names and indexes.

For better understanding, check substitutions file presented below:
```sh
file "$(ipmimanager_DIR)db/board.present.template"
{
  pattern {      PREFIX,  INDEX,  OPT,  SLOT }
          { $(P)Ctrl,  $(SLOT1_MODULE)-$(CRATE_NUM)$(SLOT1_IDX):, "",     1 }
          { $(P)Ctrl,  $(SLOT2_MODULE)-$(CRATE_NUM)$(SLOT2_IDX):, "",     2 }
          { $(P)Ctrl,  $(SLOT3_MODULE)-$(CRATE_NUM)$(SLOT3_IDX):, "",     3 }
          { $(P)Ctrl,  $(SLOT5_MODULE)-$(CRATE_NUM)$(SLOT5_IDX):, "",     5 }
          { $(P)Ctrl,  $(SLOT7_MODULE)-$(CRATE_NUM)$(SLOT7_IDX):, "",     7 }
          { $(P)Ctrl,  $(SLOT19_MODULE)-$(CRATE_NUM)$(SLOT3_IDX):, "",    19 }
          { $(P)Ctrl,  $(SLOT21_MODULE)-$(CRATE_NUM)$(SLOT5_IDX):, "",    21 }
          { $(P)Ctrl,  $(SLOT23_MODULE)-$(CRATE_NUM)$(SLOT7_IDX):, "",    23 }
          { $(P)Ctrl,  PM-$(CRATE_NUM)01:, "",    32 }
          { $(P)Ctrl,  CU-$(CRATE_NUM)01:, "",    48 }
          { $(P)Ctrl,  CU-$(CRATE_NUM)02:, "",    49 }
          { $(P)Ctrl,  MTCA-$(CRATE_NUM)00:, Clk-,    64 }
          { $(P)Ctrl,  MTCA-$(CRATE_NUM)00:, PCI-,    65 }
}
```

**NAME_MODE = 1 (non-standard naming convention for MTCA chassis and its modules)**

To launch IOC, you need to use `example_ess_name1.cmd` file. Several features of the e3-ipmimnager can be configured there,
 - `$(P)` prefix, the first part of PVs names,
 - `$(CRATE_NUM)` number of the MTCA chassis,
 - `$(DISP_MSG)` if set to 1, the ioc will print all messages on the IOC console, if set to 0 only the most important messages will be generated,
 - `$(MCH_ADDR)` is an IP address of the MCH,
 - `$(TELNET_PORT)` is a ip port for telnet session to connect with MCH (it used to read link state),
 - `$(ARCHIVER)` enables internal archiving mechanism, if set to 1, archiver is turned on, set to 0, archiver is disabled,
 - `$(ARCHIVER_SIZE)` number of samples to be stored with internal archiver,
 - `$(MODE_NAME)` if set to 0, the standard naming convention for MTCA chassis will be applied, if set to 1, the non-standard convention can be implemented
 - `$(MTCA_PREF)` prefix for non-dynamic PVs for MTCA chassis settings
 - `$(IOC_PREF)` prefix for non-dynamic PVs for IOC settings
 - `$(DEPLOYMENT_DIR)` path with subsitutions files

Each AMC module has two macros to be defined:
 - `SLOT_X_IDX`
 - `SLOT_X_MODULE`

where X represents a slot of the given module. It means that before running the IOC, an operator has to know the localization of all AMC boards.
AMC modules can be named differently. The macro SLOT_X_MODULE represents those names.
The macro SLOT_X_IDX is a custom index.

This mode allows for a more flexible configuration of MTCA modules. When NAME_MODE set to 1, an operator has to fix not only AMC modules but also PMs and CUs and their indexes.

For better understanding, check substitutions file presented below:

```sh
file "$(ipmimanager_DIR)db/board.present.template"
{
  pattern {      PREFIX,  INDEX,  OPT,  SLOT }
          { $(P)Ctrl-,  $(SLOT1_MODULE)-$(SLOT1_IDX):, "",     1 }
          { $(P)Ctrl-,  $(SLOT2_MODULE)-$(SLOT2_IDX):, "",     2 }
          { $(P)Ctrl-,  $(SLOT3_MODULE)-$(SLOT3_IDX):, "",     3 }
          { $(P)Ctrl-,  $(SLOT5_MODULE)-$(SLOT5_IDX):, "",     5 }
          { $(P)Ctrl-,  $(SLOT7_MODULE)-$(SLOT7_IDX):, "",     7 }
          { $(P)Ctrl-,  $(SLOT19_MODULE)-$(SLOT19_IDX):, "",    19 }
          { $(P)Ctrl-,  $(SLOT21_MODULE)-$(SLOT21_IDX):, "",    21 }
          { $(P)Ctrl-,  $(SLOT23_MODULE)-$(SLOT23_IDX):, "",    23 }
          { $(P)Ctrl-,  $(SLOT32_MODULE)-$(SLOT32_IDX):, "",    32 }
          { $(P)Ctrl-,  $(SLOT48_MODULE)-$(SLOT48_IDX):, "",    48 }
          { $(P)Ctrl-,  $(SLOT49_MODULE)-$(SLOT49_IDX):, "",    49 }
          { $(P),  :, Clk-,    64 }
          { $(P),  :, PCI-,    65 }
}
```

## General settings (common for both modes)

In cmd file, an operator can also configure the behavior of the IOC:

 - `GENERATE_DB` → if set to empty (GENERATE_DB=) generates templates and substitutions files - dynamic mode, this mode cannot be used in runtime ioc mode (it will exit IOC)
 - `USE_STREAM` → if set to empty (USE_STREAM=) loads PVs with StreamDevice device support to use telnet co communicate with MCH, if not defined the database will not be loaded
 - `STREAM_PORT` → port for telnet connection with MCH
 - `USE_EXPERT` → if set to empty (USE_EXPERT=) loads PVs to reboot and shutdown chassis/boards, if not the database will not be loaded (it goes together with USE_STREAM and STREAM_PORT macros)
 - `USE_STREAM_PASS` → for password-protected MCH telnet communication, if set to empty (USE_STREAM_PASS=) the IOC will load the special protocol file with password
 - `STREAM_PASS` → password for MCH telnet connection
 - `CHECK_VER` → if set to empty (CHECK_VER=) the ioc will load PV to check panel version and module version


**MCH with password**

To use the stream layer when MCH is password-protected, you need to define the macro with the password (in this case `PASS` macro). Then, in iocshLoad you need to add two macros:
`USE_STREAM_PASS` (with an empty value), and `STREAM_PASS` with a password to the MCH (in this case to `PASS` macro value was assigned).
The rest of the macros stay with no changes.

Check out the example:

```
epicsEnvSet("PASS", "12345678")

iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=dynamic,MCH_ADDR=$(MCH_ADDR),ARCHIVER=$(ARCHIVER),ARCHIVER_SIZE=$(ARCHIVER_SIZE),P=$(P), CRATE_NUM=$(CRATE_NUM),TIMEOUT=10,USE_STREAM=,USE_STREAM_PASS=,STREAM_PASS=$(PASS),STREAM_PORT=$(TELNET_PORT),USE_EXPERT=,USE_ESSIOC=,PANEL_VER=$(PANEL_VER),NAME_MODE=$(NAME_MODE),MTCA_PREF=$(MTCA_PREF),IOC_PREF=$(IOC_PREF),$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR),CHECK_VER=,")
```
