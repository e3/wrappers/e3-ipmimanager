e3-ipmimanager - utility scripts
======
ESS Site-specific EPICS module : ipmimanager

# Introduction

Utility scripts are intended to get information about MTCA crate without GUI operator panels.

The scripts have been developed using Python 3.6.8 and require following Python modules:
- pyepics (https://pypi.org/project/pyepics/)
- configparser (https://pypi.org/project/configparser)
- mdutils (https://pypi.org/project/mdutils/)
- argparse
- telnetlib

The scripts emulate MCH command-line interface andcan be used for tests, but also for normal operation with e3-ipmimanager as a helper tools
- [`ipmimanager_show_fru.py`](../utils/ipmimanager_show_fru.py) - _displays all modules and M-States managed by the IOC_
- [`ipmimanager_show_fru_info.py`](../utils/ipmimanager_show_fruinfo.py) - _displays detailed information about selected module_
- [`ipmimanager_show_link_state.py`](../utils/ipmimanager_show_link_state.py) - _displays information about AMC links for chassis managed by the IOC_
- [`ipmimanager_show_sensorinfo.py`](../utils/ipmimanager_show_sensorinfo.py) - _displays detailed information about sensors of selected module_
- [`mch_telnet.py`](../utils/mch_telnet.py) - _helper script to verify connectivity with MCH via telnet interface_

# Configuration

All scripts require common configuration file. By default, the scripts look for [`../utils/mtca_crate.ini`](../utils/mtca_crate.ini) and load it if no other file specified as a command-line argument. Every script supports argument `--config_file (-c)` which allows changing the configuration file that should be used for the script execution.

The configuration file specifies following settings related to IOC and MTCA crates:
- global prefix used for all IOC variables (`PREFIX in section [IOC]`)
- MCH IP addresses (`MCH_IP in sections [MTCA_CRATE_*]`)
- prefixes for PVs related to all MTCA modules managed by the IOC (`PV_PREFIX_MODULE_* in sections [MTCA_CRATE_*]`)

Scripts support more than one MTCA chassis per IOC. To specify settings for more than one MTCA chassis please add more `[MTCA_CRATE_*]` sections. Section name should start with `MTCA_CRATE_`.

Example:
```ini
[IOC]
PREFIX = LabS:Ctrl-

[MTCA_CRATE_1]
MCH_IP = 192.168.1.0
PV_PREFIX_MTCA_CRATE = MTCA-100:

...

[MTCA_CRATE_2]
MCH_IP = 192.168.1.1
PV_PREFIX_MTCA_CRATE = MTCA-200:

...

```

# Usage

## 1. ipmimanager_show_fru

Example command
```commandline
$ python3 ipmimanager_show_fru.py
```

Example output
```
MTCA_CRATE_1 - FRU Information:
----------------
 FRU  Device                        State
==========================================
   5  CCT AM 900/412                M4
   6  mTCA-EVR-300                  M4
   7  SIS8300KU AMC                 M4
   9  RTM Carrier                   M4
  11  RTM Carrier                   M4
  40  Schroff uTCA CU               M4
  41  Schroff uTCA CU               M4
  51  PM-AC1000                     M4
  60  MCH-Clock                     M4
  61  MCH-PCIe                      M4
  92  SIS8300KU RTM                 M4
  94  RTM Carrier RTM               M4
  96  RTM Carrier RTM               M4
==========================================
```

Supported arguments
```commandline
-c, --config_file - Specify configuration file
```

## 2. ipmimanager_show_fruinfo

Example command
```commandline
$ python3 ipmimanager_show_fruinfo.py 5
or
$ python3 ipmimanager_show_fruinfo.py 5 -m MTCA_CRATE_2
```

Argument for the scripts is FRU ID (Field Replaceable Unit) - unique identifier of every module installed in the MTCA chassis. The number can be found using `ipmimanager_show_fru.py`
_NOTE: The same covention is used in MCH command-line interface._

Supported arguments
```commandline
-c, --config_file - Specify configuration file
-m, --mtca_crate - Specify MTCA crate (if IOC handles more than one), use section name from *.ini file
```

Example output
```
---------------------------------------
FRU Info for device 5:
---------------------------------------
Board Info Area
Manufacturer             : Concurrent Technologies
Board Name               : AM 900/412
Serial Number            : M30574/002
Part Number              : 781-6011-98
FRU file ID              :
---------------------------------------
Product Info Area
Manufacturer             :
Product Name             :
Product Number           :
Part Version             :
Product Serial Number    :
Asset Tag                :
FRU file ID              :
---------------------------------------
```

## 3. ipmimanager_show_link_state

Example command
```commandline
$ python3 ipmimanager_show_link_state.py
```

Supported arguments
```commandline
-c, --config_file - Specify configuration file
```

Example output
```
MTCA_CRATE_1

FRU   Device     Port  Type       Linkwidth  Speed
=======================================
5     AMC_1      0     Eth        x1         1.000 Gbps
      AMC_1      4     PCIE       x4         8.000 GT/s
                 5     PCIE       x4         8.000 GT/s
                 6     PCIE       x4         8.000 GT/s
                 7     PCIE       x4         8.000 GT/s
=======================================
```

## 4. ipmimanager_show_sensorinfo

Example command
```commandline
$ python3 ipmimanager_show_sensorinfo.py 5
or
$ python3 ipmimanager_show_sensorinfo.py 5 -m MTCA_CRATE_2
```

Argument for the scripts is FRU ID (Field Replaceable Unit) - unique identifier of every module installed in the MTCA chassis. The number can be found using `ipmimanager_show_fru.py`
_NOTE: The same covention is used in MCH command-line interface._

Supported arguments
```commandline
-c, --config_file - Specify configuration file
-m, --mtca_crate - Specify MTCA crate (if IOC handles more than one), use section name from *.ini file
```

Example output
```
Sensor Information for FRU 5 - CCT AM 900/412
====================================================================================================
 #   Name                            Value       Unit             Sensor Type
----------------------------------------------------------------------------------------------------
  0  HS 005 AMC1                          0.000  unspecified      invalid
  1  Ejector Handle                       1.000  unspecified      invalid
  2  VCC CORE                             0.950  volts            voltage
  3  +PS 1V0                              1.010  volts            voltage
  4  +PS 1V05                             1.080  volts            voltage
  5  +3.3V PSU                            3.230  volts            voltage
  6  +5V PSU                              5.115  volts            voltage
  7  +12V PSU                            12.296  volts            voltage
  8  Ambient Temp                        33.000  C                temperature
  9  CPU AMB Temp                        35.000  C                temperature
 10  CPU INT Temp                        35.000  C                temperature
----------------------------------------------------------------------------------------------------
```

## 5. mch_telnet

Example command
```commandline
$ python3 mch_telnet.py 192.168.1.1
```

Argument for the scripts is IP address of MCH

Example output
```
FRU list:
  0) MCH             - NAT-MCH-CM                     [M4]
  3) mcmc1           - NAT-MCH-MCMC                   [M4]
  5) AMC1            - CCT AM 900/412                 [M4]
  6) AMC2            - mTCA-EVR-300                   [M4]
  7) AMC3            - SIS8300KU AMC                  [M4]
  9) AMC5            - RTM Carrier                    [M4]
 11) AMC7            - RTM Carrier                    [M4]
 40) CU1             - Schroff uTCA CU                [M4]
 41) CU2             - Schroff uTCA CU                [M4]
 51) PM2             - PM-AC1000                      [M4]
 60) Clock1          - MCH-Clock                      [M4]
 61) HubMod1         - MCH-PCIe                       [M4]
 92) AMC3-RTM        - SIS8300KU RTM                  [M4]
 94) AMC5-RTM        - RTM Carrier RTM                [M4]
 96) AMC7-RTM        - RTM Carrier RTM                [M4]


Sensors list for FRU 0:
  0) HS 000 CM                      -            0x10            [          ] [Compact, 0xc2, 0x66, 0xf0]


Sensors list for FRU 3:
  1) Temp CPU                       -              35 C          [ok        ] [Full, 0xc2, 0x61, Temp]
  2) Temp I/O                       -              31 C          [ok        ] [Full, 0xc2, 0x61, Temp]
  3) HotSwap                        -            0x01            [          ] [Compact, 0xc2, 0x61, 0xf2]
  4) Version Change                 -            0x00            [          ] [Compact, 0xc2, 0x61, 0x2b]
  5) Base 1.2V                      -            1.21 V          [ok        ] [Full, 0xc2, 0x61, Voltage]
  6) Base 1.5V                      -            1.53 V          [ok        ] [Full, 0xc2, 0x61, Voltage]
  7) Base 1.8V                      -            1.82 V          [ok        ] [Full, 0xc2, 0x61, Voltage]
  8) Base 2.5V                      -            2.52 V          [ok        ] [Full, 0xc2, 0x61, Voltage]
  9) Base 3.3V                      -           3.376 V          [ok        ] [Full, 0xc2, 0x61, Voltage]
 10) Base 12V                       -           12.30 V          [ok        ] [Full, 0xc2, 0x61, Voltage]
 11) Base Current                   -            2.11 A          [ok        ] [Full, 0xc2, 0x61, Current]
 12) Red-Status                     -            0x01            [          ] [Compact, 0xc2, 0x61, 0xc2]
 13) HS 003 MCMC1                   -            0x10            [          ] [Compact, 0xc2, 0x61, 0xf0]

...

Links state:

   5   AMC_1      0   Eth       x1      1.000  Gbps
       AMC_1      4  PCIE       x4      8.000  GT/s
                  5  PCIE       x4      8.000  GT/s
                  6  PCIE       x4      8.000  GT/s
                  7  PCIE       x4      8.000  GT/s

```
_NOTE: Presented example is only short part of the full output generated by the script. Full output contains list of all modules installed in the MTCA chassis, list of sensors for every module and list of communication links between AMC modules._
