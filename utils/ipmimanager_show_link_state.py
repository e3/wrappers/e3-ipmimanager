#!/usr/bin/env python

import ipmimanager_common as e4i_common
import argparse


def print_links(links_str):
	links_str = list(filter(None, links_str))
	print("{:5} {:10} {:5} {:10} {:10} {:20}".format(links_str[2], links_str[3], links_str[4], links_str[5], links_str[6], links_str[7]))
	print(links_str[8])

	amc_indices = [i for i, s in enumerate(links_str) if 'AMC' in s]
	for i, index in enumerate(amc_indices):
		if links_str[index-1].isnumeric():
			print("{:5} {:10} ".format(links_str[index - 1], links_str[index]), end='')
		else:
			print("{:5} {:10} ".format("", links_str[index]), end='')

		start = amc_indices[i]+1
		if i + 1 < len(amc_indices):
			end = amc_indices[i + 1]
		else:
			end = None
		amc_ports = links_str[start:end]
		amc_ports_count = int(len(amc_ports)/5)
		for p in range(0, amc_ports_count):
			if p == 0:
				print("{:5} {:10} {:10} {:} {:}".format(amc_ports[p*5+0], amc_ports[p*5+1], amc_ports[p*5+2], amc_ports[p*5+3],  amc_ports[p*5+4]))
			else:
				print("{:16} {:5} {:10} {:10} {:} {:}".format("", amc_ports[p * 5 + 0], amc_ports[p * 5 + 1], amc_ports[p * 5 + 2], amc_ports[p * 5 + 3], amc_ports[p * 5 + 4]))
	print(links_str[-1])


if __name__ == "__main__":
	config_file = e4i_common.DEFAULT_CONFIG_FILE

	parser = argparse.ArgumentParser(description='Show AMC link state')
	parser.add_argument('-c', '--config_file', type=str, help='Configuration file')
	args = parser.parse_args()

	if args.config_file:
		config_file = args.config_file

	links = e4i_common.get_links_state(config_file)
	for crate in links:
		print("")
		print(crate)
		print("")
		print_links(links[crate])
