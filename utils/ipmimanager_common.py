from epics import caget, caput
import configparser

DEFAULT_CONFIG_FILE = "../utils/mtca_crate.ini"

PVSUFFIX_FRUPRESENCE = "P"
PVSUFFIX_FRUID = "FruId"
PVSUFFIX_FRUNAME = "FruName"
PVSUFFIX_FRUSTATE = "State"
PVSUFFIX_FRUBMANUF = "BManuf"
PVSUFFIX_FRUBNAME = "BName"
PVSUFFIX_FRUBSERIALNUMBER = "BSN"
PVSUFFIX_FRUBPARTNUMBER = "BPN"
PVSUFFIX_FRUBFRUFILEID = "BFruFileId"
PVSUFFIX_FRUPMANUF = "PManuf"
PVSUFFIX_FRUPNAME = "PName"
PVSUFFIX_FRUPRODUCTNUMBER = "PN"
PVSUFFIX_FRUPPARTVERSION = "PPV"
PVSUFFIX_FRUPSERIALNUMBER = "PSN"
PVSUFFIX_FRUPASSETTAG = "PAT"
PVSUFFIX_FRUPFRUFILEID = "PFruFileId"
PVSUFFIX_FRUFSTATE = "FState"
PVSUFFIX_FRUSENSORCNT = "SensorCnt"
PVSUFFIX_FRUSLOT = "Slot"

ALL_PVSUFFIXES_FOR_TESTS_FRU = [PVSUFFIX_FRUPRESENCE,
                                PVSUFFIX_FRUID,
                                PVSUFFIX_FRUNAME,
                                PVSUFFIX_FRUSTATE,
                                PVSUFFIX_FRUBMANUF,
                                PVSUFFIX_FRUBNAME,
                                PVSUFFIX_FRUBSERIALNUMBER,
                                PVSUFFIX_FRUBPARTNUMBER,
                                PVSUFFIX_FRUBFRUFILEID,
                                PVSUFFIX_FRUPMANUF,
                                PVSUFFIX_FRUPNAME,
                                PVSUFFIX_FRUPRODUCTNUMBER,
                                PVSUFFIX_FRUPPARTVERSION,
                                PVSUFFIX_FRUPSERIALNUMBER,
                                PVSUFFIX_FRUPASSETTAG,
                                PVSUFFIX_FRUPFRUFILEID,
                                PVSUFFIX_FRUFSTATE,
                                PVSUFFIX_FRUSENSORCNT,
                                PVSUFFIX_FRUSLOT ]



PVSUFFIX_SENSDRTYPE = "SdrType"
PVSUFFIX_SENTYPE = "Type"
PVSUFFIX_SENNAME = "Name"
PVSUFFIX_SENVALUE = "Value"
PVSUFFIX_SENUNITS = "Units"
PVSUFFIX_SENEVTRDTYPE = "EvtrdType"
PVSUFFIX_SENP = "P"
PVSUFFIX_SENNORMMAX = "NormMax"
PVSUFFIX_SENNORMMIN = "NormMin"
PVSUFFIX_SENTHRCOMMIT = "ThrCommit"
PVSUFFIX_SENTHRUPDATEALLOWED = "ThrUpdtAllow"
PVSUFFIX_SENTHRUPNONRECVAL = "ThrUpNonRec"
PVSUFFIX_SENTHRUPCRITVAL = "ThrUpCrt"
PVSUFFIX_SENTHRUPNONCRITVAL = "ThrUpNonCrt"
PVSUFFIX_SENTHRLONONRECVAL = "ThrLoNonRec"
PVSUFFIX_SENTHRLOCRITVAL = "ThrLoCrt"
PVSUFFIX_SENTHRLONONCRITVAL = "ThrLoNonCrt"
PVSUFFIX_SENTHRUPNONRECVAL_SP = "ThrUpNonRec-SP"
PVSUFFIX_SENTHRUPCRITVAL_SP = "ThrUpCrt-SP"
PVSUFFIX_SENTHRUPNONCRITVAL_SP = "ThrUpNonCrt-SP"
PVSUFFIX_SENTHRLONONRECVAL_SP = "ThrLoNonRec-SP"
PVSUFFIX_SENTHRLOCRITVAL_SP = "ThrLoCrt-SP"
PVSUFFIX_SENTHRLONONCRITVAL_SP = "ThrLoNonCrt-SP"
PVSUFFIX_SENTHRUPNONRECSTATE = "ThrUpNonRecStat"
PVSUFFIX_SENTHRUPCRITSTATE = "ThrUpCrtStat"
PVSUFFIX_SENTHRUPNONCRITSTATE = "ThrUpNonCrtStat"
PVSUFFIX_SENTHRLONONRECSTATE = "ThrLoNonRecStat"
PVSUFFIX_SENTHRLOCRITSTATE = "ThrLoCrtStat"
PVSUFFIX_SENTHRLONONCRITSTATE = "ThrLoNonCrtStat"
PVSUFFIX_SENTHRNOTACTUAL = "ThrNotActual"
PVSUFFIX_SENHYSTPOS = "HystPos"
PVSUFFIX_SENHYSTNEG = "HystNeg"
PVSUFFIX_SENPASSTHRLOCRITVAL = "PassThrLoCrt"
PVSUFFIX_SENPASSTHRLONONCRITVAL = "PassThrLoNonCrt"
PVSUFFIX_SENPASSTHRUPNONCRITVAL = "PassThrUpNonCrt"
PVSUFFIX_SENPASSTHRUPCRITVAL = "PassThrUpCrt"
PVSUFFIX_SENPASSEGU = "PassEgu"
PVSUFFIX_SENPASSWAVEEGU = "PassWaveEgu"
PVSUFFIX_SENWAVE = "Wave"

ALL_PVSUFFIXES_FOR_TESTS_SENSOR = [ PVSUFFIX_SENSDRTYPE,
                                    PVSUFFIX_SENTYPE,
                                    PVSUFFIX_SENNAME,
                                    PVSUFFIX_SENVALUE,
                                    PVSUFFIX_SENUNITS,
                                    PVSUFFIX_SENEVTRDTYPE,
                                    PVSUFFIX_SENP,
                                    PVSUFFIX_SENNORMMAX,
                                    PVSUFFIX_SENNORMMIN,
                                    PVSUFFIX_SENTHRCOMMIT,
                                    PVSUFFIX_SENTHRUPDATEALLOWED,
                                    PVSUFFIX_SENTHRUPNONRECVAL,
                                    PVSUFFIX_SENTHRUPCRITVAL,
                                    PVSUFFIX_SENTHRUPNONCRITVAL,
                                    PVSUFFIX_SENTHRLONONRECVAL,
                                    PVSUFFIX_SENTHRLOCRITVAL,
                                    PVSUFFIX_SENTHRLONONCRITVAL,
                                    PVSUFFIX_SENTHRUPNONRECVAL_SP,
                                    PVSUFFIX_SENTHRUPCRITVAL_SP,
                                    PVSUFFIX_SENTHRUPNONCRITVAL_SP,
                                    PVSUFFIX_SENTHRLONONRECVAL_SP,
                                    PVSUFFIX_SENTHRLOCRITVAL_SP,
                                    PVSUFFIX_SENTHRLONONCRITVAL_SP,
                                    PVSUFFIX_SENTHRUPNONRECSTATE,
                                    PVSUFFIX_SENTHRUPCRITSTATE,
                                    PVSUFFIX_SENTHRUPNONCRITSTATE,
                                    PVSUFFIX_SENTHRLONONRECSTATE,
                                    PVSUFFIX_SENTHRLOCRITSTATE,
                                    PVSUFFIX_SENTHRLONONCRITSTATE,
                                    PVSUFFIX_SENTHRNOTACTUAL,
                                    PVSUFFIX_SENHYSTPOS,
                                    PVSUFFIX_SENHYSTNEG,
                                    PVSUFFIX_SENWAVE ]

HIDDEN_PVSUFFIXES_FOR_TESTS_SENSOR = [
    PVSUFFIX_SENPASSTHRLOCRITVAL,
    PVSUFFIX_SENPASSTHRLONONCRITVAL,
    PVSUFFIX_SENPASSTHRUPNONCRITVAL,
    PVSUFFIX_SENPASSTHRUPCRITVAL,
    PVSUFFIX_SENPASSEGU,
    PVSUFFIX_SENPASSWAVEEGU,
]

PVSUFFIX_CHASSIS_GETLINKS_SP = "GetLinks-SP"
PVSUFFIX_CHASSIS_LINKS_RB = "Links-RB"
PVSUFFIX_CHASSIS_GETMCHVERSION_SP = "GetMchVersion-SP"
PVSUFFIX_CHASSIS_MCHVERSION_RB = "MchVersion-RB"
PVSUFFIX_CHASSIS_MCHVERSIONMACRO = "#MchVersionMacro"
PVSUFFIX_CHASSIS_MCHVERSIONCOMP_RB = "MchVersionComp-RB"

PVSUFFIX_EXPERT_SHUTDOWN_SP = "ShutDown-SP"
PVSUFFIX_EXPERT_REBOOT_SP = "Reboot-SP"
PVSUFFIX_EXPERT_REBOOTFRU_SP = "RebootFru-SP"

ALL_PVSUFFIXES_FOR_TESTS_CHASSIS = [
    PVSUFFIX_CHASSIS_GETLINKS_SP,
    PVSUFFIX_CHASSIS_LINKS_RB,
    PVSUFFIX_CHASSIS_GETMCHVERSION_SP,
    PVSUFFIX_CHASSIS_MCHVERSION_RB,
    PVSUFFIX_CHASSIS_MCHVERSIONMACRO,
    PVSUFFIX_CHASSIS_MCHVERSIONCOMP_RB,
    PVSUFFIX_EXPERT_SHUTDOWN_SP,
    PVSUFFIX_EXPERT_REBOOT_SP,
    PVSUFFIX_EXPERT_REBOOTFRU_SP,
]

PVSUFFIX_GLOBAL_TICK = "#Tick"
PVSUFFIX_GLOBAL_SYNC = "Sync"
PVSUFFIX_GLOBAL_SENSORINTERVAL_SP = "SensorInterval-SP"
PVSUFFIX_GLOBAL_THRESHINTERVAL_SP = "ThreshInterval-SP"
PVSUFFIX_GLOBAL_EVENTINTERVAL_SP = "EventInterval-SP"
PVSUFFIX_GLOBAL_TIMESTAMPS = "TimeStamps"
PVSUFFIX_GLOBAL_ENABLE_ARCHIVER = "EnableArchiver-SP"
PVSUFFIX_GLOBAL_PANELVER = "PanelVer"

ALL_PVSUFFIXES_FOR_TESTS_GLOBAL = [
    PVSUFFIX_GLOBAL_TICK,
    PVSUFFIX_GLOBAL_SYNC,
    PVSUFFIX_GLOBAL_SENSORINTERVAL_SP,
    PVSUFFIX_GLOBAL_THRESHINTERVAL_SP,
    PVSUFFIX_GLOBAL_EVENTINTERVAL_SP,
    PVSUFFIX_GLOBAL_TIMESTAMPS,
    PVSUFFIX_GLOBAL_ENABLE_ARCHIVER,
    PVSUFFIX_GLOBAL_PANELVER,
]

sdr_types = {
    0x01: "Full",
    0x02: "Compact",
    0x03: "Event-only",
}


def get_modules_prefixes(config, section):
    module_prefixes = dict()
    options = config.options(section)
    for option in options:
        if option.startswith("pv_prefix_module_"):
            module_prefixes[option[len("pv_prefix_module_"):]] = config.get(section, option)
    return module_prefixes


def create_fru_map(config_file=DEFAULT_CONFIG_FILE):
    config = configparser.ConfigParser()
    config.read(config_file)

    present_frus = dict()

    ioc_prefix = config['IOC']['PREFIX']
    for section in config.sections():
        if section.startswith("MTCA_CRATE_"):
            present_frus[section] = dict()
            modules_prefixes = get_modules_prefixes(config, section)
            for module in modules_prefixes:
                pv_fru_name_prefix = ioc_prefix + modules_prefixes[module]
                pv_name_presence = pv_fru_name_prefix + PVSUFFIX_FRUPRESENCE
                fru_present = caget(pv_name_presence, timeout=1.0)
                if fru_present:
                    pv_name_fru_id = pv_fru_name_prefix + PVSUFFIX_FRUID
                    fru_id = caget(pv_name_fru_id)
                    present_frus[section][fru_id] = pv_fru_name_prefix

    return present_frus


def sdrtype_to_str(sdr_type):
    return sdr_types.get(sdr_type, "Unknown")


def get_links_state(config_file=DEFAULT_CONFIG_FILE):
    config = configparser.ConfigParser()
    config.read(config_file)

    ioc_prefix = config['IOC']['PREFIX']
    links = dict()
    for section in config.sections():
        if section.startswith("MTCA_CRATE_"):
            crate_prefix = config[section]['PV_PREFIX_MTCA_CRATE']
            pv_fru_name_prefix = ioc_prefix + crate_prefix

            caput(pv_fru_name_prefix + PVSUFFIX_CHASSIS_GETLINKS_SP, 1, True)
            links[section] = caget(pv_fru_name_prefix + PVSUFFIX_CHASSIS_LINKS_RB, as_string=True)

    return links
