#!/usr/bin/env python

from epics import caget
import ipmimanager_common as e4i_common
import argparse


def print_fru_list(present_fru_dict):
	print("----------------")
	print(" FRU  Device                        State")
	print("==========================================")

	for fru_id in sorted(present_fru_dict):
		fru_pv_prefix = present_fru_dict[fru_id]
		fru_name = caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUNAME)
		fru_state = caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUSTATE)

		print(str(fru_id).rjust(4) + "  " + fru_name.ljust(30) + "M" + str(fru_state))

	print("==========================================")


if __name__ == "__main__":
	config_file = e4i_common.DEFAULT_CONFIG_FILE

	parser = argparse.ArgumentParser(description='Show FRU list')
	parser.add_argument('-c', '--config_file', type=str, help='Configuration file')
	args = parser.parse_args()

	if args.config_file:
		config_file = args.config_file

	fru_map = e4i_common.create_fru_map(config_file)
	for crate in fru_map:
		print("")
		print(crate + " - FRU Information:")
		print_fru_list(fru_map[crate])
