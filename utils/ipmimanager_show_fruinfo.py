#!/usr/bin/env python

from epics import caget
import ipmimanager_common as e4i_common
import argparse


def print_fru_info(present_fru_dict, fru_id):
	fru_pv_prefix = present_fru_dict[int(fru_id)]
	print("---------------------------------------")
	print("FRU Info for device %s:" % fru_id)
	print("---------------------------------------")
	print("Board Info Area")
	print("Manufacturer             : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUBMANUF))
	print("Board Name               : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUBNAME))
	print("Serial Number            : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUBSERIALNUMBER))
	print("Part Number              : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUBPARTNUMBER))
	print("FRU file ID              : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUBFRUFILEID))
	print("---------------------------------------")
	print("Product Info Area")
	print("Manufacturer             : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUPMANUF))
	print("Product Name             : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUPNAME))
	print("Product Number           : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUPRODUCTNUMBER))
	print("Part Version             : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUPPARTVERSION))
	print("Product Serial Number    : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUPSERIALNUMBER))
	print("Asset Tag                : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUPASSETTAG))
	print("FRU file ID              : %s" % caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUPFRUFILEID))
	print("---------------------------------------")


if __name__ == "__main__":
	config_file = e4i_common.DEFAULT_CONFIG_FILE

	parser = argparse.ArgumentParser(description='Show FRU information')
	parser.add_argument('-c', '--config_file', type=str, help='Configuration file')
	parser.add_argument('-m', '--mtca_crate', type=str, help='Specify MTCA crate (if IOC handles more than one)')
	parser.add_argument('fru_id')
	args = parser.parse_args()

	if args.config_file:
		config_file = args.config_file

	fru_map = e4i_common.create_fru_map(config_file)
	if args.mtca_crate:
		section_fru_map = fru_map[args.mtca_crate]
	else:
		first_key = next(iter(fru_map))
		section_fru_map = fru_map[first_key]

	print_fru_info(section_fru_map, args.fru_id)
