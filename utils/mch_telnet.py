# MCH Telnet Communication wrapper

import telnetlib
import time
import string
import sys


def is_hex(s):
    hex_digits = set(string.hexdigits + 'xX')
    return all(c in hex_digits for c in s)


class ParsingError(Exception):
    def __init__(self, msg='MCH response parsing error', *args, **kwargs):
        super().__init__(msg, *args, **kwargs)


class FRU:
    __slots__ = ["fru_id", "device_id", "fru_state", "fru_name"]

    def __init__(self, fru_id, device_id, fru_state, fru_name):
        self.fru_id = int(fru_id)
        self.device_id = device_id.strip()
        self.fru_state = fru_state.strip()
        self.fru_name = fru_name.strip()

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "{:3}) {:15} - {:30} [{:}]".format(self.fru_id, self.device_id, self.fru_name, self.fru_state)


class FRUSensor:
    __slots__ = ["number", "sdr_type", "sensor_type", "entity", "instance", "value", "state", "name", "unit"]

    POS_NUMBER = slice(0, 6)
    POS_SDR_TYPE = slice(6, 15)
    POS_TYPE = slice(15, 23)
    POS_ENTITY = slice(23, 29)
    POS_INSTANCE = slice(29, 35)
    POS_VALUE = slice(35, 46)
    POS_STATE = slice(46, 52)
    POS_NAME = 52

    def __init__(self, number, sdr_type, sensor_type, entity, instance, value, unit, state, name):
        self.number = number.strip()
        self.sdr_type = sdr_type.strip()
        self.sensor_type = sensor_type.strip()
        self.entity = entity.strip()
        self.instance = instance.strip()
        self.value = value.strip()
        self.state = state.strip()
        self.name = name.strip()
        self.unit = unit.strip()

    def __lt__(self, other):
        return self.name < other.name

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "{:>3}) {:30} - {:>15} {:10} [{:10}] [{:}, {:}, {:}, {:}]".format(self.number, self.name, self.value, self.unit, self.state, self.sdr_type, self.entity, self.instance, self.sensor_type)


class MCHTelnetConn:

    def __init__(self, ip_address):
        self.tn = telnetlib.Telnet(ip_address)
        self.tn.read_until(b"nat>")

    def __del__(self):
        self.tn.write(b"exit\r\n")
        self.tn.read_all()

    def send_command(self, command):
        # clear command-line
#        self.tn.write(b"\r\n")
#        self.tn.read_until(b"nat>")

        # send command
        self.tn.write("{:}\r\n".format(command).encode())

    def get_response(self):
        # read response
        mch_response = self.tn.read_until(b"nat>")
        time.sleep(1)
        return mch_response.decode("ascii")

    def get_fru_list(self):
        self.send_command("show_fru")
        mch_response = self.get_response()
        #print(mch_response_str)

        # parse response
        mch_response_lines = mch_response.split("\n")
        boundary_lines = [c for [c, line] in enumerate(mch_response_lines) if "===" in line]
        if len(boundary_lines) != 2:
            raise ParsingError

        mch_response_fru_lines = mch_response_lines[boundary_lines[0] + 1:boundary_lines[1]]
        #    print(mch_response_fru_lines)

        fru_list = []
        for line in mch_response_fru_lines:
            # Remove unnecessary whitespaces
            line_stripped = " ".join(line.split())
            # Split line
            line_splitted = line_stripped.split(" ")
            #        print(line_splitted)
            if len(line_splitted) < 4:
                raise ParsingError
            fru_obj = FRU(int(line_splitted[0]), line_splitted[1], line_splitted[2], ' '.join(line_splitted[3:]))
            fru_list.append(fru_obj)

        return fru_list

    def get_sensor_info(self, fru_id):
        self.send_command("show_sensorinfo {:}".format(fru_id))
        mch_response = self.get_response()
        #print(mch_response)

        # parse response
        mch_response_lines = mch_response.split("\n")
        boundary_lines = [c for [c, line] in enumerate(mch_response_lines) if "---" in line]
        if len(boundary_lines) != 2:
            raise ParsingError

        mch_response_sensors_lines = mch_response_lines[boundary_lines[0] + 1:boundary_lines[1]]
        #print(mch_response_sensors_lines)

        sensors_list = []
        for line in mch_response_sensors_lines:
            # Remove unnecessary whitespaces
            sensor_number = line[FRUSensor.POS_NUMBER]
            if not sensor_number.strip().isnumeric():
                continue
            sdr_type = line[FRUSensor.POS_SDR_TYPE]
            sensor_type = line[FRUSensor.POS_TYPE]
            entity = line[FRUSensor.POS_ENTITY]
            instance = line[FRUSensor.POS_INSTANCE]
            value = line[FRUSensor.POS_VALUE]
            unit = ""
            val_split = value.strip().split(" ")
            if len(val_split) == 2:
                value = val_split[0]
                unit = val_split[1]

            state = line[FRUSensor.POS_STATE]
            name = line[FRUSensor.POS_NAME:]
            if "  " in name.strip():                        # workaround to remove 0x00 at the beginning of compact sensor name
                name_splitted = name.strip().split("  ")
                if is_hex(name_splitted[0]):
                    name = ''.join(name_splitted[1:])

            sensor_object = FRUSensor(sensor_number, sdr_type, sensor_type, entity, instance, value, unit, state, name)
            sensors_list.append(sensor_object)

        return sensors_list

    def get_link_state(self):
        self.send_command("show_link_state")
        mch_response = self.get_response()
#        print(mch_response)

        # parse response
        mch_response_lines = mch_response.split("\n")
        boundary_lines = [c for [c, line] in enumerate(mch_response_lines) if "===" in line]
        if len(boundary_lines) != 2:
            raise ParsingError

        mch_response_links_lines = mch_response_lines[boundary_lines[0] + 1:boundary_lines[1]]
        #print(mch_response_links_lines)
        return mch_response_links_lines


if __name__ == '__main__':

    input_args = len(sys.argv) - 1
    if input_args != 1:
        print("MCH IP address not specified")
        print("Usage: ")
        print(sys.argv[0] + " <mch_ip_address>")
        print("e.g." + sys.argv[0] + " 192.168.0.1")
        exit(-1)

    mch_ip_address = sys.argv[1]
    MCHConn = MCHTelnetConn(mch_ip_address)

    try:
        # Read fru list
        fru_list = MCHConn.get_fru_list()

        print("FRU list:")
        for fru in fru_list:
            print(fru)

        for fru in fru_list:
            sensor_info = MCHConn.get_sensor_info(fru.fru_id)
            print("\n\nSensors list for FRU {:}:".format(fru.fru_id))
            for sensor in sensor_info:
                print(sensor)

        print("\n\nLinks state:")
        links_state = MCHConn.get_link_state()
        for s in links_state:
            print(s)

    except Exception as err:
        print("Exception: {:}".format(err))
        exit(-1)
