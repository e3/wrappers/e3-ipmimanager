#!/usr/bin/env python

import argparse
from epics import caget
import ipmimanager_common as e4i_common


def print_sensor_info(present_fru_dict, fru_id):
	fru_pv_prefix = present_fru_dict[int(fru_id)]
	sensor_cnt = caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUSENSORCNT)
	fru_name = caget(fru_pv_prefix + e4i_common.PVSUFFIX_FRUNAME)
	print("Sensor Information for FRU {:} - {:}".format(fru_id, fru_name))
	print("====================================================================================================")
	print(" #   Name                            Value       Unit             Sensor Type")
	print("----------------------------------------------------------------------------------------------------")
	for i in range(0, sensor_cnt):
		sensor_type = caget(fru_pv_prefix + "Sen" + str(i) + e4i_common.PVSUFFIX_SENTYPE)
		sensor_name = caget(fru_pv_prefix + "Sen" + str(i) + e4i_common.PVSUFFIX_SENNAME)
		sensor_value = caget(fru_pv_prefix + "Sen" + str(i) + e4i_common.PVSUFFIX_SENVALUE)
		sensor_units = caget(fru_pv_prefix + "Sen" + str(i) + e4i_common.PVSUFFIX_SENUNITS)

		print("{:>3}  {:<30}  {: >#010.3f}  {:15.15}  {:15.15}".format(i, sensor_name, sensor_value, sensor_units, sensor_type))
	print("----------------------------------------------------------------------------------------------------")


if __name__ == "__main__":
	config_file = e4i_common.DEFAULT_CONFIG_FILE

	parser = argparse.ArgumentParser(description='Show FRU sensors information')
	parser.add_argument('-c', '--config_file', type=str, help='Configuration file')
	parser.add_argument('-m', '--mtca_crate', type=str, help='Specify MTCA crate (if IOC handles more than one)')
	parser.add_argument('fru_id')
	args = parser.parse_args()

	if args.config_file:
		config_file = args.config_file

	fru_map = e4i_common.create_fru_map(config_file)
	if args.mtca_crate:
		section_fru_map = fru_map[args.mtca_crate]
	else:
		first_key = next(iter(fru_map))
		section_fru_map = fru_map[first_key]

	print_sensor_info(section_fru_map, args.fru_id)
